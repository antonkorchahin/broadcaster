import {precacheAndRoute} from 'workbox-precaching';

precacheAndRoute([
  {url: '/wp-content/themes/zeno_new/css/style.min.css', revision: null },
  {url: '/wp-content/themes/zeno_new/js/jquery.min.js', revision: null },
  {url: '/wp-content/themes/zeno_new/js/migrate.min.js', revision: null },
  {url: '/wp-content/themes/zeno_new/js/library.min.js', revision: null },
  {url: '/wp-content/themes/zeno_new/js/script.min.js', revision: null },
]);