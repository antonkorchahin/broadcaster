import {fetchPrayerLine, searchPrayerLines} from "@/api/zenofm/prayerLines.api"
import {prayerLineUrl, religionLink} from "@/helpers/prayer-line.helper"
import {getAttributes} from "@/api/zenofm/search.api"
import {getDescription} from "@/helpers/description.helper"
import {getOptimizedImage} from "@/helpers/images.helper"

export const PrayerLineRepository = () => {
  const get = async (identifier) => {
    const result = await fetchPrayerLine(identifier)

    if (!result) {
      return false
    }

    const {
      uid,
      name,
      description = null,
      website = null,
      facebook = null,
      twitter = null,
      instagram = null,
      religion = null,
      logo = null,
      backgroundImage = null,
      phone = null,
      pretty_url = null,
      // email = null,
      // ext = null,
    } = result

    let prayerLine = {
      key: uid,
      slug: pretty_url,
      identifier: pretty_url ? pretty_url : uid,
      name: name,
      logo: logo ? getOptimizedImage(encodeURI(logo), 500, 500) : null,
      background: backgroundImage ? getOptimizedImage(encodeURI(backgroundImage), 1920, 1080) : null,
      description: getDescription(description),
      religion: religion,
      phone: phone,
      website: website,
      facebook: facebook,
      twitter: twitter,
      instagram: instagram
    }

    prayerLine.url = prayerLineUrl(prayerLine.identifier)
    prayerLine.urlAMP = prayerLineUrl(prayerLine.identifier, true)

    return prayerLine
  }

  const search = async (params = {}) => {
    let results = await searchPrayerLines(params)

    if (!results.length) {
      return []
    }

    return results.map(prayerLine => {
      return {
        url: prayerLineUrl(prayerLine.pretty_url),
        name: prayerLine.name,
        logo: getOptimizedImage(prayerLine.logo, 268, 268)
      }
    })
  }

  const getReligions = async (limit = null) => {
    const religions = await getAttributes('prayerlines', 'religions', limit)

    return religions.map(religion => {
      return {
        name: religion.name,
        url: religionLink(religion)
      }
    })
  }

  return {
    get,
    search,
    getReligions
  }
}