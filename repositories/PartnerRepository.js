import {getDescription} from "@/helpers/description.helper"
import {getOptimizedImage} from "@/helpers/images.helper"
import {fetchPartner, fetchPartners} from "@/api/tools/partners.api";
import {partnerUrl} from "@/helpers/partners.helper";
import {getRadioLogo} from "@/helpers/station.helpers";
import {siteUrl} from "@/helpers/helpers";


export const PartnerRepository = () => {

  const get = async (identifier) => {
    let result = await fetchPartner(identifier)

    if (!result) {
      return false
    }

    const {
      slug = null,
      title = null,
      logo = null,
      description = null,
      assets = [],
      // id,
      // order = null,
    } = result

    const partner = {
      name: title,
      logo: getOptimizedImage(logo, 268, 268),
      url: partnerUrl(slug),
      description: getDescription(description),
      assets: []
    }

    if (!Array.isArray(assets) && !assets.length) {
      return partner
    }

    partner.assets = assets.map(asset => {
      if (asset.type === "station") {
        asset.logo = getRadioLogo(asset.key, 268, 268)
        asset.url = siteUrl(`/radio/${asset.prettyUrl}/`)
      } else {
        asset.logo = getOptimizedImage(asset.logo, 268, 268)
        asset.url = siteUrl(`/${asset.type}/${typeof asset.prettyUrl !== "undefined" ? asset.prettyUrl : asset.key}/`)
      }

      return {
        name: asset.title,
        logo: asset.logo,
        url: asset.url,
        type: asset.type
      }
    })

    return partner
  }

  const list = async (limit = null) => {
    let results = await fetchPartners(limit)

    return results.map(partner => {
      return {
        name: partner.title,
        logo: getOptimizedImage(partner.logo, 268, 268),
        url: partnerUrl(partner.slug)
      }
    })
  }

  return {
    get,
    list
  }
}