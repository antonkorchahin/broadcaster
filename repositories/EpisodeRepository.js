import {fetchEpisode, searchEpisodes} from "@/api/zenofm/episodes.api"
import {getDescription} from "@/helpers/description.helper"
import {getOptimizedImage} from "@/helpers/images.helper"
import {episodeUrl, getEpisodeAuthor, getEpisodeSummary} from "@/helpers/episodes.helper"


export const EpisodeRepository = (podcast = null) => {

  const get = async (episodeId) => {
    let result = await fetchEpisode(podcast.key, episodeId)

    if (!result) {
      return false
    }

    const {
      key,
      title = null,
      description = null,
      duration_hms = null,
      file_url = null,
      image = null,
      author = null,
      publish_date = null,
      link = null,
      pretty_url = null,
      tags = [],
    } = result

    return {
      key: key,
      slug: pretty_url,
      title: title,
      description: getDescription(description),
      author: getEpisodeAuthor(author),
      file_url: file_url,
      link: link,
      tags: tags,
      duration: duration_hms,
      logo: getOptimizedImage(image, 500, 500),
      url: episodeUrl(podcast.identifier, pretty_url ? pretty_url : key),
      published_at: publish_date,
    }
  }

  const search = async (params = {}) => {
    let results = await searchEpisodes(podcast.key, params)

    if (!results || !results.length) {
      return []
    }

    return results.map(episodeData => {
      const {
        key,
        title = null,
        file_url = null,
        image = null,
        duration_hms = null,
        publish_date = null,
        pretty_url = null,
      } = episodeData

      return {
        key: key,
        title: title,
        summary: getEpisodeSummary(episodeData),
        url: episodeUrl(podcast.identifier, pretty_url ? pretty_url : key),
        file_url: file_url,
        duration: duration_hms,
        logo: getOptimizedImage(image, 75, 75),
        published_at: publish_date,
      }
    })
  }

  return {
    get,
    search
  }
}