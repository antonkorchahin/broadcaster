import {fetchPodcast, fetchStationPodcasts, searchPodcasts} from "@/api/zenofm/podcasts.api"
import {getRadioLogo, radioURL} from "@/helpers/station.helpers"
import {getAttributes} from "@/api/zenofm/search.api"
import {getDescription} from "@/helpers/description.helper"
import {getOptimizedImage} from "@/helpers/images.helper"
import {categoryLink, countryLink, languageLink, podcastUrl} from "@/helpers/podcasts.helper"


export const PodcastRepository = () => {
  const get = async (identifier) => {
    const result = await fetchPodcast(identifier)

    if (!result) {
      return false
    }

    const {
      key,
      title = null,
      image = null,
      description = null,
      categories = [],
      language = null,
      prettyUrl = null,
      author = null,
      link = null,
      episodesCount = null,
      station = null,
    } = result

    let podcast = {
      key: key,
      slug: prettyUrl,
      identifier: prettyUrl ? prettyUrl : key,
      title: title,
      description: getDescription(description),
      author: author,
      categories: categories,
      language: language,
      link: link,
      episodesCount: episodesCount,
      logo: getOptimizedImage(image, 500, 500),
    }

    podcast.url = podcastUrl(podcast.identifier)
    podcast.urlAMP = podcastUrl(podcast.identifier, true)

    if (station) {
      const {
        key = null,
        name = null,
        pretty_url = null,
        featured = null,
        sponsored = null,
      } = station

      podcast.station = {
        name: name,
        slug: pretty_url,
        url: radioURL(pretty_url),
        logo: getRadioLogo(key, 100, 100),
        featured: featured,
        sponsored: sponsored,
      }
    }

    return podcast
  }

  const search = async (params = {}) => {

    let results = await searchPodcasts(params)

    if (!results.length) {
      return []
    }

    return results.map(podcastData => {
      const {
        objectID = null,
        title = null,
        image = null,
        pretty_url = null,
      } = podcastData

      let podcast = {
        key: objectID,
        slug: pretty_url,
        identifier: pretty_url || objectID,
        title: title,
        logo: getOptimizedImage(image, 268, 268),
      }

      podcast.url = podcastUrl(podcast.identifier)

      return {
        url: podcast.url,
        title: podcast.title,
        logo: podcast.logo
      }
    })
  }

  const stationPodcasts = async (stationKey) => {
    const results = await fetchStationPodcasts(stationKey)

    if (!results.length) {
      return []
    }

    return results.map(podcastData => {
      const {
        key = null,
        title = null,
        image = null,
        prettyUrl = null,
      } = podcastData

      let podcast = {
        key: key,
        slug: prettyUrl,
        identifier: prettyUrl || key,
        title: title,
        logo: getOptimizedImage(image, 268, 268),
      }

      return {
        url: podcastUrl(podcast.identifier),
        title: podcast.title,
        logo: podcast.logo
      }
    })
  }

  const getCountries = async (limit = null) => {
    const countries = await getAttributes('podcasts', 'countries', limit)

    return countries.map(country => {
      return {
        name: country.name,
        url: countryLink(country)
      }
    })
  }

  const getLanguages = async (limit = null) => {
    const languages = await getAttributes('podcasts', 'languages', limit)

    return languages.map(language => {
      return {
        name: language.name,
        url: languageLink(language)
      }
    })
  }

  const getCategories = async (limit = null) => {
    const categories = await getAttributes('podcasts', 'categories', limit)

    return categories.map(category => {
      return {
        id: category.id,
        name: category.text,
        parent: category.parent,
        url: categoryLink({name: category.text})
      }
    })
  }

  return {
    get,
    search,
    stationPodcasts,
    getCountries,
    getLanguages,
    getCategories,
  }
}