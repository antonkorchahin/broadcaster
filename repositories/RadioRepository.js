import {logError} from "@/helpers/helpers"
import {
  countryLink,
  genreLink,
  getRadioBackground,
  getRadioLogo,
  languageLink,
  radioURL,
  streamURL
} from "@/helpers/station.helpers"
import {getAttributes} from "@/api/zenofm/search.api"
import {fetchRadio, fetchStationCards, searchStations} from "@/api/zenofm/stations.api"
import {fetchStationPodcasts} from "@/api/zenofm/podcasts.api"
import {getDescription} from "@/helpers/description.helper"

export const RadioRepository = () => {
  const get = async (identifier) => {
    const result = await fetchRadio(identifier)

    if (!result) {
      return false
    }

    const {
      objectID,
      country = null,
      content_type = null,
      station_name = null,
      language = null,
      city = null,
      description = null,
      pretty_url = null,
      exclusive = false,
      featured = false,
      sponsored = false,
      updated = null,
      live_streams = null,
      streamname = null,
      // podcasts = 0,
      // audio_video = null,
    } = result

    const radioPoscasts = await fetchStationPodcasts(result.objectID)

    return {
      key: result.objectID,
      slug: pretty_url,
      name: station_name,
      url: radioURL(pretty_url),
      country: country,
      genre: content_type,
      languages: language ? result.language.split(',') : [],
      city: city,
      description: getDescription(description),
      hasVideo: !!live_streams,
      video: live_streams ? {
        name: live_streams[0].name,
        identifier: live_streams[0].url
      } : null,
      exclusive: !!exclusive,
      featured: !!featured,
      sponsored: !!sponsored,
      updated: result.updated,
      streamURL: streamURL(streamname),
      streamName: streamname,
      hasPodcasts: radioPoscasts.length > 0,
      logo: getRadioLogo(objectID, 240, 240, updated),
      background: getRadioBackground(objectID, 1500, 600, updated),
    }
  }

  const search = async (params = {}) => {
    let results = await searchStations(params)

    if (!results.length) {
      return []
    }

    return results.map(radio => {
      return {
        url: radioURL(radio.pretty_url),
        name: radio.station_name,
        logo: getRadioLogo(radio.objectID, 268, 268, radio.updated),
        featured: typeof radio.featured !== "undefined" && radio.featured === 1,
        sponsored: typeof radio.sponsored !== "undefined" && radio.sponsored === 1
      }
    })
  }

  const getRecommendedStations = async (station) => {
    try {
      const limit = 7

      let params = {
        limit: limit,
      }

      const {country = null, category = null} = station

      if (country) {
        params.country = country
      }

      if (category) {
        params.genre = category
      }

      let recommended = await search(params)

      if (recommended.length < 2 && country) {
        recommended = await search({
          country: country,
          limit: limit
        })
      }

      if (recommended.length < 2 && category) {
        recommended = await search({
          genre: category,
          limit: limit
        })
      }

      if (recommended.length < 2) {
        return []
      }

      // Exclude current station
      recommended = recommended.filter(item => item.name !== station.name)

      recommended = recommended.slice(0, 6)

      return recommended
    } catch (e) {
      logError(e)
      return null
    }
  }

  const getCards = async (stationKey) => {
    const cardsData = await fetchStationCards(stationKey)

    let cards = {}

    cardsData.forEach(card => {
      cards[card.type] = card
    })

    cards.CardAnnouncement.announcements = cards.CardAnnouncement.announcements.slice(0, 3)

    return cards
  }

  const getCountries = async (limit = null) => {
    const countries = await getAttributes('stations', 'countries', limit)

    return countries.map(country => {
      return {
        name: country.name,
        url: countryLink(country)
      }
    })
  }

  const getLanguages = async (limit = null) => {
    const languages = await getAttributes('stations', 'languages', limit)

    return languages.map(language => {
      return {
        name: language.name,
        url: languageLink(language)
      }
    })
  }

  const getGenres = async (limit = null) => {
    const genres = await getAttributes('stations', 'genres', limit)

    return genres.map(genre => {
      return {
        name: genre.name,
        url: genreLink(genre)
      }
    })
  }

  return {
    get,
    search,
    getCountries,
    getLanguages,
    getGenres,
    getCards,
    getRecommendedStations,
  }
}