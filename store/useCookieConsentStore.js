import create from 'zustand'

const useCookieConsentStore = create((set, get) => ({
  consentFinished: false,

  location: {
    country: null,
    gdprCountry: null
  },

  levels: [],

  computed: {
    get country() {
      return get().location.country
    },

    get consentRequired() {
      return get().location.gdprCountry
    },

    get analyticsEnabled() {
      return get().levels.includes('analytics')
    },

    get targetingEnabled() {
      return get().levels.includes('targeting')
    },
  },

  setConsentFinished: (payload) => {
    set({consentFinished: !!payload})
  },

  setLocation: (payload) => {
    set({location: payload})

    if (!payload.gdprCountry) {
      set({levels: ["necessary", "analytics", "targeting"]})
    }
  },

  setLevels: (payload) => {
    set({levels: payload})
  },
}))

export default useCookieConsentStore