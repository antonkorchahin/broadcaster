import create from "zustand"

const useSearchStore = create(set => ({
  showFilters: false,

  setShowFilters: (showFilters) => {
    set({showFilters: showFilters})
  }
}))

export default useSearchStore