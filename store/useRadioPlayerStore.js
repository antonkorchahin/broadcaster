import create from "zustand"

const nowPlaying = {
  author: "",
  title: ""
}

const playerRadio = {
  image: "",
  name: "",
  stream: "",
  prettyUrl: ""
}

const useRadioPlayerStore = create((set, get) => ({
  active: false,
  playing: false,
  loading: false,
  nowPlaying: nowPlaying,
  playerRadio: playerRadio,

  play: () => {
    set({active: true})
    set({playing: true})
  },

  pause: () => {
    set({playing: false})
  },

  stop: () => {
    set({active: false})
    set({playing: false})
    set({loading: false})

    set({nowPlaying: nowPlaying})
    set({playerRadio: playerRadio})
  },

  setLoading: (isLoading) => {
    set({loading: isLoading})
  },

  setPlayerRadio: (playerRadio) => {
    set({playerRadio: playerRadio})
  },

  setNowPlaying: (nowPlaying) => {
    set({nowPlaying: nowPlaying})
  }
}))

export default useRadioPlayerStore