import create from "zustand"

const player = {
  id: null,
  title: null,
  subTitle: null,
  image: null,
  canonical: null,
  mediaURL: null,
  streamName: null
}

const ad = {
  mediaFile: null,
  triggers: {
    implession: null,
    start: null,
    firstQuartile: null,
    midpoint: null,
    thirdQuartile: null,
    complete: null,
  }
}

const usePlayerStore = create((set, get) => ({
  loading: false,
  status: null, // paused / playing,
  player: player,
  ad: ad,
  volume: 100,
  duration: null,
  currentTime: 0,
  playbackRate: 1,
  type: null,

  state: {
    get isLoading() {
      return !!get().loading
    },
    get isPlaying() {
      return get().status === 'playing'
    },
    get isPaused() {
      return get().status === 'paused'
    },
    get isStoped() {
      return !get().status
    },
  },

  typeState: {
    get isRadioPlayer() {
      return get().type === 'radio'
    },
    get isEpisodePlayer() {
      return get().type === 'episode'
    },
    get isAdPlayer() {
      return get().ad.mediaFile !== null
    }
  },

  setLoading(loading) {
    set({loading: loading})
  },

  play: () => {
    get().setStatus('playing')
  },

  pause: () => {
    get().setStatus('paused')
  },

  stop: () => {
    get().setStatus()
  },

  setPlayer: ({
                id = null,
                title = null,
                image = null,
                subTitle = null,
                mediaURL = null,
                canonical = null,
                streamName = null,
              }, type = null) => {

    get().setType(type)

    set({
      player: {
        id: id,
        title: title,
        image: image,
        subTitle: subTitle,
        canonical: canonical,
        streamName: streamName,
        mediaURL: mediaURL,
      }
    })
  },

  setAd: (ad) => {
    set({ad: ad})
  },

  resetAd: () => {
    get().setDuration()
    get().setVolume()
    get().setCurrentTime()
    set({ad: ad})
  },

  setType: (type) => {
    set({type: type})
  },

  setStatus: (status = null) => {
    set({status: status})
  },

  setVolume: (volume = 100) => {
    set({volume: volume})
  },

  setDuration: (duration = null) => {
    set({duration: duration})
  },

  setCurrentTime: (currentTime = 0) => {
    set({currentTime: currentTime})
  },

  setPlaybackRate: (playbackRate = 1) => {
    set({playbackRate: playbackRate})
  },

  skipTime: (backward = false) => {
    let newTime = backward ? get().currentTime - 10 : get().currentTime + 30

    if (newTime < 0) {
      newTime = 0
    }

    get().setCurrentTime(newTime)
  }
}))

export default usePlayerStore