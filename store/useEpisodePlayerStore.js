import create from "zustand"

const episode = {
  key: "",
  url: "",
  image: "",
  title: "",
  author: "",
  file: ""
}

const useEpisodePlayerStore = create((set, get) => ({
  active: false,
  playing: false,
  loading: false,
  playerEpisode: episode,
  adsLoaded: false,
  adsPlaying: false,
  adsPaused: false,

  play: () => {
    set({active: true})
    set({playing: true})
  },

  pause: () => {
    set({playing: false})
  },

  playAd:() => {
    if (!get().adsLoaded) {
      get().playIMA()
    }
  },

  startIMA: () => {
    set({adsLoaded: false})
    set({adsPlaying: true})
    set({adsPaused: false})
  },

  playIMA: () => {
    set({adsPaused: false})
  },

  pauseIMA: () => {
    set({adsPaused: true})
  },

  stopIMA: () => {
    set({adsPlaying: false})
    set({adsPaused: false})
    set({adsLoaded: true})
  },

  stop: () => {
    set({active: false})
    set({playing: false})
    set({loading: false})
    set({adsPlaying: false})
    set({adsPaused: false})
    set({adsLoaded: false})
    set({playerEpisode: episode})
  },

  setAdsLoaded: (adsLoaded) => {
    set({adsLoaded: adsLoaded})
  },

  setActive: (active) => {
    set({active: active})
  },

  setLoading: (isLoading) => {
    set({loading: isLoading})
  },

  setPlayerEpisode: (episode) => {
    set({playerEpisode: episode})
  }
}))

export default useEpisodePlayerStore