const moduleExports = {
  trailingSlash: true,
  outputFileTracing: false,

  images: {
    domains: [
      "images.zeno.fm"
    ]
  },

  webpack: (config, {dev, isServer}) => {
    if (!isServer) {
      config.resolve.fallback.fs = false
    }

    // Replace React with Preact only in client production build
    if (!dev && !isServer) {
      Object.assign(config.resolve.alias, {
        react: "preact/compat",
        "react-dom$": "preact/compat",
        'react-dom/server$': "preact/compat/server",
        'react-dom/server.browser$': "preact/compat/server.browser",
        'react-dom/client$': "preact/compat/client"
      })
    }

    return config
  }
};

// const sentryWebpackPluginOptions = {
//   // Additional config options for the Sentry Webpack plugin. Keep in mind that
//   // the following options are set automatically, and overriding them is not
//   // recommended:
//   //   release, url, org, project, authToken, configFile, stripPrefix,
//   //   urlPrefix, include, ignore
//   url: "https://sentry.io/",
//   org: "zeno-dq",
//   project: "zeno-dq",
//   authToken: process.env.SENTRY_AUTH_TOKEN,
//   silent: true // Suppresses all logs
//   // For all available options, see:
//   // https://github.com/getsentry/sentry-webpack-plugin#options.
// };

// Make sure adding Sentry options is the last code to run before exporting, to
// ensure that your source maps include changes from all other Webpack plugins
// module.exports = withBundleAnalyzer(withSentryConfig(moduleExports, sentryWebpackPluginOptions))
// module.exports = withBundleAnalyzer(moduleExports)

module.exports = moduleExports

const withBundleAnalyzer = require('@next/bundle-analyzer')({
  enabled: process.env.ANALYZE === 'true'
})
module.exports = withBundleAnalyzer(moduleExports)