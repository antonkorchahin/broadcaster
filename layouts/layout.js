import MainMenu from "@/components/MainMenu"
import {siteUrl} from "@/helpers/helpers"
import Head from "next/head"
import Footer from "@/components/Footer"

const Layout = ({children, title = "", description = "", withGap = true}) => {
  const meta_title = title + ' | Zeno.FM'

  return (
    <>
      <Head>
        <meta charSet="utf-8"/>
        <meta httpEquiv="X-UA-Compatible" content="IE=edge"/>
        <meta name="viewport"
              content="minimum-scale=1, maximum-scale=5, initial-scale=1, width=device-width, shrink-to-fit=no, viewport-fit=cover"/>

        <title>{meta_title}</title>
        <meta name="description" content={description}/>

        <link rel="preconnect" href="https://www.google-analytics.com"/>
        <link rel="dns-prefetch" href="https://www.google-analytics.com"/>

        <link rel="preconnect" href="https://www.googletagmanager.com/"/>
        <link rel="dns-prefetch" href="https://www.googletagmanager.com/"/>
        <link rel="preconnect" href="https://googlesyndication.com/"/>
        <link rel="dns-prefetch" href="https://googlesyndication.com/"/>
        <link rel="preconnect" href="https://googleadservices.com/"/>
        <link rel="dns-prefetch" href="https://googleadservices.com/"/>
        <link rel="preconnect" href="https://google.com/"/>
        <link rel="dns-prefetch" href="https://google.com/"/>

        <meta name="application-name" content="Zeno.FM"/>
        <meta name="apple-mobile-web-app-title" content="Zeno.FM"/>
        <meta name="apple-mobile-web-app-capable" content="yes"/>

        <meta name="format-detection" content="telephone=no"/>
        <meta name="mobile-web-app-capable" content="yes"/>
        <meta name="msapplication-config" content="/static/browserconfig.xml"/>
        <meta name="msapplication-TileColor" content="#F5AB19"/>
        <meta name="msapplication-tap-highlight" content="no"/>

        <meta name="msapplication-TileImage" content="/static/icons/ms-icon-144x144.png"/>
        <meta name="theme-color" content="#F5AB19"/>
        <meta name="msapplication-navbutton-color" content="#F5AB19"/>
        <meta name="apple-mobile-web-app-status-bar-style" content="black-translucent"/>
        <meta name="msapplication-starturl" content="/"/>

        <link rel="apple-touch-icon" sizes="57x57" href={siteUrl("/static/icons/apple-icon-57x57.png")}/>
        <link rel="apple-touch-icon" sizes="60x60" href={siteUrl("/static/icons/apple-icon-60x60.png")}/>
        <link rel="apple-touch-icon" sizes="72x72" href={siteUrl("/static/icons/apple-icon-72x72.png")}/>
        <link rel="apple-touch-icon" sizes="76x76" href={siteUrl("/static/icons/apple-icon-76x76.png")}/>
        <link rel="apple-touch-icon" sizes="114x114" href={siteUrl("/static/icons/apple-icon-114x114.png")}/>
        <link rel="apple-touch-icon" sizes="120x120" href={siteUrl("/static/icons/apple-icon-120x120.png")}/>
        <link rel="apple-touch-icon" sizes="144x144" href={siteUrl("/static/icons/apple-icon-144x144.png")}/>
        <link rel="apple-touch-icon" sizes="152x152" href={siteUrl("/static/icons/apple-icon-152x152.png")}/>
        <link rel="apple-touch-icon" sizes="180x180" href={siteUrl("/static/icons/apple-icon-180x180.png")}/>
        <link rel="apple-touch-icon" sizes="512x512" href={siteUrl("/static/icons/apple-icon-512x512.png")}/>

        <link rel="icon" type="image/png" sizes="192x192" href={siteUrl("/static/icons/android-icon-192x192.png")}/>
        <link rel="icon" type="image/png" sizes="32x32" href={siteUrl("/static/icons/favicon-32x32.png")}/>
        <link rel="icon" type="image/png" sizes="96x96" href={siteUrl("/static/icons/favicon-96x96.png")}/>
        <link rel="icon" type="image/png" sizes="16x16" href={siteUrl("/static/icons/favicon-16x16.png")}/>

        <link rel="manifest" href={siteUrl("/static/manifest.json")}/>

        <link rel="mask-icon" href={siteUrl("/static/icons/safari-pinned-tab.svg")} color="#5bbad5"/>
        <link rel="shortcut icon" href={siteUrl("/favicon.ico")}/>
      </Head>

      <div className={'flex flex-col min-h-screen ' + (withGap ? 'sm:space-y-8 space-y-4' : '')}>
        <div className="flex-grow flex flex-col">
          <MainMenu/>

          <main className="flex-grow flex flex-col">
            {children}
          </main>
        </div>

        <Footer/>
      </div>
    </>
  )
}

export default Layout