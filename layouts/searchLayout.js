import {searchLink, siteUrl} from "@/helpers/helpers"
import {pageViewEvent} from "@/lib/ga"
import {BsX} from "react-icons/bs"
import Link from "next/link"
import {useRouter} from "next/router"
import Layout from "./layout"
import React, {useEffect, useState} from "react"
import {BiSliderAlt} from "react-icons/bi"
import useSearchStore from "@/store/useSearchStore"

const SearchLayout = ({children, title = "default title", description = "default-description"}) => {
  const {showFilters, setShowFilters} = useSearchStore((state) => state)

  const router = useRouter()

  let {query = false} = router.query

  const params = query ? {query: query} : null

  useEffect(() => {
    pageViewEvent("search", siteUrl(router.asPath))
  }, [router.asPath])

  const [searchTerm, setSearchTerm] = useState("")
  const [timer, setTimer] = useState(0)

  function debounce(query, timeout = 500) {
    clearTimeout(timer)
    setTimer(setTimeout(() => {
      const queryString = router.query

      if (query) {
        queryString.query = query
      } else {
        delete queryString.query
      }

      router.push({pathname: router.pathname + "/", query: queryString})
    }, timeout))
  }

  const doSearch = (event) => {
    setSearchTerm(event.target.value)
    debounce(event.target.value)
  }

  useEffect(() => {
    if (router.isReady) {
      if (typeof router.query.query !== "undefined" && router.query.query.length) {
        setSearchTerm(router.query.query)
      }
    }
  }, [router])

  const resetSearch = () => {
    setSearchTerm("")
    clearTimeout(timer)

    const queryString = router.query
    delete queryString.query

    router.push({pathname: router.pathname + "/", query: queryString})
  }

  return (
    <Layout title={title} description={description}>
      <div className={`bg-light-gray` + (!showFilters ? ' border-b border-medium-gray' : '')}>
        <div
          className="container max-w-screen-lg mx-auto px-4 sm:pt-8 pt-4 pb-4">
          <div className="relative">
            <input onChange={doSearch} value={searchTerm} type="text"
                   placeholder="Search by name"
                   className="border-medium-gray focus:ring-1 focus:ring-primary focus:border-transparent form-input pl-4 md:pr-14 pr-10 md:py-4 py-2 md:text-xl text-base w-full rounded-md"/>

            {
              searchTerm.length > 0 &&
              <BsX onClick={resetSearch}
                   className="absolute sm:text-3xl text-xl top-1/4 right-2 cursor-pointer opacity-50"/>
            }
          </div>

          <div className="flex-line mt-4 justify-between sm:text-lg text-normal font-semibold">
            <nav>
              <ul className="flex-line sm:space-x-6 space-x-4">
                <li>
                  <Link href={searchLink("radio", params)} prefetch={false}>
                    <a
                      className={'hover:border-primary pb-1 border-b-2 border-transparent' + (router.route === "/search/radio" ? " border-primary" : "")}>
                      <span>Radio</span>
                    </a>
                  </Link>
                </li>
                <li>
                  <Link href={searchLink("podcasts", params)} prefetch={false}>
                    <a
                      className={'hover:border-primary pb-1 border-b-2 border-transparent' + (router.route === "/search/podcasts" ? " border-primary" : "")}>
                      <span>Podcasts</span>
                    </a>
                  </Link>
                </li>
              </ul>
            </nav>

            {
              router.route !== "/search" &&
              <button
                className={`justify-self-end flex-line pb-1 border-b-2 border-transparent` + (showFilters ? ' border-secondary hover:border-transparent' : ' hover:border-secondary')}
                onClick={() => setShowFilters(!showFilters)}>
                <span className="mr-2 sm:block hidden">Filters</span>
                <BiSliderAlt className="text-2xl"/>
              </button>
            }
          </div>
        </div>
      </div>

      <div>
        {children}
      </div>

    </Layout>
  )
}

export default SearchLayout