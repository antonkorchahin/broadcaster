// tailwind.config.js
const colors = require('tailwindcss/colors')

module.exports = {
  purge: [
    './pages/**/*.{js,ts,jsx,tsx}',
    './layouts/**/*.{js,ts,jsx,tsx}',
    './components/**/*.{js,ts,jsx,tsx}',
  ],
  darkMode: false, // or 'media' or 'class'
  theme: {
    extend: {
      minHeight: (theme) => ({
        ...theme('spacing'),
      }),
      colors: {
        'primary': '#F5AB19',
        'secondary': '#212124',
        'secondary-light': '#333',
        'light-primary': '#fca801',
        'light-secondary': '#101011',
        'light-gray': '#F3F3F3',
        'medium-gray': '#E6E6E6',
        'dark-gray': '#1D1D1F',


        'facebook': '#4267B2',
        'twitter': '#1DA1F2',

        'app': '#28282A',
        'stripe': '#635bff',
        'stripe-hover': '#4A42E6',
        'paypal': '#0070ba',
        'paypal-hover': '#0057A1',
        'cashapp': '#00d64f',
        'cashapp-hover': '#00BD36',

        'zeno-light-blue': '#4aabd8',
        'zeno-dark-blue': '#162027',
      },

      borderColor: {
        'primary': '#F5AB19',
      },
    },
  },
  variants: {
    extend: {
      borderWidth: ['last', 'hover'],
      margin: ['responsive', 'hover', 'last'],
      padding: ['first', 'last'],
      display: ['responsive', 'group-hover'],
      backgroundColor: ['active'],
      backgroundOpacity: ['active'],
      scale: ['active'],
      textColor: ['active'],
    },
  },
  plugins: [
    require('@tailwindcss/forms'),
    require('@tailwindcss/line-clamp')
  ],
}
