ARG BASE_IMAGE=node:16
ARG NODE_ENV=production

FROM $BASE_IMAGE as builder
WORKDIR /app
COPY ./package.json ./
COPY ./package-lock.json ./
RUN CI=true npm ci
COPY . ./
RUN NODE_ENV=$NODE_ENV npm run build

FROM $BASE_IMAGE
WORKDIR /app
COPY --from=builder /app ./
ENV NODE_ENV=$NODE_ENV
ENV NEXT_TELEMETRY_DISABLED=1
EXPOSE 80
CMD ["npm", "run", "start"]
