export const updateProgress = (progressElement) => {
  const min = parseInt(progressElement.min) ?? 0
  const max = parseInt(progressElement.max) ?? 0
  const val = parseInt(progressElement.value) ?? 0

  const progress = max ? ((val - min) * 100 / (max - min)) : 0

  progressElement.style.backgroundSize = progress + '% 100%'
}