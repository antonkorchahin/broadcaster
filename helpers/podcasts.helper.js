import {siteUrl} from "@/helpers/helpers"

export const podcastUrl = (identifier = null, isAMP = false) => {
  if (!identifier) {
    return siteUrl("/podcasts/")
  }

  return siteUrl(`/podcast/${identifier}/`, isAMP)
}

export const countryLink = (country = null) => {
  let url = "/podcasts/countries/"

  if (country) {
    url = url + `${encodeURIComponent(country.name)}/`
  }

  return siteUrl(url)
}

export const languageLink = (language = null) => {
  let url = "/podcasts/languages/"

  if (language) {
    url = url + `${encodeURIComponent(language.name)}/`
  }

  return siteUrl(url)
}

export const categoryLink = (category = null) => {
  let url = "/podcasts/categories/"

  if (category) {
    url = url + `${encodeURIComponent(category.name)}/`
  }

  return siteUrl(url)
}

export const countryCategoryLink = (country, category) => {
  let url = `/podcasts/country-category/${encodeURIComponent(country)}/${encodeURIComponent(category)}/`

  return siteUrl(url)
}

export const languageCategoryLink = (language, category) => {
  let url = `/podcasts/language-category/${encodeURIComponent(language)}/${encodeURIComponent(category)}/`

  return siteUrl(url)
}