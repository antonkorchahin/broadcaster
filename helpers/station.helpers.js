import {siteUrl} from "@/helpers/helpers"
import {getOptimizedImage} from "@/helpers/images.helper"

export const getRadioLogo = (key, width = 270, height = 270, updated_at = null) => {
  let imageUrl = `https://stream-tools.zenomedia.com/content/stations/${key}/image/`

  if (typeof updated_at !== "undefined" && updated_at) {
    imageUrl += `?updated=${updated_at}`
  }

  return getOptimizedImage(imageUrl, width, height)
}

export const getRadioBackground = (key, width = 0, height = 0, updated_at = null) => {
  let imageUrl = `https://stream-tools.zenomedia.com/content/stations/${key}/microsite/background_image/`

  if (typeof updated_at !== "undefined" && updated_at) {
    imageUrl += `?updated=${updated_at}`
  }

  return getOptimizedImage(imageUrl, width, height)
}

export const radioURL = (slug = null, isAMP = false) => {
  let path = "/radio/"

  if (slug) {
    path += `${slug}/`
  }

  return siteUrl(path, isAMP)
}

export const streamURL = (streamName) => {
  if (!streamName) {
    return null
  }

  return `https://stream.zeno.fm/${streamName}`
}

export const countryLink = (country = null) => {
  let url = "/radio/countries/"

  if (country) {
    url = url + `${encodeURIComponent(country.name)}/`
  }

  return siteUrl(url)
}

export const languageLink = (language = null) => {
  let url = "/radio/languages/"

  if (language) {
    url = url + `${encodeURIComponent(language.name)}/`
  }

  return siteUrl(url)
}

export const genreLink = (genre = null) => {
  let url = "/radio/genres/"

  if (genre) {
    url = url + `${encodeURIComponent(genre.name)}/`
  }

  return siteUrl(url)
}

export const countryGenreLink = (country, genre) => {
  let url = `/radio/country-genre/${encodeURIComponent(country)}/${encodeURIComponent(genre)}/`

  return siteUrl(url)
}

export const languageGenreLink = (language, genre) => {
  let url = `/radio/language-genre/${encodeURIComponent(language)}/${encodeURIComponent(genre)}/`

  return siteUrl(url)
}

