import {siteUrl} from "@/helpers/helpers"
import {getDescription} from "@/helpers/description.helper"

export const episodeUrl = (podcastIdentifier, episodeIdentifier, isAMP = false) => {
  return siteUrl(`/podcast/${podcastIdentifier}/episodes/${episodeIdentifier}/`, isAMP)
}

export const getEpisodeAuthor = (author = null) => {
  if (!author || !author.length || author === "Unknown") {
    return null
  }

  return author
}

export const getEpisodeSummary = ({summary = null, description = null}) => {
  if (!summary || !summary.length) {
    if (description || description.length) {
      summary = description
    }
  }

  summary = getDescription(summary, true)

  if (summary) {
    if (summary.length > 300) {
      summary = summary.substring(0, 300) + '...'
    }

    return summary
  }

  return null
}