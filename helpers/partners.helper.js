import {siteUrl} from "@/helpers/helpers"

export const partnerUrl = (slug = null) => {
  let path = "/partners/"

  if (slug) {
    path += `${slug}/`
  }

  return siteUrl(path)
}