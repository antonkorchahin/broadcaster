import {BASE_URL} from "@/api/index"

export const logError = (e, context = null) => {
  if (context) {
    console.log(context)
  }

  console.log(e)
}

export const siteUrl = (path = "/", isAMP = false) => {
  return BASE_URL + `${isAMP ? "/amp" : ""}${path}`
}

export const searchLink = (type, params = null) => {
  let str = ""

  if (params) {
    const searchParams = new URLSearchParams(params)
    str = "?" + searchParams.toString()
  }

  return siteUrl(`/search/${type}/${str}`)
}

export const discoverLink = () => {
  return siteUrl(`/discover/`)
}

