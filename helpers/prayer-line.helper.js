import {siteUrl} from "@/helpers/helpers"

export const prayerLineUrl = (identifier = null, isAMP = false) => {
  let path = "/prayer-lines/"

  if (identifier) {
    path += `${identifier}/`
  }

  return siteUrl(path, isAMP)
}

export const religionLink = (religion = null) => {
  let url = "/prayer-lines/religions/"

  if (religion) {
    url = url + `${encodeURIComponent(religion.name)}/`
  }

  return siteUrl(url)
}