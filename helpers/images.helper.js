export const getOptimizedImage = (imageURL, width = 270, height = 270) => {
  if (typeof imageURL === "undefined" || !imageURL || !imageURL.length) {
    return null
  }

  const createHmac = require("create-hmac")

  const KEY = "c00eca2bc3a3f7c19b50882db2c7efba9dc87d83c899c3c2bd557d1983f916dfff339f503d9ae849e3c37c1da9c12833d4ff01ce60c0e4906bf671f47438695e"
  const SALT = "d681a205b59b687c29d27f2738d893dc714b7c797ed2c09068d5389d8c6f052f5356adee520176950042fd2aeca3561a988805bc8fdda823e847dc1c78454e45"

  const urlSafeBase64 = (string) => {
    return Buffer.from(string).toString("base64").replace(/=/g, "").replace(/\+/g, "-").replace(/\//g, "_")
  }

  const hexDecode = (hex) => Buffer.from(hex, "hex")

  const sign = (salt, target, secret) => {
    const hmac = createHmac("sha256", hexDecode(secret))
    hmac.update(hexDecode(salt))
    hmac.update(target)
    return urlSafeBase64(hmac.digest())
  }

  const extension = "webp"
  const encoded_url = urlSafeBase64(imageURL)
  const path = `/rs:fit:${width}:${height}/g:ce:0:0/${encoded_url}.${extension}`

  const signature = sign(SALT, path, KEY)

  return `https://images.zeno.fm/${signature}${path}`
}