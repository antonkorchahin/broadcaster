import sanitizeHtml from "sanitize-html"

export const getDescription = (description, clean = false) => {
  if (typeof description === "undefined" || !description || !description.length) {
    return null
  }

  if (description.indexOf('<br/>') === -1) {
    description = description.replace(/\n/g, "<br/>")
  }

  const allowedTags = clean ? [] : [
    "div", "ol", "p", "ul", "b", "br", "em", "i", "span", "strong"
  ]

  return sanitizeHtml(description, {
    allowedTags: allowedTags,
    disallowedTagsMode: 'discard',
    allowedAttributes: {},
  })
}