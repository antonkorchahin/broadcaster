importScripts('sw-toolbox.js');

toolbox.precache([
  '/wp-content/themes/zeno_new/css/style.min.css',
  '/wp-content/themes/zeno_new/js/jquery.min.js',
  '/wp-content/themes/zeno_new/js/migrate.min.js',
  '/wp-content/themes/zeno_new/js/library.min.js',
  '/wp-content/themes/zeno_new/js/script.min.js'
]);

toolbox.router.get('/upload/*', toolbox.cacheFirst);

toolbox.router.get('*', toolbox.networkFirst, {
  networkTimeoutSeconds: 5
});