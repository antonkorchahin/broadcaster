export const GA_TRACKING_ID = process.env.NEXT_PUBLIC_GOOGLE_ANALYTICS

export const page_view = (url) => {
  if (typeof window !== "undefined" && typeof window.gtag !== "undefined") {
    window.gtag("config", GA_TRACKING_ID, {
      page_path: url
    })
  }
}

export const event = ({action, params}) => {
  if (typeof window !== "undefined" && typeof window.gtag !== "undefined") {
    window.gtag("event", action, params)
  }
}

export const pageViewEvent = (name, url) => {
  event({
    action: name,
    params: {
      event_category: "page_view",
      event_label: url
    }
  })
}

export const pageViewByStationCountryEvent = (country, url) => {
  if (country) {
    event({
      action: country,
      params: {
        event_category: "page_view_by_station_country",
        event_label: url
      }
    })
  }
}

export const radioPause = (url) => {
  event({
    action: "radio_pause",
    params: {
      event_category: "radio_player",
      event_label: url
    }
  })
}

export const radioStart = (url) => {
  event({
    action: "radio_start",
    params: {
      event_category: "radio_player",
      event_label: url
    }
  })
}

export const radioPlayerPause = (url) => {
  event({
    action: "radio_player_pause",
    params: {
      event_category: "radio_player",
      event_label: url
    }
  })
}

export const radioPlayerStart = (url) => {
  event({
    action: "radio_player_start",
    params: {
      event_category: "radio_player",
      event_label: url
    }
  })
}

export const radioPlayerVolumeMute = (url) => {
  event({
    action: "radio_player_volume_mute",
    params: {
      event_category: "radio_player",
      event_label: url
    }
  })
}

export const radioPlayerVolumeUnmute = (url) => {
  event({
    action: "radio_player_volume_unmute",
    params: {
      event_category: "radio_player",
      event_label: url
    }
  })
}

export const radioPlayerVolumeChange = (url, value) => {
  event({
    action: "radio_player_volume_change",
    params: {
      event_category: "radio_player",
      event_label: url,
      value: value
    }
  })
}

export const episodeStart = (url) => {
  event({
    action: "episode_start",
    params: {
      event_category: "episode_player",
      event_label: url
    }
  })
}

export const episodePause = (url) => {
  event({
    action: "episode_pause",
    params: {
      event_category: "episode_player",
      event_label: url
    }
  })
}

export const episodePlayerStart = (url) => {
  event({
    action: "episode_player_start",
    params: {
      event_category: "episode_player",
      event_label: url
    }
  })
}

export const episodePlayerPause = (url) => {
  event({
    action: "episode_player_pause",
    params: {
      event_category: "episode_player",
      event_label: url
    }
  })
}

export const episodePlayerSkipForward = (url) => {
  event({
    action: "episode_player_skip_forward",
    params: {
      event_category: "episode_player",
      event_label: url
    }
  })
}

export const episodePlayerSkipBackward = (url) => {
  event({
    action: "episode_player_skip_backward",
    params: {
      event_category: "episode_player",
      event_label: url
    }
  })
}

export const episodePlayerSeeked = (url) => {
  event({
    action: "episode_player_seeked",
    params: {
      event_category: "episode_player",
      event_label: url
    }
  })
}

export const episodePlayerVolumeMute = (url) => {
  event({
    action: "episode_player_volume_mute",
    params: {
      event_category: "episode_player",
      event_label: url
    }
  })
}

export const episodePlayerVolumeUnmute = (url) => {
  event({
    action: "episode_player_volume_unmute",
    params: {
      event_category: "episode_player",
      event_label: url
    }
  })
}

export const episodePlayerVolumeChange = (url, value) => {
  event({
    action: "episode_player_volume_change",
    params: {
      event_category: "episode_player",
      event_label: url,
      value: value
    }
  })
}

export const prayerLineGetStartedClick = (url) => {
  event({
    action: "get_started_click",
    params: {
      event_category: "prayer_line",
      event_label: url
    }
  })
}
