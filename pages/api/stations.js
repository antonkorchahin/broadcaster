import {RadioRepository} from "@/repositories/RadioRepository"

export default async function handler(req, res) {
  const results = await RadioRepository().search(req.query)
  res.status(200).json(results)
}