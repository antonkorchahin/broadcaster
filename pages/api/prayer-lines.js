import {PrayerLineRepository} from "@/repositories/PrayerLineRepository"

export default async function handler(req, res) {
  const results = await PrayerLineRepository().search(req.query)
  res.status(200).json(results)
}