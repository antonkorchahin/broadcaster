import {EpisodeRepository} from "@/repositories/EpisodeRepository"

export default async function handler(req, res) {
  const podcast = {
    key: req.query.podcastKey,
    identifier: req.query.podcastIdentifier,
  }

  const results = await EpisodeRepository(podcast).search(req.query)
  res.status(200).json(results)
}