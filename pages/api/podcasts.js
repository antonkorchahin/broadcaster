import {PodcastRepository} from "@/repositories/PodcastRepository"

export default async function handler(req, res) {
  const results = await PodcastRepository().search(req.query)
  res.status(200).json(results)
}