import Document, {Html, Head, Main, NextScript} from 'next/document'
import {GA_TRACKING_ID} from "@/lib/ga"

class MyDocument extends Document {
  static async getInitialProps(ctx) {
    const initialProps = await Document.getInitialProps(ctx)
    return {...initialProps}
  }

  render() {
    return (
      <Html lang="en">
        <Head/>
        <body className="consent">
        <Main/>
        <NextScript/>

        <script
          dangerouslySetInnerHTML={{
            __html: `
            window.dataLayer = window.dataLayer || [];
            function gtag(){dataLayer.push(arguments);}
            gtag('js', new Date());
            
            gtag('config', '${GA_TRACKING_ID}');
          `,
          }}
        />
        </body>
      </Html>
    )
  }
}

export default MyDocument