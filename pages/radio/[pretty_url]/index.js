import StationCards from "@/components/Stations/StationCards"
import StationInfo from "@/components/Stations/StationInfo"
import StationRecommendedStations from "@/components/Stations/StationRecommendedStations"
import {getRadioLogo, radioURL} from "@/helpers/station.helpers"
import Head from "next/head"
import React, {useEffect} from "react"
import Layout from "@/layouts/layout"
import {RadioRepository} from "@/repositories/RadioRepository"
import StationNavbar from "@/components/Stations/StationNavbar"
import {pageViewByStationCountryEvent, pageViewEvent} from "@/lib/ga"
import Script from "next/script"
import useCookieConsentStore from "@/store/useCookieConsentStore"


const Station = ({station, stationCards, recommendedStationsList, meta}) => {
  const consentRequired = useCookieConsentStore((state) => state.computed.consentRequired)
  const consentFinished = useCookieConsentStore((state) => state.consentFinished)
  const targetingScriptsAllowed = useCookieConsentStore((state) => state.computed.targetingEnabled)

  useEffect(() => {
    pageViewEvent("radio", station.url)
    pageViewByStationCountryEvent(station.country, station.url)
  }, [station.url, station.country])

  return (
    <Layout title={meta.title} description={meta.description} withGap={!station.featured}>
      <Head>
        <link rel="preconnect" href="https://zenoplay.zenomedia.com"/>

        <meta itemProp="name" content={meta.title}/>
        <meta itemProp="description" content={meta.description}/>
        <meta itemProp="image" content={meta.logo}/>

        <link rel="canonical" href={meta.canonical}/>
        <link rel="amphtml" href={meta.amp}/>

        <meta name="twitter:card" content="player"/>
        <meta name="twitter:title" content={meta.title}/>
        <meta name="twitter:site" content="@Zeno_Media"/>
        <meta name="twitter:description" content={meta.description}/>
        <meta name="twitter:image" content={meta.logo}/>
        <meta name="twitter:image:alt" content={meta.title}/>

        <meta name="twitter:player" content={`https://zeno.fm/player.php/player.php?stream=${station.streamURL}`}/>
        <meta name="twitter:player:stream" content={station.streamURL}/>
        <meta name="twitter:player:stream:content_type" content="audio/aac"/>
        <meta name="twitter:player:width" content="480"/>
        <meta name="twitter:player:height" content="50"/>

        <meta property="og:site_name" content="Zeno.FM"/>
        <meta property="og:title" content={meta.title}/>
        <meta property="og:type" content="music.radio_station"/>
        <meta property="og:url" content={station.url}/>
        <meta property="og:description" content={meta.description}/>
        <meta property="og:audio" content={station.streamURL}/>
        <meta property="og:image" content={meta.logo}/>

        <meta name="apple-itunes-app"
              content={`app-id=1207197757, app-argument=https://zenoplay.zenomedia.com/share/ZenoFM?referrer=zeno.fm&lang=eng&stationId=${station.key}`}/>

      </Head>

      {
        targetingScriptsAllowed &&
        <template>
          <Script
            onError={(e) => {
              console.error("Failed to load Google AdSense script", e);
            }}
            strategy="afterInteractive"
            src={`https://pagead2.googlesyndication.com/pagead/js/adsbygoogle.js?client=${process.env.NEXT_PUBLIC_GOOGLE_ADSENSE}`}
            crossOrigin="anonymous"
          />

          <Script src="https://securepubads.g.doubleclick.net/tag/js/gpt.js" strategy="afterInteractive"/>
          <Script id="doubleclick-script" strategy="afterInteractive">
            {`
              window.googletag = window.googletag || {cmd: []};
              googletag.cmd.push(function() {
                googletag.defineSlot('/21869305879/ZenoFM_970x250', [970, 250], 'div-gpt-ad-1664481770557-0').addService(googletag.pubads());
                googletag.pubads().enableSingleRequest();
                googletag.enableServices();
              });              
            `}
          </Script>

          <Script strategy="afterInteractive" src="https://imasdk.googleapis.com/js/sdkloader/ima3.js"></Script>
          <Script strategy="afterInteractive" src="https://play.adtonos.com/attc-uAdJ2ujapxW6xDFKk.min.js"></Script>
        </template>
      }

      {
        consentRequired === false &&
        <>
          <Script strategy="afterInteractive" src="https://synchrobox.adswizz.com/register2.php"/>
          <Script strategy="afterInteractive" src="https://cdn.adswizz.com/adswizz/js/SynchroClient2.js"/>
          <Script strategy="afterInteractive"
                  src="https://playerservices.live.streamtheworld.com/api/idsync.js?stationId=118143"/>
        </>
      }

      {
        (consentRequired && consentFinished) &&
        <>
          <Script strategy="afterInteractive"
                  src={`https://synchrobox.adswizz.com/register2.php?aw_0_req.gdpr=${targetingScriptsAllowed}`}/>
          <Script strategy="afterInteractive"
                  src={`https://cdn.adswizz.com/adswizz/js/SynchroClient2.js?aw_0_req.gdpr=${targetingScriptsAllowed}`}/>
          <Script strategy="afterInteractive"
                  src="https://playerservices.live.streamtheworld.com/api/idsync.js?stationId=118143"/>
        </>
      }

      {
        (station.hasPodcasts || station.hasVideo) &&
        <StationNavbar station={station}/>
      }

      <StationInfo station={station}/>

      {
        stationCards &&
        <StationCards station={station} stationCards={stationCards}/>
      }

      {
        (!station.featured && station.slug === 'sampleradio') &&
        <div className="bg-light-gray">
          <div className="container max-w-screen-lg mx-auto px-4 py-8">
            <div id='div-gpt-ad-1664481770557-0' style={{width: "970px", height: "250px"}}>
              <Script id="doubleclick-ad-unit" strategy="lazyOnload">
                {`googletag.cmd.push(function() { googletag.display('div-gpt-ad-1664481770557-0'); });`}
              </Script>
            </div>
          </div>
        </div>
      }

      {
        (recommendedStationsList && recommendedStationsList.length > 0 && !station.featured) &&
        <StationRecommendedStations station={station} recommended={recommendedStationsList}/>
      }
    </Layout>
  )
}

export default Station

export async function getStaticProps(context) {
  let station = await RadioRepository().get(context.params.pretty_url)

  if (!station) {
    return {
      notFound: true
    }
  }

  let stationCards = await RadioRepository().getCards(station.key)

  const recommendedStationsList = await RadioRepository().getRecommendedStations(station)

  const meta = {
    title: `Listen to ${station.name}`,
    description: `Listen to ${station.name} for the best ` + (station.languages.length ? station.languages.join(", ") : '') + ` ${station.genre} radio. Listen live, catch up on old episodes and keep up to date with announcements.`,
    logo: getRadioLogo(station.key, 500, 500, station.updated),
    canonical: station.url,
    amp: radioURL(station.slug, true)
  }

  return {
    props: {
      station,
      stationCards,
      recommendedStationsList,
      meta
    },
    revalidate: 60
  }
}

export async function getStaticPaths() {
  return {
    paths: [],
    fallback: "blocking"
  }
}