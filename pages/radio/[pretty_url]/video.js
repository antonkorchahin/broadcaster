import {getRadioLogo} from "@/helpers/station.helpers"
import {pageViewEvent} from "@/lib/ga"
import {searchLink} from "@/helpers/helpers"
import Head from "next/head"
import Layout from "@/layouts/layout"
import React, {useEffect} from "react"
import Link from "next/link"
import {FiHash} from "react-icons/fi"
import {FiMapPin} from "react-icons/fi"
import {FiMic} from "react-icons/fi"
import StationNavbar from "@/components/Stations/StationNavbar"
import {RadioRepository} from "@/repositories/RadioRepository"

const Video = ({station, meta}) => {
  useEffect(() => {
    pageViewEvent("video", meta.canonical)
  }, [meta.canonical])

  return (
    <Layout title={meta.title} description={meta.description}>

      <Head>
        <meta itemProp="name" content={meta.title}/>
        <meta itemProp="description" content={meta.description}/>
        <meta itemProp="image" content={meta.logo}/>

        <link rel="canonical" href={meta.canonical}/>

        <meta property="og:site_name" content="Zeno.FM"/>
        <meta property="og:title" content={meta.title}/>
        <meta property="og:type" content="website"/>
        <meta property="og:url" content={meta.canonical}/>
        <meta property="og:description" content={meta.description}/>
        <meta property="og:image" content={meta.logo}/>

        <meta name="twitter:card" content="summary_large_image"/>
        <meta name="twitter:site" content="@Zeno_Media"/>
        <meta name="twitter:title" content={meta.title}/>
        <meta name="twitter:description" content={meta.description}/>
        <meta name="twitter:image" content={meta.logo}/>
        <meta name="twitter:image:alt" content={meta.title}/>
      </Head>

      {
        (station.hasPodcasts || station.hasVideo) &&
        <StationNavbar station={station}/>
      }

      <div className="container sm:mt-8 mt-4 max-w-screen-lg mx-auto px-4 min-h-96">

        <div className="lg:flex flex-row-reverse block">

          <div className={station.featured ? "w-full" : "lg:w-9/12 w-full"}>
            <div style={{position: "relative", paddingBottom: "56.25%", height: 0, overflow: "hidden"}}>
              <iframe
                style={{
                  width: "100%",
                  height: "100%",
                  position: "absolute",
                  left: "0px",
                  top: "0px",
                  overflow: "hidden"
                }}
                frameBorder="0"
                type="text/html" src={`https://www.dailymotion.com/embed/video/${station.video.identifier}`}
                width="100%"
                height="100%" allowFullScreen></iframe>
            </div>

            <h1 className="md:text-3xl font-semibold text-xl break-words my-4">
              {station.name}
            </h1>

            {
              station.description &&
              <div itemProp="description" className="description" dangerouslySetInnerHTML={{
                __html: station.description
              }}/>
            }

            <div className="sm:text-sm text-xs mt-4 flex flex-wrap">
              {
                station.genre &&
                <Link href={searchLink("radio", {genre: station.genre})} prefetch={false}>
                  <a className="tag">
                    <span>
                      <FiHash className="w-4"/>
                      {station.genre}
                    </span>
                  </a>
                </Link>
              }

              {
                (station.country || station.city) &&
                <Link href={searchLink("radio", {country: station.country})} prefetch={false}>
                  <a className="tag">
                    <span>
                      <FiMapPin className="w-4"/>
                      <span itemProp="location">{station.city ? station.city + ", " : ""} {station.country}</span>
                    </span>
                  </a>
                </Link>
              }

              {
                station.languages.length > 0 &&
                station.languages.map(language => (
                  <Link href={searchLink("radio", {language: language})} key={language} prefetch={false}>
                    <a className="tag">
                      <span>
                        <FiMic className="w-4"/>
                        <span itemProp="knowsLanguage">{language}</span>
                      </span>
                    </a>
                  </Link>
                ))
              }
            </div>
          </div>
        </div>
      </div>
    </Layout>
  )
}

export default Video

export async function getStaticProps(context) {
  let station = await RadioRepository().get(context.params.pretty_url)

  if (!station || !station.hasVideo) {
    return {
      notFound: true
    }
  }

  const meta = {
    title: `Live stream from ${station.name}`,
    description: `Live stream from ${station.name}`,
    logo: getRadioLogo(station.key, 500, 500, station.updated),
    canonical: station.url + "video/"
  }

  return {
    props: {
      station,
      meta,
    },
    revalidate: 60
  }
}

export async function getStaticPaths() {
  return {
    paths: [],
    fallback: "blocking"
  }
}