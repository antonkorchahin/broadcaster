import React from "react"
import {RadioRepository} from "@/repositories/RadioRepository"

const Shows = () => {
  return null
}

export default Shows

export async function getStaticProps(context) {
  let station = await RadioRepository().get(context.params.pretty_url)

  if (!station || !station.hasPodcasts) {
    return {
      notFound: true
    }
  }

  return {
    redirect: {
      destination: station.url + "podcasts/", permanent: true
    }
  }
}

export async function getStaticPaths() {
  return {
    paths: [],
    fallback: "blocking"
  }
}