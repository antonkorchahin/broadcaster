import {getRadioLogo} from "@/helpers/station.helpers"
import {pageViewEvent} from "@/lib/ga"
import Head from "next/head"
import Layout from "@/layouts/layout"
import React, {useEffect} from "react"
import {PodcastRepository} from "@/repositories/PodcastRepository"
import Container from "@/components/common/Container"
import ListGrid from "@/components/ListGrid"
import StationNavbar from "@/components/Stations/StationNavbar"
import {RadioRepository} from "@/repositories/RadioRepository"

const Podcasts = ({station, podcasts, meta}) => {
  useEffect(() => {
    pageViewEvent("radio_podcasts", meta.canonical)
  }, [meta.canonical])

  return (
    <Layout title={meta.title} description={meta.description}>

      <Head>
        <meta itemProp="name" content={meta.title}/>
        <meta itemProp="description" content={meta.description}/>
        <meta itemProp="image" content={meta.logo}/>

        <link rel="canonical" href={meta.canonical}/>

        <meta property="og:site_name" content="Zeno.FM"/>
        <meta property="og:title" content={meta.title}/>
        <meta property="og:type" content="website"/>
        <meta property="og:url" content={meta.canonical}/>
        <meta property="og:description" content={meta.description}/>
        <meta property="og:image" content={meta.logo}/>

        <meta name="twitter:card" content="summary_large_image"/>
        <meta name="twitter:site" content="@Zeno_Media"/>
        <meta name="twitter:title" content={meta.title}/>
        <meta name="twitter:description" content={meta.description}/>
        <meta name="twitter:image" content={meta.logo}/>
        <meta name="twitter:image:alt" content={meta.title}/>
      </Head>

      {
        (station.hasPodcasts || station.hasVideo) &&
        <StationNavbar station={station}/>
      }

      <Container>
        <div className="sm:mt-8 mt-4">
          <ListGrid items={podcasts}/>
        </div>
      </Container>
    </Layout>
  )
}

export default Podcasts

export async function getStaticProps(context) {
  let station = await RadioRepository().get(context.params.pretty_url)

  if (!station || !station.hasPodcasts) {
    return {
      notFound: true
    }
  }

  const podcasts = await PodcastRepository().stationPodcasts(station.key)

  if (!podcasts.length) {
    return {
      notFound: true
    }
  }

  const meta = {
    title: `Recent Episodes from ${station.name}`,
    description: "Check out all the best episodes of your favorite podcasts. Listen now!",
    logo: getRadioLogo(station.key, 500, 500),
    canonical: station.url + "podcasts/"
  }

  return {
    props: {
      station,
      podcasts,
      meta,
    },
    revalidate: 60
  }
}

export async function getStaticPaths() {
  return {
    paths: [],
    fallback: "blocking"
  }
}