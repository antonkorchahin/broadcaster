import Breadcrumbs from "@/components/Breadcrumbs"
import GetStarted from "@/components/GetStarted"
import Section from "@/components/Section"
import StationGenreList from "@/components/Stations/StationGenreList"
import StationListGrid from "@/components/Stations/StationListGrid"
import {discoverLink, searchLink, siteUrl} from "@/helpers/helpers"
import {countryGenreLink, countryLink, radioURL} from "@/helpers/station.helpers"
import Layout from "@/layouts/layout"
import {pageViewByStationCountryEvent, pageViewEvent} from "@/lib/ga"
import Head from "next/head"
import Link from "next/link"
import React, {useEffect} from "react"
import Container from "@/components/common/Container"
import {RadioRepository} from "@/repositories/RadioRepository"

const CountryGenre = ({country, genre, radioGenres, popularRadio, meta, breadcrumbs}) => {
  useEffect(() => {
    pageViewEvent("radio_country_genre", meta.canonical)
    pageViewByStationCountryEvent(country.name, meta.canonical)
  }, [meta.canonical, country.name])

  return (
    <Layout title={meta.title} description={meta.description}>

      <Head>
        <meta itemProp="name" content={meta.title}/>
        <meta itemProp="description" content={meta.description}/>

        <link rel="canonical" href={meta.canonical}/>

        <meta property="og:site_name" content="Zeno.FM"/>
        <meta property="og:title" content={meta.title}/>
        <meta property="og:type" content="website"/>
        <meta property="og:url" content={meta.canonical}/>
        <meta property="og:description" content={meta.description}/>

        <meta name="twitter:card" content="summary_large_image"/>
        <meta name="twitter:site" content="@Zeno_Media"/>
        <meta name="twitter:title" content={meta.title}/>
        <meta name="twitter:description" content={meta.description}/>
      </Head>

      <Breadcrumbs breadcrumbs={breadcrumbs} heading={`${genre.name} Radio Stations from ${country.name}`}/>

      <Container>
        <StationGenreList genres={radioGenres} country={country} genre={genre}/>

        {
          popularRadio.length > 0 &&
          <Section title="Popular Radio Stations"
                   url={searchLink("radio", {genre: genre.name, country: country.name})}>
            <StationListGrid stations={popularRadio}/>
          </Section>
        }

        {
          !popularRadio.length &&
          <div className="text-xl">
            There are no {genre.name} radio stations in {country.name} yet.
            <Link href={siteUrl("/streaming/")} prefetch={false}>
              <a className="ml-2 text-primary underline">
                Create first
              </a>
            </Link>
          </div>
        }
      </Container>

      <GetStarted/>

    </Layout>
  )
}

export default CountryGenre

export async function getStaticProps(context) {
  let countryName = context.params.country
  let genreName = context.params.all.length === 2 ? context.params.all.join("/") : context.params.all[0]

  const countries = await RadioRepository().getCountries()
  const country = countries.find(c => c.name === countryName)

  if (typeof country === "undefined") {
    return {
      notFound: true
    }
  }

  const radioGenres = await RadioRepository().getGenres(10)
  const genre = radioGenres.find(c => c.name === genreName)

  if (typeof genre === "undefined") {
    return {
      notFound: true
    }
  }

  const popularRadio = await RadioRepository().search({
    genre: genre.name,
    country: country.name,
    limit: 24
  })

  const meta = {
    title: `Listen to the best ${genre.name} radio stations from ${country.name}`,
    description: "Find and listen to best radio stations from around the world",
    canonical: countryGenreLink(country.name, genre.name)
  }

  const breadcrumbs = [
    {title: "Discover", url: discoverLink()},
    {title: "Radio", url: radioURL()},
    {title: "Countries", url: countryLink()},
    {title: country.name, url: countryLink(country)},
    {title: genre.name}
  ]

  return {
    props: {
      country,
      genre,
      radioGenres,
      popularRadio,
      meta,
      breadcrumbs
    },
    revalidate: 60
  }
}

export async function getStaticPaths() {
  return {
    paths: [],
    fallback: "blocking"
  }
}