import Breadcrumbs from "@/components/Breadcrumbs"
import GetStarted from "@/components/GetStarted"
import Section from "@/components/Section"
import StationGenreList from "@/components/Stations/StationGenreList"
import StationListGrid from "@/components/Stations/StationListGrid"
import {discoverLink, searchLink, siteUrl} from "@/helpers/helpers"
import {languageGenreLink, languageLink, radioURL} from "@/helpers/station.helpers"
import Layout from "@/layouts/layout"
import {pageViewEvent} from "@/lib/ga"
import Head from "next/head"
import Link from "next/link"
import React, {useEffect} from "react"
import Container from "@/components/common/Container"
import {RadioRepository} from "@/repositories/RadioRepository"

const LanguageGenre = ({language, genre, radioGenres, popularRadio, meta, breadcrumbs}) => {
  useEffect(() => {
    pageViewEvent("radio_language_genre", meta.canonical)
  }, [meta.canonical])

  return (
    <Layout title={meta.title} description={meta.description}>

      <Head>
        <meta itemProp="name" content={meta.title}/>
        <meta itemProp="description" content={meta.description}/>

        <link rel="canonical" href={meta.canonical}/>

        <meta property="og:site_name" content="Zeno.FM"/>
        <meta property="og:title" content={meta.title}/>
        <meta property="og:type" content="website"/>
        <meta property="og:url" content={meta.canonical}/>
        <meta property="og:description" content={meta.description}/>

        <meta name="twitter:card" content="summary_large_image"/>
        <meta name="twitter:site" content="@Zeno_Media"/>
        <meta name="twitter:title" content={meta.title}/>
        <meta name="twitter:description" content={meta.description}/>
      </Head>

      <Breadcrumbs breadcrumbs={breadcrumbs} heading={`${language.name} Speaking ${genre.name} Radio Stations`}/>

      <Container>
        <StationGenreList genres={radioGenres} language={language} genre={genre}/>

        {
          popularRadio.length > 0 &&
          <Section title="Popular Radio Stations"
                   url={searchLink("radio", {genre: genre.name, language: language.name})}>
            <StationListGrid stations={popularRadio}/>
          </Section>
        }

        {
          !popularRadio.length &&
          <div className="text-xl">
            There are no {language.name} speaking {genre.name} radio stations yet.
            <Link href={siteUrl("/streaming/")} prefetch={false}>
              <a className="ml-2 text-primary underline">
                Create first
              </a>
            </Link>
          </div>
        }
      </Container>

      <GetStarted/>

    </Layout>
  )
}

export default LanguageGenre

export async function getStaticProps(context) {
  let languageName = context.params.language
  let genreName = context.params.all.length === 2 ? context.params.all.join("/") : context.params.all[0]

  const languages = await RadioRepository().getLanguages()
  const language = languages.find(c => c.name === languageName)

  if (typeof language === "undefined") {
    return {
      notFound: true
    }
  }

  const radioGenres = await RadioRepository().getGenres(10)
  const genre = radioGenres.find(c => c.name === genreName)

  if (typeof genre === "undefined") {
    return {
      notFound: true
    }
  }

  const popularRadio = await RadioRepository().search({
    genre: genre.name,
    language: language.name,
    limit: 24
  })

  const meta = {
    title: `Listen to the best ${language.name} speaking ${genre.name} radio stations`,
    description: "Find and listen to best radio stations from around the world",
    canonical: languageGenreLink(language.name, genre.name)
  }

  const breadcrumbs = [
    {title: "Discover", url: discoverLink()},
    {title: "Radio", url: radioURL()},
    {title: "Languages", url: languageLink()},
    {title: language.name, url: languageLink(language)},
    {title: genre.name}
  ]

  return {
    props: {
      language,
      genre,
      radioGenres,
      popularRadio,
      meta,
      breadcrumbs
    },
    revalidate: 60
  }
}

export async function getStaticPaths() {
  return {
    paths: [],
    fallback: "blocking"
  }
}