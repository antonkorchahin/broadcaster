import Breadcrumbs from "@/components/Breadcrumbs"
import DiscoverCountryList from "@/components/Discover/DiscoverCountryList"
import GetStarted from "@/components/GetStarted"
import Section from "@/components/Section"
import StationGenreList from "@/components/Stations/StationGenreList"
import StationListGrid from "@/components/Stations/StationListGrid"
import {discoverLink, searchLink} from "@/helpers/helpers"
import {countryLink, radioURL} from "@/helpers/station.helpers"
import {pageViewByStationCountryEvent, pageViewEvent} from "@/lib/ga"
import Head from "next/head"
import React, {useEffect} from "react"
import Layout from "@/layouts/layout"
import Container from "@/components/common/Container"
import {RadioRepository} from "@/repositories/RadioRepository"

const Country = ({country, radioGenres, countries, popularRadio, meta, breadcrumbs}) => {
  useEffect(() => {
    pageViewEvent("radio_country", meta.canonical)
    pageViewByStationCountryEvent(country.name, meta.canonical)
  }, [meta.canonical, country.name])

  return (
    <Layout title={meta.title} description={meta.description}>

      <Head>
        <meta itemProp="name" content={meta.title}/>
        <meta itemProp="description" content={meta.description}/>

        <link rel="canonical" href={meta.canonical}/>

        <meta property="og:site_name" content="Zeno.FM"/>
        <meta property="og:title" content={meta.title}/>
        <meta property="og:type" content="website"/>
        <meta property="og:url" content={meta.canonical}/>
        <meta property="og:description" content={meta.description}/>

        <meta name="twitter:card" content="summary_large_image"/>
        <meta name="twitter:site" content="@Zeno_Media"/>
        <meta name="twitter:title" content={meta.title}/>
        <meta name="twitter:description" content={meta.description}/>
      </Head>

      <Breadcrumbs breadcrumbs={breadcrumbs} heading={`Radio Stations from ${country.name}`}/>

      <Container>
        <StationGenreList genres={radioGenres} country={country}/>

        {
          popularRadio.length > 0 &&
          <Section title={`Popular Radio Stations`}
                   url={searchLink("radio", {country: country.name})}>
            <StationListGrid stations={popularRadio}/>
          </Section>
        }

      </Container>

      <div className="my-12 sm:py-12 py-8 bg-primary">
        <div className="container max-w-screen-lg mx-auto px-4">
          <DiscoverCountryList countries={countries} fullListLink={countryLink()}/>
        </div>
      </div>

      <GetStarted/>

    </Layout>
  )
}

export default Country

export async function getStaticProps(context) {
  let countryName = context.params.country

  let countries = await RadioRepository().getCountries()
  const country = countries.find(c => c.name === countryName)

  if (typeof country === "undefined") {
    return {
      notFound: true
    }
  }

  const radioGenres = await RadioRepository().getGenres(10)
  countries = await RadioRepository().getCountries(10)

  const popularRadio = await RadioRepository().search({
    country: country.name,
    limit: 24
  })

  const meta = {
    title: `Listen to the best radio stations from ${country.name}`,
    description: "Find and listen to best radio stations from around the world",
    canonical: countryLink(country)
  }

  const breadcrumbs = [
    {title: "Discover", url: discoverLink()},
    {title: "Radio", url: radioURL()},
    {title: "Countries", url: countryLink()},
    {title: country.name}
  ]

  return {
    props: {
      country,
      radioGenres,
      countries,
      popularRadio,
      meta,
      breadcrumbs
    },
    revalidate: 60
  }
}

export async function getStaticPaths() {
  return {
    paths: [],
    fallback: "blocking"
  }
}