import AlphabeticalList from "@/components/AlphabeticalList"
import Breadcrumbs from "@/components/Breadcrumbs"
import {discoverLink} from "@/helpers/helpers"
import {countryLink, radioURL} from "@/helpers/station.helpers"
import Layout from "@/layouts/layout"
import {pageViewEvent} from "@/lib/ga"
import Head from "next/head"
import React, {useEffect} from "react"
import Container from "@/components/common/Container"
import {RadioRepository} from "@/repositories/RadioRepository"

const Countries = ({countries, meta, breadcrumbs}) => {
  useEffect(() => {
    pageViewEvent("radio_countries", meta.canonical)
  }, [meta.canonical])

  return (
    <Layout title={meta.title} description={meta.description}>

      <Head>
        <meta itemProp="name" content={meta.title}/>
        <meta itemProp="description" content={meta.description}/>

        <link rel="canonical" href={meta.canonical}/>

        <meta property="og:site_name" content="Zeno.FM"/>
        <meta property="og:title" content={meta.title}/>
        <meta property="og:type" content="website"/>
        <meta property="og:url" content={meta.canonical}/>
        <meta property="og:description" content={meta.description}/>

        <meta name="twitter:card" content="summary_large_image"/>
        <meta name="twitter:site" content="@Zeno_Media"/>
        <meta name="twitter:title" content={meta.title}/>
        <meta name="twitter:description" content={meta.description}/>
      </Head>

      <Breadcrumbs breadcrumbs={breadcrumbs} heading={meta.title}/>

      <Container>
        <AlphabeticalList list={countries}/>
      </Container>
    </Layout>
  )
}

export default Countries

export async function getStaticProps() {
  let countries = await RadioRepository().getCountries()

  const meta = {
    title: "Discover Radio Stations by Countries",
    description: "Find and listen to best radio stations from around the world",
    canonical: countryLink()
  }

  const breadcrumbs = [
    {title: "Discover", url: discoverLink()},
    {title: "Radio", url: radioURL()},
    {title: "Countries"}
  ]

  return {
    props: {
      countries,
      meta,
      breadcrumbs
    },
    revalidate: 60
  }
}