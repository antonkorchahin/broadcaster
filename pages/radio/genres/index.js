import Breadcrumbs from "@/components/Breadcrumbs"
import {discoverLink} from "@/helpers/helpers"
import {genreLink, radioURL} from "@/helpers/station.helpers"
import Layout from "@/layouts/layout"
import {pageViewEvent} from "@/lib/ga"
import Head from "next/head"
import Link from "next/link"
import React, {useEffect} from "react"
import Container from "@/components/common/Container"
import {RadioRepository} from "@/repositories/RadioRepository"

const Genres = ({genres, meta, breadcrumbs}) => {
  useEffect(() => {
    pageViewEvent("radio_genres", meta.canonical)
  }, [meta.canonical])

  return (
    <Layout title={meta.title} description={meta.description}>

      <Head>
        <meta itemProp="name" content={meta.title}/>
        <meta itemProp="description" content={meta.description}/>

        <link rel="canonical" href={meta.canonical}/>

        <meta property="og:site_name" content="Zeno.FM"/>
        <meta property="og:title" content={meta.title}/>
        <meta property="og:type" content="website"/>
        <meta property="og:url" content={meta.canonical}/>
        <meta property="og:description" content={meta.description}/>

        <meta name="twitter:card" content="summary_large_image"/>
        <meta name="twitter:site" content="@Zeno_Media"/>
        <meta name="twitter:title" content={meta.title}/>
        <meta name="twitter:description" content={meta.description}/>
      </Head>

      <Breadcrumbs breadcrumbs={breadcrumbs} heading={meta.title}/>

      <Container>
        <div className="grid md:grid-cols-3 grid-cols-1 gap-8">
          {genres.map((genre, index) => (
            <div key={index}>
              <h2 className="text-xl font-semibold">
                <Link href={genre.url} prefetch={false}>
                  <a
                    className="border-b border-primary sm:pb-1 pb-0 hover:text-primary transition hover:border-transparent">{genre.name}</a>
                </Link>
              </h2>
            </div>
          ))}
        </div>
      </Container>
    </Layout>
  )
}

export default Genres

export async function getStaticProps() {
  const genres = await RadioRepository().getGenres(10)

  const meta = {
    title: "Discover Radio Stations by Genres",
    description: "Find and listen to best radio stations from around the world",
    canonical: genreLink()
  }

  const breadcrumbs = [
    {title: "Discover", url: discoverLink()},
    {title: "Radio", url: radioURL()},
    {title: "Genres"}
  ]

  return {
    props: {
      genres,
      meta,
      breadcrumbs
    },
    revalidate: 60
  }
}