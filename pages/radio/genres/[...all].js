import Breadcrumbs from "@/components/Breadcrumbs"
import GetStarted from "@/components/GetStarted"
import Section from "@/components/Section"
import StationGenreList from "@/components/Stations/StationGenreList"
import StationListGrid from "@/components/Stations/StationListGrid"
import {discoverLink, searchLink} from "@/helpers/helpers"
import {genreLink, radioURL} from "@/helpers/station.helpers"
import {pageViewEvent} from "@/lib/ga"
import Head from "next/head"
import React, {useEffect} from "react"
import Layout from "@/layouts/layout"
import Container from "@/components/common/Container"
import {RadioRepository} from "@/repositories/RadioRepository"

const Genre = ({genre, radioGenres, popularRadio, meta, breadcrumbs}) => {
  useEffect(() => {
    pageViewEvent("radio_genre", meta.canonical)
  }, [meta.canonical])

  return (
    <Layout title={meta.title} description={meta.description}>

      <Head>
        <meta itemProp="name" content={meta.title}/>
        <meta itemProp="description" content={meta.description}/>

        <link rel="canonical" href={meta.canonical}/>

        <meta property="og:site_name" content="Zeno.FM"/>
        <meta property="og:title" content={meta.title}/>
        <meta property="og:type" content="website"/>
        <meta property="og:url" content={meta.canonical}/>
        <meta property="og:description" content={meta.description}/>

        <meta name="twitter:card" content="summary_large_image"/>
        <meta name="twitter:site" content="@Zeno_Media"/>
        <meta name="twitter:title" content={meta.title}/>
        <meta name="twitter:description" content={meta.description}/>
      </Head>

      <Breadcrumbs breadcrumbs={breadcrumbs} heading={`${genre.name} Radio Stations`}/>

      <Container>
        <StationGenreList genres={radioGenres} genre={genre}/>

        {
          popularRadio.length > 0 &&
          <Section title={`Popular Radio Stations`} url={searchLink("radio", {genre: genre.name})}>
            <StationListGrid stations={popularRadio}/>
          </Section>
        }
      </Container>

      <GetStarted/>

    </Layout>
  )
}

export default Genre

export async function getStaticProps(context) {
  let genreName = context.params.all.length === 2 ? context.params.all.join("/") : context.params.all[0]

  const radioGenres = await RadioRepository().getGenres(10)
  const genre = radioGenres.find(c => c.name === genreName)

  if (typeof genre === "undefined") {
    return {
      notFound: true
    }
  }

  const popularRadio = await RadioRepository().search({
    genre: genre.name,
    limit: 24
  })

  const meta = {
    title: `Listen to the best ${genre.name} radio stations`,
    description: "Find and listen to best radio stations from around the world",
    canonical: genreLink(genre)
  }

  const breadcrumbs = [
    {title: "Discover", url: discoverLink()},
    {title: "Radio", url: radioURL()},
    {title: "Genres", url: genreLink()},
    {title: genre.name}
  ]

  return {
    props: {
      genre,
      radioGenres,
      popularRadio,
      meta,
      breadcrumbs
    },
    revalidate: 60
  }
}

export async function getStaticPaths() {
  return {
    paths: [],
    fallback: "blocking"
  }
}