import Breadcrumbs from "@/components/Breadcrumbs"
import DiscoverCountryList from "@/components/Discover/DiscoverCountryList"
import DiscoverLanguageList from "@/components/Discover/DiscoverLanguageList"
import DiscoverRadioBy from "@/components/Discover/DiscoverRadioBy"
import GetStarted from "@/components/GetStarted"
import Section from "@/components/Section"
import StationListGrid from "@/components/Stations/StationListGrid"
import {discoverLink, searchLink} from "@/helpers/helpers"
import {countryLink, languageLink, radioURL} from "@/helpers/station.helpers"
import Layout from "@/layouts/layout"
import {pageViewEvent} from "@/lib/ga"
import Head from "next/head"
import React, {useEffect} from "react"
import Container from "@/components/common/Container"
import {RadioRepository} from "@/repositories/RadioRepository"

const DiscoverRadio = ({countries, languages, stations, meta, breadcrumbs}) => {
  useEffect(() => {
    pageViewEvent("discover_radio", meta.canonical)
  }, [meta.canonical])

  return (
    <Layout title={meta.title} description={meta.description}>

      <Head>
        <meta itemProp="name" content={meta.title}/>
        <meta itemProp="description" content={meta.description}/>

        <link rel="canonical" href={meta.canonical}/>

        <meta property="og:site_name" content="Zeno.FM"/>
        <meta property="og:title" content={meta.title}/>
        <meta property="og:type" content="website"/>
        <meta property="og:url" content={meta.canonical}/>
        <meta property="og:description" content={meta.description}/>

        <meta name="twitter:card" content="summary_large_image"/>
        <meta name="twitter:site" content="@Zeno_Media"/>
        <meta name="twitter:title" content={meta.title}/>
        <meta name="twitter:description" content={meta.description}/>
      </Head>

      <Breadcrumbs breadcrumbs={breadcrumbs} heading="Discover Radio Stations"/>

      <Container>
        <Section title="Discover Radio Stations" borderBottom={true} heightAuto={true}>
          <DiscoverRadioBy/>
        </Section>

        {
          stations.length > 0 &&
          <Section title="Popular Radio Stations" url={searchLink("radio")}>
            <StationListGrid stations={stations}/>
          </Section>
        }

      </Container>

      <div className="my-12 sm:py-12 py-8 bg-primary">
        <div className="container max-w-screen-lg mx-auto px-4">
          <div className="grid md:grid-cols-1 grid-cols-1 gap-6">
            <DiscoverCountryList countries={countries} fullListLink={countryLink()}/>

            <DiscoverLanguageList languages={languages} fullListLink={languageLink()}/>
          </div>
        </div>
      </div>

      <GetStarted/>

    </Layout>
  )
}

export default DiscoverRadio

export async function getStaticProps() {
  const countries = await RadioRepository().getCountries(10)
  const languages = await RadioRepository().getLanguages(10)

  const stations = await RadioRepository().search({
    limit: 12
  })

  const meta = {
    title: "Discover radio stations",
    description: "Discover and listen to the best radio stations from around the world",
    canonical: radioURL()
  }

  const breadcrumbs = [
    {title: "Discover", url: discoverLink()},
    {title: "Radio"}
  ]

  return {
    props: {
      countries,
      languages,
      stations,
      meta,
      breadcrumbs
    },
    revalidate: 60
  }
}