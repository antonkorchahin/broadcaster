import AlphabeticalList from "@/components/AlphabeticalList"
import Breadcrumbs from "@/components/Breadcrumbs"
import {discoverLink} from "@/helpers/helpers"
import {languageLink, radioURL} from "@/helpers/station.helpers"
import Layout from "@/layouts/layout"
import {pageViewEvent} from "@/lib/ga"
import Head from "next/head"
import React, {useEffect} from "react"
import Container from "@/components/common/Container"
import {RadioRepository} from "@/repositories/RadioRepository"

const Languages = ({languages, meta, breadcrumbs}) => {
  useEffect(() => {
    pageViewEvent("radio_languages", meta.canonical)
  }, [meta.canonical])

  return (
    <Layout title={meta.title} description={meta.description}>

      <Head>
        <meta itemProp="name" content={meta.title}/>
        <meta itemProp="description" content={meta.description}/>

        <link rel="canonical" href={meta.canonical}/>

        <meta property="og:site_name" content="Zeno.FM"/>
        <meta property="og:title" content={meta.title}/>
        <meta property="og:type" content="website"/>
        <meta property="og:url" content={meta.canonical}/>
        <meta property="og:description" content={meta.description}/>

        <meta name="twitter:card" content="summary_large_image"/>
        <meta name="twitter:site" content="@Zeno_Media"/>
        <meta name="twitter:title" content={meta.title}/>
        <meta name="twitter:description" content={meta.description}/>
      </Head>

      <Breadcrumbs breadcrumbs={breadcrumbs} heading={meta.title}/>

      <Container>
        <AlphabeticalList list={languages}/>
      </Container>
    </Layout>
  )
}

export default Languages

export async function getStaticProps() {
  const languages = await RadioRepository().getLanguages()

  const meta = {
    title: "Discover Radio Stations by Languages",
    description: "Find and listen to best radio stations from around the world",
    canonical: languageLink()
  }

  const breadcrumbs = [
    {title: "Discover", url: discoverLink()},
    {title: "Radio", url: radioURL()},
    {title: "Languages"}
  ]

  return {
    props: {
      languages,
      meta,
      breadcrumbs
    },
    revalidate: 60
  }
}