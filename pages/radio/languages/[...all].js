import Breadcrumbs from "@/components/Breadcrumbs"
import DiscoverLanguageList from "@/components/Discover/DiscoverLanguageList"
import GetStarted from "@/components/GetStarted"
import Section from "@/components/Section"
import StationGenreList from "@/components/Stations/StationGenreList"
import StationListGrid from "@/components/Stations/StationListGrid"
import {discoverLink, searchLink} from "@/helpers/helpers"
import {languageLink, radioURL} from "@/helpers/station.helpers"
import {pageViewEvent} from "@/lib/ga"
import Head from "next/head"
import React, {useEffect} from "react"
import Layout from "@/layouts/layout"
import Container from "@/components/common/Container"
import {RadioRepository} from "@/repositories/RadioRepository"

const Language = ({language, radioGenres, languages, popularRadio, meta, breadcrumbs}) => {
  useEffect(() => {
    pageViewEvent("radio_language", meta.canonical)
  }, [meta.canonical])

  return (
    <Layout title={meta.title} description={meta.description}>

      <Head>
        <meta itemProp="name" content={meta.title}/>
        <meta itemProp="description" content={meta.description}/>

        <link rel="canonical" href={meta.canonical}/>

        <meta property="og:site_name" content="Zeno.FM"/>
        <meta property="og:title" content={meta.title}/>
        <meta property="og:type" content="website"/>
        <meta property="og:url" content={meta.canonical}/>
        <meta property="og:description" content={meta.description}/>

        <meta name="twitter:card" content="summary_large_image"/>
        <meta name="twitter:site" content="@Zeno_Media"/>
        <meta name="twitter:title" content={meta.title}/>
        <meta name="twitter:description" content={meta.description}/>
      </Head>

      <Breadcrumbs breadcrumbs={breadcrumbs} heading={`${language.name} Speaking Radio Stations`}/>

      <Container>
        <StationGenreList genres={radioGenres} language={language}/>

        {
          popularRadio.length > 0 &&
          <Section title="Popular Radio Stations"
                   url={searchLink("radio", {language: language.name})}>
            <StationListGrid stations={popularRadio}/>
          </Section>
        }
      </Container>

      <div className="my-12 sm:py-12 py-8 bg-primary">
        <div className="container max-w-screen-lg mx-auto px-4">
          <DiscoverLanguageList languages={languages} fullListLink={languageLink()}/>
        </div>
      </div>

      <GetStarted/>

    </Layout>
  )
}

export default Language

export async function getStaticProps(context) {
  let languageName = context.params.all.length === 2 ? context.params.all.join("/") : context.params.all[0]

  let languages = await RadioRepository().getLanguages()
  const language = languages.find(c => c.name === languageName)

  if (typeof language === "undefined") {
    return {
      notFound: true
    }
  }

  const radioGenres = await RadioRepository().getGenres(10)
  languages = await RadioRepository().getLanguages(10)

  const popularRadio = await RadioRepository().search({
    language: language.name,
    limit: 24
  })

  const meta = {
    title: `Listen to the best ${language.name} speaking radio stations`,
    description: "Find and listen to best radio stations from around the world",
    canonical: languageLink(language)
  }

  const breadcrumbs = [
    {title: "Discover", url: discoverLink()},
    {title: "Radio", url: radioURL()},
    {title: "Languages", url: languageLink()},
    {title: language.name}
  ]

  return {
    props: {
      language,
      radioGenres,
      languages,
      popularRadio,
      meta,
      breadcrumbs
    },
    revalidate: 60
  }
}

export async function getStaticPaths() {
  return {
    paths: [],
    fallback: "blocking"
  }
}