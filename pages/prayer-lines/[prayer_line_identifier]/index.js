import {searchLink, siteUrl} from "@/helpers/helpers"
import {FaPrayingHands} from "react-icons/fa"
import {pageViewEvent, prayerLineGetStartedClick} from "@/lib/ga"
import Image from "next/image"
import React, {useEffect} from "react"
import Layout from "@/layouts/layout"
import {ImFacebook2} from "react-icons/im"
import {ImInstagram} from "react-icons/im"
import {ImSphere} from "react-icons/im"
import {ImTwitter} from "react-icons/im"
import Head from "next/head"
import Link from "next/link"
import {PrayerLineRepository} from "@/repositories/PrayerLineRepository"

const PrayerLine = ({prayerLine, meta}) => {
  useEffect(() => {
    pageViewEvent("prayer_line", meta.canonical)
  }, [meta.canonical])

  const getStartedClick = () => {
    prayerLineGetStartedClick(siteUrl(router.asPath))
  }

  return (
    <Layout title={meta.title} description={meta.description} withGap={false}>

      <Head>
        <meta itemProp="name" content={meta.title}/>
        <meta itemProp="description" content={meta.description}/>
        <meta itemProp="image" content={prayerLine.logo}/>

        <link rel="canonical" href={meta.canonical}/>
        <link rel="amphtml" href={meta.amp}/>

        <meta property="og:site_name" content="Zeno.FM"/>
        <meta property="og:title" content={meta.title}/>
        <meta property="og:type" content="website"/>
        <meta property="og:url" content={meta.canonical}/>
        <meta property="og:description" content={meta.description}/>
        <meta property="og:image" content={prayerLine.logo}/>

        <meta name="twitter:card" content="summary_large_image"/>
        <meta name="twitter:site" content="@Zeno_Media"/>
        <meta name="twitter:title" content={meta.title}/>
        <meta name="twitter:description" content={meta.description}/>
        <meta name="twitter:image" content={prayerLine.logo}/>
        <meta name="twitter:image:alt" content={meta.title}/>
      </Head>

      <section className="relative bg-transparent">
        <div className="bg-black w-full h-full absolute overflow-hidden" style={{"zIndex": "-1"}}>
          {
            prayerLine.background &&
            <Image className="opacity-20"
                   alt={prayerLine.name}
                   priority
                   src={prayerLine.background}
                   layout="fill"
                   objectFit="cover"
            />
          }
        </div>

        <div className="container z-50 bg-transparent max-w-screen-lg mx-auto pt-8 pb-6 px-4 text-white">
          <div className="sm:block flex">
            <div className="flex-none sm:mb-10 mb-4 sm:text-center text-left sm:w-auto w-32 md:mr-0 mr-4">
              {
                prayerLine.logo &&
                <Image className="rounded"
                       src={prayerLine.logo}
                       height={250}
                       width={250}
                       quality={100}
                       priority
                       alt={prayerLine.name}/>
              }
            </div>

            <h1 className="sm:text-4xl text-lg sm:text-center text-left break-words">
              {prayerLine.name}
            </h1>
          </div>

          <div className="flex text-center sm:mt-4 mt-0">
            <Link href={searchLink("prayer-lines", {religion: prayerLine.religion})} prefetch={false}>
              <a className="tag mx-auto sm:text-lg">
                <span>
                  <FaPrayingHands className="w-4 mr-1"/>
                  <span>{prayerLine.religion}</span>
                </span>
              </a>
            </Link>
          </div>

          <div className="flex justify-center text-2xl mt-4 mb-10">
            {
              prayerLine.facebook &&
              <a href={prayerLine.facebook} target="_blank" rel="noopener noreferrer"
                 className="mx-4 hover:text-primary" aria-label="Facebook page">
                <ImFacebook2/>
              </a>
            }

            {
              prayerLine.instagram &&
              <a href={prayerLine.instagram} target="_blank" rel="noopener noreferrer"
                 className="mx-4 hover:text-primary" aria-label="Instagram page">
                <ImInstagram/>
              </a>
            }

            {
              prayerLine.twitter &&
              <a href={prayerLine.twitter} target="_blank" rel="noopener noreferrer" className="mx-4 hover:text-primary"
                 aria-label="Twitter page">
                <ImTwitter/>
              </a>
            }

            {
              prayerLine.website &&
              <a href={prayerLine.website} target="_blank" rel="noopener noreferrer" className="mx-4 hover:text-primary"
                 aria-label="Website">
                <ImSphere/>
              </a>
            }
          </div>
        </div>
      </section>

      <section className="container max-w-screen-lg mx-auto py-14 px-4">
        <h2 className="sm:text-2xl text-xl font-semibold mb-8">Prayer Line: {prayerLine.phone}</h2>

        <div className="description" dangerouslySetInnerHTML={{
          __html: prayerLine.description
        }}/>
      </section>

      <section className="relative bg-transparent">
        <div className="bg-black w-full h-full absolute overflow-hidden" style={{"zIndex": "-1"}}>
          <Image
            alt="prayer lines"
            src="/images/prayer_lines.webp"
            layout="fill"
            objectFit="cover"
            quality={100}
          />
        </div>

        <div className="container z-50 max-w-screen-lg mx-auto py-32 px-4 text-white text-center">
          <h3 className="text-3xl mx-4 font-semibold">
            Create your own Prayer Line
          </h3>

          <p className="my-8 text-lg">The easiest way to set up a conference call Prayer Line</p>

          <a onClick={getStartedClick} href="https://form.asana.com/?k=cbnMDAQ11rWs1Pe6LEpU3Q&d=205463619831"
             target="_blank" rel="noopener noreferrer"
             className="inline-block text-black mx-auto bg-primary px-8 py-4 font-bold my-4 rounded-full">
            Get Started
          </a>
        </div>

      </section>

    </Layout>
  )
}

export default PrayerLine

export async function getStaticProps(context) {
  const prayerLine = await PrayerLineRepository().get(context.params.prayer_line_identifier)

  if (!prayerLine) {
    return {
      notFound: true
    }
  }

  if (prayerLine.slug && prayerLine.slug !== context.params.prayer_line_identifier) {
    return {
      redirect: {
        destination: prayerLine.url, permanent: true
      }
    }
  }

  const meta = {
    title: `Listen to ${prayerLine.name}`,
    description: `Listen to ${prayerLine.name} for the best prayer line about ${prayerLine.religion} on Zeno.FM. Staying together, while apart.`,
    canonical: prayerLine.url,
    amp: prayerLine.urlAMP
  }

  return {
    props: {
      prayerLine,
      meta
    },
    revalidate: 60
  }
}

export async function getStaticPaths() {
  return {
    paths: [],
    fallback: "blocking"
  }
}