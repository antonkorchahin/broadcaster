import Breadcrumbs from "@/components/Breadcrumbs"
import GetStarted from "@/components/GetStarted"
import PrayerLineReligionList from "@/components/PrayerLines/PrayerLineReligionList"
import Section from "@/components/Section"
import {discoverLink, searchLink} from "@/helpers/helpers"
import Layout from "@/layouts/layout"
import {pageViewEvent} from "@/lib/ga"
import React, {useEffect} from "react"
import Container from "@/components/common/Container"
import {PrayerLineRepository} from "@/repositories/PrayerLineRepository"
import {prayerLineUrl} from "@/helpers/prayer-line.helper"
import ListGrid from "@/components/ListGrid"

const DiscoverPrayerLines = ({prayerLines, religions, meta, breadcrumbs}) => {
  useEffect(() => {
    pageViewEvent("discover_prayer_lines", meta.canonical)
  }, [meta.canonical])

  return (
    <Layout title={meta.title} description={meta.description}>

      <Breadcrumbs breadcrumbs={breadcrumbs} heading="Discover Prayer Lines"/>

      <Container>
        <PrayerLineReligionList religions={religions}/>

        {
          prayerLines.length > 0 &&
          <Section title="Popular Prayer Lines" url={searchLink("prayer-lines")}>
            <ListGrid items={prayerLines}/>
          </Section>
        }
      </Container>

      <GetStarted/>

    </Layout>
  )
}

export default DiscoverPrayerLines

export async function getStaticProps() {
  const religions = await PrayerLineRepository().getReligions(15)

  const prayerLines = await PrayerLineRepository().search({
    limit: 24
  })

  const meta = {
    title: "Discover Prayer Lines",
    description: "Discover prayer lines from around the world",
    canonical: prayerLineUrl()
  }

  const breadcrumbs = [
    {title: "Discover", url: discoverLink()},
    {title: "Prayer Lines"}
  ]

  return {
    props: {
      prayerLines,
      religions,
      meta,
      breadcrumbs
    },
    revalidate: 60
  }
}