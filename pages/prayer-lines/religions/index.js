import Breadcrumbs from "@/components/Breadcrumbs"
import {discoverLink} from "@/helpers/helpers"
import {prayerLineUrl, religionLink} from "@/helpers/prayer-line.helper"
import Layout from "@/layouts/layout"
import {pageViewEvent} from "@/lib/ga"
import Head from "next/head"
import Link from "next/link"
import React, {useEffect} from "react"
import Container from "@/components/common/Container"
import {PrayerLineRepository} from "@/repositories/PrayerLineRepository"

const Religion = ({religions, meta, breadcrumbs}) => {
  useEffect(() => {
    pageViewEvent("prayer_lines_religions", meta.canonical)
  }, [meta.canonical])

  return (
    <Layout title={meta.title} description={meta.description}>

      <Head>
        <meta itemProp="name" content={meta.title}/>
        <meta itemProp="description" content={meta.description}/>

        <link rel="canonical" href={meta.canonical}/>

        <meta property="og:site_name" content="Zeno.FM"/>
        <meta property="og:title" content={meta.title}/>
        <meta property="og:type" content="website"/>
        <meta property="og:url" content={meta.canonical}/>
        <meta property="og:description" content={meta.description}/>

        <meta name="twitter:card" content="summary_large_image"/>
        <meta name="twitter:site" content="@Zeno_Media"/>
        <meta name="twitter:title" content={meta.title}/>
        <meta name="twitter:description" content={meta.description}/>
      </Head>

      <Breadcrumbs breadcrumbs={breadcrumbs} heading={meta.title}/>

      <Container>
        <div className="grid md:grid-cols-3 grid-cols-1 gap-8">
          {religions.map((religion, index) => (
            <div key={index}>
              <h2 className="text-xl font-semibold">
                <Link href={religion.url} prefetch={false}>
                  <a
                    className="border-b border-primary sm:pb-1 pb-0 hover:text-primary transition hover:border-transparent">{religion.name}</a>
                </Link>
              </h2>
            </div>
          ))}
        </div>
      </Container>
    </Layout>
  )
}

export default Religion

export async function getStaticProps() {
  const religions = await PrayerLineRepository().getReligions(15)

  const meta = {
    title: "Discover Prayer Lines by Religions",
    description: "Find and listen to best prayer lines from around the world on zeno.fm",
    canonical: religionLink()
  }

  const breadcrumbs = [
    {title: "Discover", url: discoverLink()},
    {title: "Prayer Lines", url: prayerLineUrl()},
    {title: "Religions"}
  ]

  return {
    props: {
      religions,
      meta,
      breadcrumbs
    },
    revalidate: 60
  }
}