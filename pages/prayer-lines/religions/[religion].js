import Breadcrumbs from "@/components/Breadcrumbs"
import GetStarted from "@/components/GetStarted"
import PrayerLineReligionList from "@/components/PrayerLines/PrayerLineReligionList"
import Section from "@/components/Section"
import {discoverLink, searchLink} from "@/helpers/helpers"
import {prayerLineUrl, religionLink} from "@/helpers/prayer-line.helper"
import {pageViewEvent} from "@/lib/ga"
import Head from "next/head"
import React, {useEffect} from "react"
import Layout from "@/layouts/layout"
import Container from "@/components/common/Container"
import {PrayerLineRepository} from "@/repositories/PrayerLineRepository"
import ListGrid from "@/components/ListGrid"

const Religion = ({religion, religions, popularPrayerLines, meta, breadcrumbs}) => {
  useEffect(() => {
    pageViewEvent("prayer_lines_religion", meta.canonical)
  }, [meta.canonical])

  return (
    <Layout title={meta.title} description={meta.description}>

      <Head>
        <meta itemProp="name" content={meta.title}/>
        <meta itemProp="description" content={meta.description}/>

        <link rel="canonical" href={meta.canonical}/>

        <meta property="og:site_name" content="Zeno.FM"/>
        <meta property="og:title" content={meta.title}/>
        <meta property="og:type" content="website"/>
        <meta property="og:url" content={meta.canonical}/>
        <meta property="og:description" content={meta.description}/>

        <meta name="twitter:card" content="summary_large_image"/>
        <meta name="twitter:site" content="@Zeno_Media"/>
        <meta name="twitter:title" content={meta.title}/>
        <meta name="twitter:description" content={meta.description}/>
      </Head>

      <Breadcrumbs breadcrumbs={breadcrumbs} heading={`${religion.name} Prayer Lines`}/>

      <Container>
        <PrayerLineReligionList religions={religions} religion={religion}/>

        {
          popularPrayerLines.length > 0 &&
          <Section title="Popular Prayer Lines" url={searchLink("prayer-lines")}>
            <ListGrid items={popularPrayerLines}/>
          </Section>
        }
      </Container>

      <GetStarted/>

    </Layout>
  )
}

export default Religion

export async function getStaticProps(context) {
  let religionName = context.params.religion

  const religions = await PrayerLineRepository().getReligions(15)
  const religion = religions.find(r => r.name === religionName)

  if (typeof religion === "undefined") {
    return {
      notFound: true
    }
  }

  const popularPrayerLines = await PrayerLineRepository().search({
    religion: religion.name,
    limit: 24
  })

  const meta = {
    title: `Listen to the best ${religion.name} prayer lines`,
    description: `Find and listen to best prayer lines about ${religion.name} from around the world on zeno.fm`,
    canonical: religionLink(religion)
  }

  const breadcrumbs = [
    {title: "Discover", url: discoverLink()},
    {title: "Prayer Lines", url: prayerLineUrl()},
    {title: "Religions", url: religionLink()},
    {title: religion.name}
  ]

  return {
    props: {
      religion,
      religions,
      popularPrayerLines,
      meta,
      breadcrumbs
    },
    revalidate: 60
  }
}

export async function getStaticPaths() {
  return {
    paths: [],
    fallback: "blocking"
  }
}