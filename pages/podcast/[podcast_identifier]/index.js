import Breadcrumbs from "@/components/Breadcrumbs"
import PodcastEpisodeList from "@/components/Podcasts/PodcastEpisodeList"
import {podcastUrl} from "@/helpers/podcasts.helper"
import {pageViewEvent} from "@/lib/ga"
import {discoverLink, searchLink} from "@/helpers/helpers"
import React, {useEffect} from "react"
import Layout from "@/layouts/layout"
import Description from "@/components/common/Description"
import PodcastCategoryList from "@/components/Podcasts/PodcastCategoryList"
import Container from "@/components/common/Container"
import {PodcastRepository} from "@/repositories/PodcastRepository"
import HeadingDetails from "@/components/common/HeadingDetails"
import LinkPill from "@/components/common/LinkPill"
import {EpisodeRepository} from "@/repositories/EpisodeRepository"
import TheImage from "@/components/common/TheImage"
import PodcastMeta from "@/components/Podcasts/PodcastMeta"
import ParentLink from "@/components/Podcasts/ParentLink";

const Details = ({podcast, categories}) => {
  return (
    <>
      {
        podcast.station &&
        <div className="mb-6">
          <HeadingDetails>Radio</HeadingDetails>
          <ParentLink url={podcast.station.url} title={podcast.station.name} logo={podcast.station.logo}/>
        </div>

      }

      {
        podcast.description &&
        <div className="mb-6">
          <HeadingDetails>About this podcast</HeadingDetails>
          <Description text={podcast.description}/>
        </div>
      }

      {
        podcast.language &&
        <div className="mb-6">
          <HeadingDetails>Language</HeadingDetails>
          <LinkPill href={searchLink("podcasts", {language: podcast.language})}>{podcast.language}</LinkPill>
        </div>
      }

      {
        (podcast.categories && podcast.categories.length) &&
        <div className="mb-4 pb-4 border-b border-gray-200">
          <HeadingDetails>Categories</HeadingDetails>

          <div className="overflow-x-auto">
            <div className="flex flex-row sm:flex-wrap flex-nowrap sm:pb-0 pb-4">
              {podcast.categories.map((item, index) => (
                <LinkPill key={index} href={searchLink("podcasts", {category: item})}>{item}</LinkPill>))}
            </div>
          </div>
        </div>
      }

      <PodcastCategoryList categories={categories} borderBottom={false}/>
    </>
  )
}

const Podcast = ({podcast, episodes, categories, meta, breadcrumbs}) => {
  useEffect(() => {
    pageViewEvent("podcast", meta.canonical)
  }, [meta.canonical])

  return (
    <Layout title={meta.title} description={meta.description}>
      <PodcastMeta meta={meta}/>

      <Breadcrumbs breadcrumbs={breadcrumbs}/>

      <Container>
        <div className="md:grid block grid-cols-12 gap-8">
          <div className="col-span-4 md:block hidden">
            <TheImage src={podcast.logo} size={300} alt={podcast.title}/>

            <div className="mt-4 pt-4 border-t border-gray-200">
              <Details podcast={podcast} categories={categories}/>
            </div>
          </div>

          <div className="md:col-span-8 col-span-12">
            <div className="flex mb-4 md:pb-0 pb-4 md:border-none border-b">
              <div className="md:hidden block flex-shrink-0 mr-2 w-28">
                <TheImage src={podcast.logo} size={204} alt={podcast.title} compact={true}/>
              </div>
              <div className="self-center">
                <h1 className="md:text-3xl sm:text-xl text-base font-semibold ln-wrap-line">
                  {podcast.title}
                </h1>

                {
                  podcast.author &&
                  <div className="mt-2 sm:text-base text-sm opacity-80 ln-wrap-line">{podcast.author}</div>
                }
              </div>
            </div>

            <div className="md:hidden block">
              <Details podcast={podcast} categories={categories}/>
            </div>

            {
              !!episodes.length &&
              <div className="pt-4 mt-4 border-t border-gray-200">
                <PodcastEpisodeList podcast={podcast} episodes={episodes} episodesCount={podcast.episodesCount}/>
              </div>
            }
          </div>
        </div>
      </Container>
    </Layout>
  )
}

export default Podcast

export async function getStaticProps(context) {
  const podcast = await PodcastRepository().get(context.params.podcast_identifier)

  if (!podcast) {
    return {
      notFound: true
    }
  }

  if (podcast.slug && podcast.slug !== context.params.podcast_identifier) {
    return {
      redirect: {
        destination: podcast.url, permanent: true
      }
    }
  }

  const episodes = await EpisodeRepository(podcast).search()

  const categories = await PodcastRepository().getCategories(10)

  const meta = {
    title: `Listen to ${podcast.title}`,
    description: `Catch up on episodes of ${podcast.title}. Stream for free today!`,
    logo: podcast.logo,
    canonical: podcast.url,
    amp: podcast.urlAMP
  }

  const breadcrumbs = [
    {title: "Discover", url: discoverLink()},
    {title: "Podcasts", url: podcastUrl()},
    {title: podcast.title}
  ]

  return {
    props: {
      podcast,
      episodes,
      categories,
      meta,
      breadcrumbs
    },
    revalidate: 60
  }
}

export async function getStaticPaths() {
  return {
    paths: [],
    fallback: "blocking"
  }
}