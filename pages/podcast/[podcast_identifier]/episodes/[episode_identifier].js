import Breadcrumbs from "@/components/Breadcrumbs"
import {podcastUrl} from "@/helpers/podcasts.helper"
import {pageViewEvent} from "@/lib/ga"
import {discoverLink} from "@/helpers/helpers"
import Head from "next/head"
import React, {useEffect} from "react"
import Layout from "@/layouts/layout"
import Description from "@/components/common/Description"
import Container from "@/components/common/Container"
import {PodcastRepository} from "@/repositories/PodcastRepository"
import HeadingDetails from "@/components/common/HeadingDetails"
import {FiExternalLink} from "react-icons/fi"
import {EpisodeRepository} from "@/repositories/EpisodeRepository"
import ParentLink from "@/components/Podcasts/ParentLink"
import TheImage from "@/components/common/TheImage"
import PlayerBtnPlaybackEpisode from "@/components/Player/PlayerBtnPlaybackEpisode";

const Episode = ({podcast, episode, meta, breadcrumbs}) => {
  useEffect(() => {
    pageViewEvent("episode_podcast", meta.canonical)
  }, [meta.canonical])

  return (
    <Layout title={meta.title} description={meta.description}>

      <Head>
        <meta itemProp="name" content={meta.title}/>
        <meta itemProp="description" content={meta.description}/>
        <meta itemProp="image" content={meta.logo}/>

        <link rel="canonical" href={meta.canonical}/>

        <meta name="twitter:card" content="player"/>
        <meta name="twitter:title" content={meta.title}/>
        <meta name="twitter:site" content="@Zeno_Media"/>
        <meta name="twitter:description" content={meta.description}/>
        <meta name="twitter:image" content={meta.logo}/>
        <meta name="twitter:image:alt" content={meta.title}/>

        <meta property="og:site_name" content="Zeno.FM"/>
        <meta property="og:title" content={meta.title}/>
        <meta property="og:type" content="website"/>
        <meta property="og:url" content={meta.canonical}/>
        <meta property="og:description" content={meta.description}/>
        <meta property="og:audio" content={episode.file_url}/>
        <meta property="og:image" content={meta.logo}/>
      </Head>

      <Breadcrumbs breadcrumbs={breadcrumbs}/>

      <Container>

        <div className="md:grid block grid-cols-12 gap-8">
          <div className="col-span-4 md:block hidden">
            <TheImage src={episode.logo} size={300} alt={episode.title}/>

            <div className="mt-4 pt-4 border-t">
              <HeadingDetails>Podcast</HeadingDetails>
              <ParentLink url={podcast.url} title={podcast.title} logo={podcast.logo}/>
            </div>
          </div>

          <div className="md:col-span-8 col-span-12">
            <div className="flex">
              <div className="md:hidden block flex-shrink-0 mr-2 w-28">
                <TheImage src={episode.logo} size={204} alt={episode.title} compact={true}/>
              </div>
              <div className="self-center">
                {
                  episode.title &&
                  <h1 className="md:text-2xl sm:text-xl text-base font-semibold ln-wrap-line">
                    {episode.title}
                  </h1>
                }

                {
                  episode.author &&
                  <div className="mt-2 sm:text-base text-sm opacity-80 ln-wrap-line">{episode.author}</div>
                }
              </div>
            </div>

            <div className="my-4 py-4 border-b border-t flex-line">
              <div className="flex-line mr-8">
                <PlayerBtnPlaybackEpisode episode={episode} podcast={podcast}/>

                {
                  episode.duration &&
                  <span className="ml-2">{episode.duration}</span>
                }
              </div>

              {
                episode.link &&
                <a href={episode.link} target="_blank" rel="noopener noreferrer"
                   className="px-3 py-1.5 border rounded flex-line hover:bg-gray-100">
                  <FiExternalLink className="mr-1"/>
                  Link
                </a>
              }
            </div>

            <div className="mb-6 md:hidden block">
              <HeadingDetails>Podcast</HeadingDetails>
              <ParentLink url={podcast.url} title={podcast.title} logo={podcast.logo}/>
            </div>

            {
              episode.description &&
              <>
                <HeadingDetails>About this episode</HeadingDetails>
                <Description text={episode.description} isClamped={false}/>
              </>
            }
          </div>
        </div>
      </Container>
    </Layout>
  )
}

export default Episode

export async function getStaticProps(context) {
  const podcast = await PodcastRepository().get(context.params.podcast_identifier)

  if (!podcast) {
    return {
      notFound: true
    }
  }

  const episode = await EpisodeRepository(podcast).get(context.params.episode_identifier)

  if (!episode) {
    return {
      notFound: true
    }
  }

  if ((podcast.slug && podcast.slug !== context.params.podcast_identifier)
    || (episode.slug && episode.slug !== context.params.episode_identifier)) {
    return {
      redirect: {
        destination: episode.url, permanent: true
      }
    }
  }

  const meta = {
    title: `Listen to ${episode.title}`,
    description: `Catch up on old episodes of ${episode.title}. All of your favorites, now on Zeno.`,
    logo: episode.logo,
    canonical: episode.url,
  }

  const breadcrumbs = [
    {title: "Discover", url: discoverLink()},
    {title: "Podcasts", url: podcastUrl()},
    {title: podcast.title, url: podcast.url},
    {title: episode.title},
  ]

  return {
    props: {
      podcast,
      episode,
      meta,
      breadcrumbs
    },
    revalidate: 60
  }
}

export async function getStaticPaths() {
  return {
    paths: [],
    fallback: "blocking"
  }
}