import React from "react"
import {EpisodeRepository} from "@/repositories/EpisodeRepository"
import {PodcastRepository} from "@/repositories/PodcastRepository"

const Episode = () => {
  return null
}

export default Episode

export async function getStaticProps(context) {
  const podcast = await PodcastRepository().get(context.params.show_identifier)

  if (!podcast) {
    return {
      notFound: true
    }
  }

  const episode = await EpisodeRepository(podcast).get(context.params.episode_identifier)

  if (!episode) {
    return {
      notFound: true
    }
  }

  return {
    redirect: {
      destination: episode.url, permanent: true
    }
  }
}

export async function getStaticPaths() {
  return {
    paths: [],
    fallback: "blocking"
  }
}