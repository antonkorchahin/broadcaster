import React from "react"
import {PodcastRepository} from "@/repositories/PodcastRepository"

const Show = () => {
  return null
}

export default Show

export async function getStaticProps(context) {
  const podcast = await PodcastRepository().get(context.params.show_identifier)

  if (!podcast) {
    return {
      notFound: true
    }
  }

  return {
    redirect: {
      destination: podcast.url, permanent: true
    }
  }
}

export async function getStaticPaths() {
  return {
    paths: [],
    fallback: "blocking"
  }
}