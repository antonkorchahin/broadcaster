import Breadcrumbs from "@/components/Breadcrumbs"
import GetStarted from "@/components/GetStarted"
import PodcastCategoryList from "@/components/Podcasts/PodcastCategoryList"
import Section from "@/components/Section"
import {discoverLink, searchLink} from "@/helpers/helpers"
import Layout from "@/layouts/layout"
import {pageViewEvent} from "@/lib/ga"
import Head from "next/head"
import React, {useEffect} from "react"
import Container from "@/components/common/Container"
import {PodcastRepository} from "@/repositories/PodcastRepository"
import ListGrid from "@/components/ListGrid"
import {countryCategoryLink, countryLink, podcastUrl} from "@/helpers/podcasts.helper"

const CountryGenre = ({country, category, podcastCategories, popularPodcasts, meta, breadcrumbs}) => {
  useEffect(() => {
    pageViewEvent("podcasts_country_category", meta.canonical)
  }, [meta.canonical])

  return (
    <Layout title={meta.title} description={meta.description}>

      <Head>
        <meta itemProp="name" content={meta.title}/>
        <meta itemProp="description" content={meta.description}/>

        <link rel="canonical" href={meta.canonical}/>

        <meta property="og:site_name" content="Zeno.FM"/>
        <meta property="og:title" content={meta.title}/>
        <meta property="og:type" content="website"/>
        <meta property="og:url" content={meta.canonical}/>
        <meta property="og:description" content={meta.description}/>

        <meta name="twitter:card" content="summary_large_image"/>
        <meta name="twitter:site" content="@Zeno_Media"/>
        <meta name="twitter:title" content={meta.title}/>
        <meta name="twitter:description" content={meta.description}/>
      </Head>

      <Breadcrumbs breadcrumbs={breadcrumbs} heading={`${category.name} Podcasts from ${country.name}`}/>

      <Container>
        <PodcastCategoryList categories={podcastCategories} country={country} category={category}/>

        {
          popularPodcasts.length > 0 &&
          <Section title="Popular Podcasts"
                   url={searchLink("podcasts", {country: country.name, category: category.id})}>
            <ListGrid items={popularPodcasts}/>
          </Section>
        }
      </Container>

      <GetStarted/>

    </Layout>
  )
}

export default CountryGenre

export async function getStaticProps(context) {
  let categoryName = context.params.category
  let countryName = context.params.country

  const countries = await PodcastRepository().getCountries()
  const country = countries.find(c => c.name === countryName)

  if (typeof country === "undefined") {
    return {
      notFound: true
    }
  }

  let categories = await PodcastRepository().getCategories()
  const category = categories.find(c => c.name === categoryName)

  if (typeof category === "undefined") {
    return {
      notFound: true
    }
  }

  const podcastCategories = await PodcastRepository().getCategories(10)

  const params = {
    category: category.id,
    country: country.name,
    limit: 24
  }

  const popularPodcasts = await PodcastRepository().search(params)

  const meta = {
    title: `Listen to the best ${category.name} podcasts from ${country.name}`,
    description: "Find and listen to best podcasts from around the world",
    canonical: countryCategoryLink(country.name, category.name)
  }

  const breadcrumbs = [
    {title: "Discover", url: discoverLink()},
    {title: "Podcasts", url: podcastUrl()},
    {title: "Countries", url: countryLink()},
    {title: country.name, url: countryLink(country)},
    {title: category.name}
  ]

  return {
    props: {
      country,
      category,
      podcastCategories,
      popularPodcasts,
      meta,
      breadcrumbs
    },
    revalidate: 60
  }
}

export async function getStaticPaths() {
  return {
    paths: [],
    fallback: "blocking"
  }
}