import Breadcrumbs from "@/components/Breadcrumbs"
import DiscoverCountryList from "@/components/Discover/DiscoverCountryList"
import DiscoverLanguageList from "@/components/Discover/DiscoverLanguageList"
import DiscoverPodcastsBy from "@/components/Discover/DiscoverPodcastsBy"
import GetStarted from "@/components/GetStarted"
import Section from "@/components/Section"
import {discoverLink, searchLink} from "@/helpers/helpers"
import Layout from "@/layouts/layout"
import {pageViewEvent} from "@/lib/ga"
import Head from "next/head"
import React, {useEffect} from "react"
import Container from "@/components/common/Container"
import {PodcastRepository} from "@/repositories/PodcastRepository"
import ListGrid from "@/components/ListGrid"
import {countryLink, languageLink, podcastUrl} from "@/helpers/podcasts.helper"

const DiscoverPodcasts = ({countries, languages, popularPodcasts, meta, breadcrumbs}) => {
  useEffect(() => {
    pageViewEvent("discover_podcasts", meta.canonical)
  }, [meta.canonical])

  return (
    <Layout title={meta.title} description={meta.description}>

      <Head>
        <meta itemProp="name" content={meta.title}/>
        <meta itemProp="description" content={meta.description}/>

        <link rel="canonical" href={meta.canonical}/>

        <meta property="og:site_name" content="Zeno.FM"/>
        <meta property="og:title" content={meta.title}/>
        <meta property="og:type" content="website"/>
        <meta property="og:url" content={meta.canonical}/>
        <meta property="og:description" content={meta.description}/>

        <meta name="twitter:card" content="summary_large_image"/>
        <meta name="twitter:site" content="@Zeno_Media"/>
        <meta name="twitter:title" content={meta.title}/>
        <meta name="twitter:description" content={meta.description}/>
      </Head>

      <Breadcrumbs breadcrumbs={breadcrumbs} heading="Discover Podcasts"/>

      <Container>
        <Section title="Discover Podcasts" borderBottom={true} heightAuto={true}>
          <DiscoverPodcastsBy/>
        </Section>

        {
          popularPodcasts.length > 0 &&
          <Section title="Popular Podcasts" url={searchLink("podcasts")}>
            <ListGrid items={popularPodcasts}/>
          </Section>
        }
      </Container>

      <div className="my-12 sm:py-12 py-8 bg-primary">
        <div className="container max-w-screen-lg mx-auto px-4">
          <div className="grid md:grid-cols-1 grid-cols-1 gap-6">
            <DiscoverCountryList countries={countries} fullListLink={countryLink()}/>

            <DiscoverLanguageList languages={languages} fullListLink={languageLink()}/>
          </div>
        </div>
      </div>

      <GetStarted/>

    </Layout>
  )
}

export default DiscoverPodcasts

export async function getStaticProps() {
  const countries = await PodcastRepository().getCountries(10)
  const languages = await PodcastRepository().getLanguages(10)

  const popularPodcasts = await PodcastRepository().search({limit: 12})

  const meta = {
    title: "Discover podcasts",
    description: "Discover and listen to the best podcasts from around the world",
    canonical: podcastUrl()
  }

  const breadcrumbs = [
    {title: "Discover", url: discoverLink()},
    {title: "Podcasts"}
  ]

  return {
    props: {
      countries,
      languages,
      popularPodcasts,
      meta,
      breadcrumbs
    },
    revalidate: 60
  }
}