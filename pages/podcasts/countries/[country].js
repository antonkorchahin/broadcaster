import Breadcrumbs from "@/components/Breadcrumbs"
import DiscoverCountryList from "@/components/Discover/DiscoverCountryList"
import GetStarted from "@/components/GetStarted"
import PodcastCategoryList from "@/components/Podcasts/PodcastCategoryList"
import Section from "@/components/Section"
import {discoverLink, searchLink} from "@/helpers/helpers"
import {pageViewEvent} from "@/lib/ga"
import Head from "next/head"
import React, {useEffect} from "react"
import Layout from "@/layouts/layout"
import Container from "@/components/common/Container"
import {PodcastRepository} from "@/repositories/PodcastRepository"
import ListGrid from "@/components/ListGrid"
import {countryLink, podcastUrl} from "@/helpers/podcasts.helper"

const Country = ({country, podcastCategories, countries, popularPodcasts, meta, breadcrumbs}) => {
  useEffect(() => {
    pageViewEvent("podcasts_country", meta.canonical)
  }, [meta.canonical])

  return (
    <Layout title={meta.title} description={meta.description}>

      <Head>
        <meta itemProp="name" content={meta.title}/>
        <meta itemProp="description" content={meta.description}/>

        <link rel="canonical" href={meta.canonical}/>

        <meta property="og:site_name" content="Zeno.FM"/>
        <meta property="og:title" content={meta.title}/>
        <meta property="og:type" content="website"/>
        <meta property="og:url" content={meta.canonical}/>
        <meta property="og:description" content={meta.description}/>

        <meta name="twitter:card" content="summary_large_image"/>
        <meta name="twitter:site" content="@Zeno_Media"/>
        <meta name="twitter:title" content={meta.title}/>
        <meta name="twitter:description" content={meta.description}/>
      </Head>

      <Breadcrumbs breadcrumbs={breadcrumbs} heading={`Podcasts from ${country.name}`}/>

      <Container>
        <PodcastCategoryList country={country} categories={podcastCategories}/>

        {
          popularPodcasts.length > 0 &&
          <Section title="Popular Podcasts"
                   url={searchLink("podcasts", {country: country.name})}>
            <ListGrid items={popularPodcasts}/>
          </Section>
        }
      </Container>

      <div className="my-12 sm:py-12 py-8 bg-primary">
        <div className="container max-w-screen-lg mx-auto px-4">
          <DiscoverCountryList countries={countries} fullListLink={countryLink()}/>
        </div>
      </div>

      <GetStarted/>

    </Layout>
  )
}

export default Country

export async function getStaticProps(context) {
  let countryName = context.params.country

  let countries = await PodcastRepository().getCountries()

  const country = countries.find(c => c.name === countryName)

  if (typeof country === "undefined") {
    return {
      notFound: true
    }
  }

  const podcastCategories = await PodcastRepository().getCategories(10)
  countries = await PodcastRepository().getCountries(10)

  const params = {
    country: country.name,
    limit: 24
  }

  const popularPodcasts = await PodcastRepository().search(params)

  const meta = {
    title: `Listen to the best podcasts from ${country.name}`,
    description: "Find and listen to best podcasts from around the world",
    canonical: countryLink(country)
  }

  const breadcrumbs = [
    {title: "Discover", url: discoverLink()},
    {title: "Podcasts", url: podcastUrl()},
    {title: "Countries", url: countryLink()},
    {title: country.name}
  ]

  return {
    props: {
      country,
      podcastCategories,
      countries,
      popularPodcasts,
      meta,
      breadcrumbs
    },
    revalidate: 60
  }
}

export async function getStaticPaths() {
  return {
    paths: [],
    fallback: "blocking"
  }
}