import Breadcrumbs from "@/components/Breadcrumbs"
import DiscoverLanguageList from "@/components/Discover/DiscoverLanguageList"
import GetStarted from "@/components/GetStarted"
import PodcastCategoryList from "@/components/Podcasts/PodcastCategoryList"
import Section from "@/components/Section"
import {discoverLink, searchLink} from "@/helpers/helpers"
import {pageViewEvent} from "@/lib/ga"
import Head from "next/head"
import React, {useEffect} from "react"
import Layout from "@/layouts/layout"
import Container from "@/components/common/Container"
import {PodcastRepository} from "@/repositories/PodcastRepository"
import ListGrid from "@/components/ListGrid"
import {languageLink, podcastUrl} from "@/helpers/podcasts.helper"

const Language = ({language, podcastCategories, languages, popularPodcasts, meta, breadcrumbs}) => {
  useEffect(() => {
    pageViewEvent("podcasts_language", meta.canonical)
  }, [meta.canonical])

  return (
    <Layout title={meta.title} description={meta.description}>

      <Head>
        <meta itemProp="name" content={meta.title}/>
        <meta itemProp="description" content={meta.description}/>

        <link rel="canonical" href={meta.canonical}/>

        <meta property="og:site_name" content="Zeno.FM"/>
        <meta property="og:title" content={meta.title}/>
        <meta property="og:type" content="website"/>
        <meta property="og:url" content={meta.canonical}/>
        <meta property="og:description" content={meta.description}/>

        <meta name="twitter:card" content="summary_large_image"/>
        <meta name="twitter:site" content="@Zeno_Media"/>
        <meta name="twitter:title" content={meta.title}/>
        <meta name="twitter:description" content={meta.description}/>
      </Head>

      <Breadcrumbs breadcrumbs={breadcrumbs} heading={`${language.name} Speaking Podcasts`}/>

      <Container>
        <PodcastCategoryList language={language} categories={podcastCategories}/>

        {
          popularPodcasts.length > 0 &&
          <Section title="Popular Podcasts"
                   url={searchLink("podcasts", {language: language.name})}>
            <ListGrid items={popularPodcasts}/>
          </Section>
        }
      </Container>

      <section className="my-12 sm:py-12 py-8 bg-primary">
        <div className="container max-w-screen-lg mx-auto px-4">
          <DiscoverLanguageList languages={languages} fullListLink={languageLink()}/>
        </div>
      </section>

      <GetStarted/>

    </Layout>
  )
}

export default Language

export async function getStaticProps(context) {
  let languageName = context.params.language

  let languages = await PodcastRepository().getLanguages()
  const language = languages.find(c => c.name === languageName)

  if (typeof language === "undefined") {
    return {
      notFound: true
    }
  }

  const podcastCategories = await PodcastRepository().getCategories(10)
  languages = await PodcastRepository().getLanguages(10)

  const params = {
    language: language.name,
    limit: 24
  }

  const popularPodcasts = await PodcastRepository().search(params)

  const meta = {
    title: `Listen to the best ${language.name} speaking podcasts`,
    description: "Find and listen to best podcasts from around the world",
    canonical: languageLink(language)
  }

  const breadcrumbs = [
    {title: "Discover", url: discoverLink()},
    {title: "Podcasts", url: podcastUrl()},
    {title: "Languages", url: languageLink()},
    {title: language.name}
  ]

  return {
    props: {
      language,
      podcastCategories,
      languages,
      popularPodcasts,
      meta,
      breadcrumbs
    },
    revalidate: 60
  }
}

export async function getStaticPaths() {
  return {
    paths: [],
    fallback: "blocking"
  }
}