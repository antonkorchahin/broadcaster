import Breadcrumbs from "@/components/Breadcrumbs"
import {discoverLink} from "@/helpers/helpers"
import Layout from "@/layouts/layout"
import {pageViewEvent} from "@/lib/ga"
import Head from "next/head"
import Link from "next/link"
import React, {useEffect} from "react"
import Container from "@/components/common/Container"
import {PodcastRepository} from "@/repositories/PodcastRepository"
import {categoryLink, podcastUrl} from "@/helpers/podcasts.helper"

const Genres = ({categories, meta, breadcrumbs}) => {
  useEffect(() => {
    pageViewEvent("podcasts_categories", meta.canonical)
  }, [meta.canonical])

  return (
    <Layout title={meta.title} description={meta.description}>

      <Head>
        <meta itemProp="name" content={meta.title}/>
        <meta itemProp="description" content={meta.description}/>

        <link rel="canonical" href={meta.canonical}/>

        <meta property="og:site_name" content="Zeno.FM"/>
        <meta property="og:title" content={meta.title}/>
        <meta property="og:type" content="website"/>
        <meta property="og:url" content={meta.canonical}/>
        <meta property="og:description" content={meta.description}/>

        <meta name="twitter:card" content="summary_large_image"/>
        <meta name="twitter:site" content="@Zeno_Media"/>
        <meta name="twitter:title" content={meta.title}/>
        <meta name="twitter:description" content={meta.description}/>
      </Head>

      <Breadcrumbs breadcrumbs={breadcrumbs} heading={meta.title}/>

      <Container>

        <div className="grid md:grid-cols-3 grid-cols-1 gap-8">
          {
            categories.map((category, index) => (
              <div key={index} className="sm:mb-4 mb-0">
                <h2 className="text-xl font-semibold mb-4">
                  <Link href={category.url} prefetch={false}>
                    <a
                      className="border-b border-primary sm:pb-1 pb-0 hover:text-primary transition hover:border-transparent">{category.name}</a>
                  </Link>
                </h2>

                {category.subcategories.map((category, index) => (
                  <div key={index} className="mb-2 text-gray-600">
                    <Link href={category.url} prefetch={false}>
                      <a className="hover:text-primary hover:underline transition">{category.name}</a>
                    </Link>
                  </div>
                ))}
              </div>
            ))
          }
        </div>
      </Container>
    </Layout>
  )
}

export default Genres

export async function getStaticProps() {
  const categoriesList = await PodcastRepository().getCategories()

  let categories = categoriesList.filter(category => !category.parent).map(category => {
    category.subcategories = categoriesList.filter(subcategory => subcategory.parent === category.id)

    return category
  })

  const meta = {
    title: "Discover Podcasts by Categories",
    description: "Find and listen to best podcasts from around the world",
    canonical: categoryLink()
  }

  const breadcrumbs = [
    {title: "Discover", url: discoverLink()},
    {title: "Podcasts", url: podcastUrl()},
    {title: "Categories"}
  ]

  return {
    props: {
      categories,
      meta,
      breadcrumbs
    },
    revalidate: 60
  }
}