import StationListGrid from "@/components/Stations/StationListGrid"
import {searchLink, siteUrl} from "@/helpers/helpers"
import {CgSpinner} from "react-icons/cg"
import {useRouter} from "next/router"
import React, {useEffect, useState} from "react"
import SearchLayout from "@/layouts/searchLayout"
import {RadioRepository} from "@/repositories/RadioRepository"
import {fetchStations} from "@/api/app/station.api"
import useSearchStore from "@/store/useSearchStore"
import {pageViewByStationCountryEvent} from "@/lib/ga"


const defaultLimit = 24

const Radio = ({countries, languages, genres}) => {
  const showFilters = useSearchStore(state => state.showFilters)

  const router = useRouter()

  let {query = '', genre = '', country = '', language = ''} = router.query

  let params = {
    query: query,
    limit: defaultLimit,
    genre: genre,
    country: country,
    language: language,
    page: 1,
  }

  let [stations, setStations] = useState(null)
  let [hasMore, setHasMore] = useState(true)
  let [page, setPage] = useState(1)
  const [loading, setLoading] = useState(false)

  useEffect(() => {
    if (page > 1) {
      params.page = page
      search(params)
    }
  }, [page])

  useEffect(() => {
    setPage(1)

    if (router.isReady) {
      params.page = 1

      if (params.country.length) {
        pageViewByStationCountryEvent(params.country, siteUrl(router.asPath))
      }

      search(params)
    }
  }, [router.query])

  const search = async (params) => {
    setLoading(true)

    const stationList = await fetchStations(params)

    if (!stations || !stations.length || params.page === 1) {
      setStations(stationList)
    } else {
      setStations(stations.concat(stationList))
    }

    setHasMore(stationList.length === defaultLimit)
    setLoading(false)
  }

  const filter = (param, value) => {
    const queryString = router.query

    if (value) {
      queryString[param] = value
    } else {
      delete queryString[param]
    }

    router.push(searchLink("radio", queryString))
  }

  const loadMore = async () => {
    setPage(page + 1)
  }

  return (
    <SearchLayout title="Search Radio" description="Search radio by title, genre, language or location">
      {
        showFilters &&
        <div className="bg-light-gray border-b border-medium-gray pb-4">
          <div className="container max-w-screen-lg mx-auto bg-transparent px-4">
            <div className="sm:grid block grid-cols-3 gap-4">
              <select onChange={(event) => filter("genre", event.target.value)}
                      value={genre ? genre : ""}
                      className="border-medium-gray focus:ring-1 focus:ring-primary focus:border-transparent w-full px-4 py-1 sm:text-base text-sm sm:mb-0 mb-4 rounded-md">
                <option value="">Genre</option>
                {
                  genres.map((option, index) =>
                    <option key={index} value={option}>{option}</option>)
                }
              </select>

              <select onChange={(event) => filter("country", event.target.value)}
                      value={country ? country : ""}
                      className="border-medium-gray focus:ring-1 focus:ring-primary focus:border-transparent w-full px-4 py-1 sm:text-base text-sm sm:mb-0 mb-4 rounded-md">
                <option value="">Country</option>
                {countries.map((option, index) =>
                  <option key={index} value={option}>{option}</option>)}
              </select>

              <select onChange={(event) => filter("language", event.target.value)}
                      value={language ? language : ""}
                      className="border-medium-gray focus:ring-1 focus:ring-primary focus:border-transparent w-full px-4 py-1 sm:text-base text-sm rounded-md">
                <option value="">Language</option>
                {languages.map((option, index) =>
                  <option key={index} value={option}>{option}</option>)}
              </select>
            </div>
          </div>
        </div>
      }

      <div className="container max-w-screen-lg mx-auto py-8 px-4">
        {
          (stations && stations.length > 0) &&
          <StationListGrid stations={stations}/>
        }

        {
          (stations && stations.length === 0) &&
          <div className="text-lg">Sorry, no Stations matched your criteria</div>
        }

        {
          hasMore &&
          <button type="button" onClick={() => loadMore()} className="load-more mt-8">
            {
              loading &&
              <span className="flex-line justify-center">
                <CgSpinner className="animate-spin text-2xl mr-1"/>
                Loading
              </span>
            }

            {
              !loading &&
              <>Load More</>
            }
          </button>
        }
      </div>
    </SearchLayout>
  )
}

export default Radio

export async function getStaticProps() {
  const countries = (await RadioRepository().getCountries()).map(country => country.name)
  const languages = (await RadioRepository().getLanguages()).map(language => language.name)
  const genres = (await RadioRepository().getGenres()).map(genre => genre.name)

  return {
    props: {
      countries,
      languages,
      genres,
    },
    revalidate: 60
  }
}