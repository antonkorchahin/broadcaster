import {searchLink} from "@/helpers/helpers"
import {CgSpinner} from "react-icons/cg"
import {useRouter} from "next/router"
import React, {useEffect, useState} from "react"
import SearchLayout from "@/layouts/searchLayout"
import {PrayerLineRepository} from "@/repositories/PrayerLineRepository"
import {fetchPrayerLines} from "@/api/app/prayer-lines.api"
import useSearchStore from "@/store/useSearchStore"
import ListGrid from "@/components/ListGrid"


const defaultLimit = 24

const PrayerLines = ({religions}) => {
  const showFilters = useSearchStore(state => state.showFilters)

  const router = useRouter()

  let {query = '', religion = ''} = router.query

  let params = {
    query: query,
    limit: defaultLimit,
    religion: religion,
    page: 1,
  }

  let [prayerLines, setPrayerLines] = useState(null)
  let [hasMore, setHasMore] = useState(true)
  let [page, setPage] = useState(1)
  const [loading, setLoading] = useState(false)

  useEffect(() => {
    if (page > 1) {
      params.page = page
      search(params)
    }
  }, [page])

  useEffect(() => {
    setPage(1)

    if (router.isReady) {
      params.page = 1

      search(params)
    }
  }, [router.query])

  const search = async (params) => {
    setLoading(true)

    const prayerLineList = await fetchPrayerLines(params)

    if (!prayerLines || !prayerLines.length || params.page === 1) {
      setPrayerLines(prayerLineList)
    } else {
      setPrayerLines(prayerLines.concat(prayerLineList))
    }

    setHasMore(prayerLineList.length === defaultLimit)
    setLoading(false)
  }

  const filter = (param, value) => {
    const queryString = router.query

    if (value) {
      queryString[param] = value
    } else {
      delete queryString[param]
    }

    router.push(searchLink("prayer-lines", queryString))
  }

  const loadMore = async () => {
    setPage(page + 1)
  }

  return (
    <SearchLayout title="Search Prayer Lines" description="Search prayer lines by name or religion">

      {
        showFilters &&
        <div className="bg-light-gray border-b border-medium-gray pb-4">
          <div className="container max-w-screen-lg mx-auto bg-transparent px-4">
            <div className="sm:grid block grid-cols-3 gap-4">
              <select onChange={(event) => filter("religion", event.target.value)}
                      value={religion ? religion : ""}
                      className="border-medium-gray focus:ring-1 focus:ring-primary focus:border-transparent w-full px-4 py-1 sm:text-base text-sm rounded-md">
                <option value="">Religion</option>
                {
                  religions.map((option, index) =>
                    <option key={index} value={option}>{option}</option>)
                }
              </select>
            </div>
          </div>
        </div>
      }

      <div className="container max-w-screen-lg mx-auto py-8 px-4">
        {
          (prayerLines && prayerLines.length > 0) &&
          <ListGrid items={prayerLines}/>
        }

        {
          (prayerLines && prayerLines.length === 0) &&
          <div className="text-lg">Sorry, no Prayer Lines matched your criteria</div>
        }

        {
          hasMore &&
          <button type="button" onClick={() => loadMore()} className="load-more mt-8">
            {
              loading &&
              <span className="flex-line justify-center">
                <CgSpinner className="animate-spin text-2xl mr-1"/>
                Loading
              </span>
            }

            {
              !loading &&
              <>Load More</>
            }
          </button>
        }
      </div>
    </SearchLayout>
  )
}

export default PrayerLines

export async function getStaticProps() {
  const religions = (await PrayerLineRepository().getReligions()).map(religion => religion.name)

  return {
    props: {
      religions,
    },
    revalidate: 60
  }
}