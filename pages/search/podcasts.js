import {searchLink} from "@/helpers/helpers"
import {CgSpinner} from "react-icons/cg"
import {useRouter} from "next/router"
import React, {useEffect, useState} from "react"
import SearchLayout from "@/layouts/searchLayout"
import {fetchPodcasts} from "@/api/app/podcasts.api"
import {PodcastRepository} from "@/repositories/PodcastRepository"
import useSearchStore from "@/store/useSearchStore"
import ListGrid from "@/components/ListGrid"


const defaultLimit = 24

const Podcast = ({countries, languages, categories}) => {
  const showFilters = useSearchStore(state => state.showFilters)

  const router = useRouter()

  let {query = '', category = '', country = '', language = ''} = router.query

  let params = {
    query: query,
    limit: defaultLimit,
    category: category,
    country: country,
    language: language,
    page: 1
  }

  let [podcasts, setPodcasts] = useState(null)
  let [hasMore, setHasMore] = useState(true)
  let [page, setPage] = useState(1)
  const [loading, setLoading] = useState(false)

  useEffect(() => {
    if (page > 1) {
      params.page = page
      search(params)
    }
  }, [page])

  useEffect(() => {
    setPage(1)

    if (router.isReady) {
      params.page = 1

      search(params)
    }
  }, [router.query])

  const search = async (params) => {
    setLoading(true)

    const podcastList = await fetchPodcasts(params)

    if (!podcasts || !podcasts.length || params.page === 1) {
      setPodcasts(podcastList)
    } else {
      setPodcasts(podcasts.concat(podcastList))
    }

    setHasMore(podcastList.length === defaultLimit)
    setLoading(false)
  }

  const filter = (param, value) => {
    const queryString = router.query

    if (value) {
      queryString[param] = value
    } else {
      delete queryString[param]
    }

    router.push(searchLink("podcasts", queryString))
  }

  const loadMore = async () => {
    setPage(page + 1)
  }

  return (
    <SearchLayout title="Search Podcasts" description="Search podcasts by title, category, language or location">

      {
        showFilters &&
        <div className="bg-light-gray border-b border-medium-gray pb-4">
          <div className="container max-w-screen-lg mx-auto bg-transparent px-4">
            <div className="sm:grid block grid-cols-3 gap-4">
              <select onChange={(event) => filter("category", event.target.value)}
                      value={category ? category : ""}
                      className="border-medium-gray focus:ring-1 focus:ring-primary focus:border-transparent w-full px-4 py-1 sm:text-base text-sm sm:mb-0 mb-4 rounded-md">
                <option value="">Category</option>
                {categories.map((option, index) =>
                  <option key={index} value={option}>{option}</option>)}
              </select>

              <select onChange={(event) => filter("country", event.target.value)}
                      value={country ? country : ""}
                      className="border-medium-gray focus:ring-1 focus:ring-primary focus:border-transparent w-full px-4 py-1 sm:text-base text-sm sm:mb-0 mb-4 rounded-md  ">
                <option value="">Country</option>
                {countries.map((option, index) =>
                  <option key={index} value={option}>{option}</option>)}
              </select>

              <select onChange={(event) => filter("language", event.target.value)}
                      value={language ? language : ""}
                      className="border-medium-gray focus:ring-1 focus:ring-primary focus:border-transparent w-full px-4 py-1 sm:text-base text-sm rounded-md">
                <option value="">Language</option>
                {languages.map((option, index) =>
                  <option key={index} value={option}>{option}</option>)}
              </select>
            </div>
          </div>
        </div>
      }

      <div className="container max-w-screen-lg mx-auto py-8 px-4">
        {
          (podcasts && podcasts.length > 0) &&
          <ListGrid items={podcasts}/>
        }

        {
          (podcasts && podcasts.length === 0) &&
          <div className="text-lg">Sorry, no Podcasts matched your criteria</div>
        }

        {
          hasMore &&
          <button type="button" onClick={() => loadMore()} className="load-more mt-8">
            {
              loading &&
              <span className="flex-line justify-center">
                <CgSpinner className="animate-spin text-2xl mr-1"/>
                Loading
              </span>
            }

            {
              !loading &&
              <>Load More</>
            }
          </button>
        }
      </div>
    </SearchLayout>
  )
}

export default Podcast

export async function getStaticProps() {
  const countries = (await PodcastRepository().getCountries()).map(country => country.name)
  const languages = (await PodcastRepository().getLanguages()).map(language => language.name)
  const categories = (await PodcastRepository().getCategories()).map(category => category.id)

  return {
    props: {
      countries,
      languages,
      categories,
    },
    revalidate: 60
  }
}