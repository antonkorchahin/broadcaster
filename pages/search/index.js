import Section from "@/components/Section"
import StationListRow from "@/components/Stations/StationListRow"
import {searchLink} from "@/helpers/helpers"
import {useRouter} from "next/router"
import React, {useEffect, useState} from "react"
import SearchLayout from "@/layouts/searchLayout"
import {fetchPodcasts} from "@/api/app/podcasts.api"
import {fetchStations} from "@/api/app/station.api"
import ListRow from "@/components/ListRow"


const Search = () => {
  const router = useRouter()

  let {query = ''} = router.query

  let [stations, setStations] = useState(null)
  let [podcasts, setPodcasts] = useState(null)

  useEffect(() => {
    if (typeof window !== "undefined") {
      if (!window.location.href.includes("query") || (typeof query !== "undefined")) {
        const params = {query: query, limit: 12}

        fetchStations(params).then(stations => {
          setStations(stations)
        })

        fetchPodcasts(params).then(podcasts => {
          setPodcasts(podcasts)
        })
      }
    }
  }, [query])

  return (
    <SearchLayout title="Search Broadcasters" description="Search Broadcasters">
      <div className="container max-w-screen-lg mx-auto px-4 pt-8">
        <Section title="Top Radio" url={searchLink("radio")} borderBottom={true}>
          {
            stations &&
            stations.length > 0 &&
            <StationListRow stations={stations}/>
          }
        </Section>

        <Section title="Top Podcasts" url={searchLink("podcasts")}>
          {
            podcasts &&
            podcasts.length > 0 &&
            <ListRow items={podcasts}/>
          }
        </Section>
      </div>
    </SearchLayout>
  )
}

export default Search