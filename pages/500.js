import {discoverLink, siteUrl} from "@/helpers/helpers"
import Layout from "@/layouts/layout"
import {pageViewEvent} from "@/lib/ga"
import Image from "next/image"
import {useRouter} from "next/router"
import React, {useEffect} from "react"
import Link from "next/link"

const Custom500 = () => {
  const router = useRouter()

  useEffect(() => {
    pageViewEvent("5xx", siteUrl(router.asPath))
  }, [router.asPath])

  return (
    <Layout title="Ooops... Something went wrong" description="Ooops... Something went wrong" withGap={false}>
      <div className="flex-grow flex items-stretch items-baseline bg-primary w-full">
        <div className="sm:w-5/12 relative">
          <Image alt="Server error"
                 src="/images/mic.webp"
                 layout="fill"
                 objectFit="cover"
          />
        </div>

        <div className="self-center mx-auto p-4">
          <h1 className="sm:text-6xl text-4xl font-bold mb-6">
            Ooops...<br/>
            Something went wrong.
          </h1>

          <p className="mb-8">Please try to refresh this page, or <a className="underline"
                                                                     href="mailto:support@zenomedia.com">contact our
            support team</a> if the problem persists.</p>

          <Link href={discoverLink()} prefetch={false}>
            <a className="py-4 px-6 text-center rounded-full font-bold bg-black text-white sm:inline-block block">
              Discover Broadcasters
            </a>
          </Link>
        </div>
      </div>
    </Layout>
  )
}

export default Custom500