import Breadcrumbs from "@/components/Breadcrumbs"
import {discoverLink, siteUrl} from "@/helpers/helpers"
import {pageViewEvent} from "@/lib/ga"
import Head from "next/head"
import React, {useEffect} from "react"
import Layout from "@/layouts/layout"
import Container from "@/components/common/Container"
import ListGrid from "@/components/ListGrid"
import {PartnerRepository} from "@/repositories/PartnerRepository";

const Partners = ({partners, meta, breadcrumbs}) => {
  useEffect(() => {
    pageViewEvent("partners", meta.canonical)
  }, [meta.canonical])

  return (
    <Layout title={meta.title} description={meta.description}>

      <Head>
        <meta itemProp="name" content={meta.title}/>
        <meta itemProp="description" content={meta.description}/>

        <link rel="canonical" href={meta.canonical}/>

        <meta property="og:site_name" content="Zeno.FM"/>
        <meta property="og:title" content={meta.title}/>
        <meta property="og:type" content="website"/>
        <meta property="og:url" content={meta.canonical}/>
        <meta property="og:description" content={meta.description}/>

        <meta name="twitter:card" content="summary_large_image"/>
        <meta name="twitter:site" content="@Zeno_Media"/>
        <meta name="twitter:title" content={meta.title}/>
        <meta name="twitter:description" content={meta.description}/>
      </Head>

      <Breadcrumbs breadcrumbs={breadcrumbs} heading="Our Partners"/>

      <Container>
        {
          partners &&
          partners.length > 0 &&
          <ListGrid items={partners}/>
        }
      </Container>
    </Layout>
  )
}

export default Partners

export async function getStaticProps() {
  const partners = await PartnerRepository().list()

  let meta = {
    title: `Zeno Media Partners`,
    description: "Zeno Media Partners",
    canonical: siteUrl("/partners/")
  }

  const breadcrumbs = [
    {title: "Discover", url: discoverLink()},
    {title: "Partners"}
  ]

  return {
    props: {
      partners,
      meta,
      breadcrumbs
    },
    revalidate: 60
  }
}