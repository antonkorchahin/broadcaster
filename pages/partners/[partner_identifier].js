import Breadcrumbs from "@/components/Breadcrumbs"
import StationListItem from "@/components/Stations/StationListItem"
import {discoverLink} from "@/helpers/helpers"
import {partnerUrl} from "@/helpers/partners.helper"
import Layout from "@/layouts/layout"
import {pageViewEvent} from "@/lib/ga"
import Discover from "@/pages/discover"
import Head from "next/head"
import React, {useEffect} from "react"
import Container from "@/components/common/Container"
import Description from "@/components/common/Description"
import TheImage from "@/components/common/TheImage"
import ListItem from "@/components/ListItem"
import {PartnerRepository} from "@/repositories/PartnerRepository";

const Partner = ({partner, meta, breadcrumbs}) => {
  useEffect(() => {
    pageViewEvent("partner", meta.canonical)
  }, [meta.canonical])

  return (
    <Layout title={meta.title} description={meta.title}>

      <Head>
        <meta itemProp="name" content={meta.title}/>
        <meta itemProp="description" content={meta.description}/>
        <meta itemProp="image" content={meta.logo}/>

        <link rel="canonical" href={meta.canonical}/>

        <meta property="og:site_name" content="Zeno.FM"/>
        <meta property="og:title" content={meta.title}/>
        <meta property="og:type" content="website"/>
        <meta property="og:url" content={meta.canonical}/>
        <meta property="og:description" content={meta.description}/>
        <meta property="og:image" content={meta.logo}/>

        <meta name="twitter:card" content="summary_large_image"/>
        <meta name="twitter:site" content="@Zeno_Media"/>
        <meta name="twitter:title" content={meta.title}/>
        <meta name="twitter:description" content={meta.description}/>
        <meta name="twitter:image" content={meta.logo}/>
        <meta name="twitter:image:alt" content={meta.title}/>
      </Head>

      <Breadcrumbs breadcrumbs={breadcrumbs}/>

      <Container>
        <div className="grid grid-cols-12 gap-4 border-b border-gray-200 pb-4 mb-4">
          <div className="md:row-span-2 row-span-1 sm:col-span-3 col-span-5">
            <TheImage src={partner.logo} alt={partner.name}/>
          </div>

          <div className="sm:col-span-9 col-span-7 flex">
            <h1 className="md:text-3xl text-xl font-semibold break-words sm:self-end self-center">
              {partner.name}
            </h1>
          </div>

          <div className="md:col-span-9 col-span-12">
            <Description text={partner.description}/>
          </div>
        </div>

        {
          partner.assets.length > 0 &&
          <div className="grid xl:grid-cols-6 lg:grid-cols-4 md:grid-cols-3 grid-cols-2 gap-4">
            {
              partner.assets.map((asset, index) => {
                if (asset.type === "station") {
                  return <StationListItem key={index} station={asset}/>
                } else if (asset.type === "podcast") {
                  return <ListItem key={index} item={asset}/>
                }
              })
            }
          </div>
        }
      </Container>
    </Layout>
  )
}

export default Partner

export async function getStaticProps(context) {
  const partner = await PartnerRepository().get(context.params.partner_identifier)

  if (!partner) {
    return {
      notFound: true
    }
  }

  const meta = {
    title: partner.name,
    description: partner.name,
    logo: partner.logo,
    canonical: partner.url
  }

  const breadcrumbs = [
    {title: "Discover", url: discoverLink()},
    {title: "Partners", url: partnerUrl()},
    {title: partner.name}
  ]

  return {
    props: {
      partner,
      meta,
      breadcrumbs
    },
    revalidate: 60
  }
}

export async function getStaticPaths() {
  return {
    paths: [],
    fallback: "blocking"
  }
}