import "@/styles/globals.scss"
import {GA_TRACKING_ID, page_view} from "@/lib/ga"
import React, {useEffect} from "react"
import {useRouter} from "next/router"
import dynamic from 'next/dynamic'
import {getGDPR} from "@/api/zenofm/gdpr.api"
import useCookieConsentStore from "@/store/useCookieConsentStore"
import Script from "next/script"
import Player from "@/components/Player/Player";

const CookieConsent = dynamic(
  () => import('@/components/CookieConsent'),
  {ssr: false}
)

const MyApp = ({Component, pageProps}) => {
  const setLocation = useCookieConsentStore((state) => state.setLocation)

  const analyticsScriptsAllowed = useCookieConsentStore((state) => state.computed.analyticsEnabled)

  const router = useRouter()

  useEffect(() => {
    const handleRouteChange = (url) => {
      page_view(url)
    }
    //When the component is mounted, subscribe to router changes
    //and log those page views
    router.events.on("routeChangeComplete", handleRouteChange)

    // If the component is unmounted, unsubscribe
    // from the event with the `off` method
    return () => {
      router.events.off("routeChangeComplete", handleRouteChange)
    }
  }, [router.events])

  useEffect(() => {
    getGDPR().then(async (data) => {
      if (typeof data.gdprCountry !== 'undefined') {
        setLocation(data)
      }
    })
  }, [])

  return (
    <>
      <CookieConsent/>

      {
        analyticsScriptsAllowed &&
        <>
          <Script
            strategy="afterInteractive"
            src={`https://www.googletagmanager.com/gtag/js?id=${GA_TRACKING_ID}`}
          />
        </>
      }

      <Component {...pageProps} />

      <Player/>
    </>
  )
}

export default MyApp