import {discoverLink, siteUrl} from "@/helpers/helpers"
import Layout from "@/layouts/layout"
import {pageViewEvent} from "@/lib/ga"
import Image from "next/image"
import {useRouter} from "next/router"
import React, {useEffect} from "react"
import Link from "next/link"

const Custom404 = () => {
  const router = useRouter()

  useEffect(() => {
    pageViewEvent("404", siteUrl(router.asPath))
  }, [router.asPath])

  return (
    <Layout title="404 Page Not Found" description="404 Page Not Found" withGap={false}>
      <div className="flex-grow flex items-stretch items-baseline bg-primary w-full">
        <div className="sm:w-5/12 relative">
          <Image alt="404 Page Not Found"
                 src="/images/mic.webp"
                 layout="fill"
                 objectFit="cover"
          />
        </div>

        <div className="self-center mx-auto p-4">
          <h1 className="sm:text-6xl text-4xl font-bold mb-6">
            404<br/>
            Page Not Found
          </h1>

          <p className="mb-8">Sorry, the page you are looking for could not be found.</p>

          <Link href={discoverLink()} prefetch={false}>
            <a className="py-4 px-6 text-center rounded-full font-bold bg-black text-white sm:inline-block block">
              Discover Broadcasters
            </a>
          </Link>
        </div>
      </div>
    </Layout>
  )
}

export default Custom404