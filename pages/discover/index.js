import DiscoverCountryList from "@/components/Discover/DiscoverCountryList"
import DiscoverLanguageList from "@/components/Discover/DiscoverLanguageList"
import DiscoverPodcastsBy from "@/components/Discover/DiscoverPodcastsBy"
import DiscoverRadioBy from "@/components/Discover/DiscoverRadioBy"
import Section from "@/components/Section"
import StationListRow from "@/components/Stations/StationListRow"
import {discoverLink} from "@/helpers/helpers"
import {partnerUrl} from "@/helpers/partners.helper"
import {prayerLineUrl} from "@/helpers/prayer-line.helper"
import {countryLink, languageLink, radioURL} from "@/helpers/station.helpers"
import Layout from "@/layouts/layout"
import {pageViewEvent} from "@/lib/ga"
import Head from "next/head"
import React, {useEffect} from "react"
import {PodcastRepository} from "@/repositories/PodcastRepository"
import {RadioRepository} from "@/repositories/RadioRepository"
import {PrayerLineRepository} from "@/repositories/PrayerLineRepository"
import ListRow from "@/components/ListRow"
import {podcastUrl} from "@/helpers/podcasts.helper"
import {PartnerRepository} from "@/repositories/PartnerRepository";

const Discover = ({
                    countries,
                    languages,
                    partners,
                    popularStations,
                    popularPodcasts,
                    meta
                  }) => {
  useEffect(() => {
    pageViewEvent("discover", meta.canonical)
  }, [meta.canonical])

  return (
    <Layout title={meta.title} description={meta.description} withGap={false}>

      <Head>
        <meta itemProp="name" content={meta.title}/>
        <meta itemProp="description" content={meta.description}/>

        <link rel="canonical" href={meta.canonical}/>

        <meta property="og:site_name" content="Zeno.FM"/>
        <meta property="og:title" content={meta.title}/>
        <meta property="og:type" content="website"/>
        <meta property="og:url" content={meta.canonical}/>
        <meta property="og:description" content={meta.description}/>

        <meta name="twitter:card" content="summary_large_image"/>
        <meta name="twitter:site" content="@Zeno_Media"/>
        <meta name="twitter:title" content={meta.title}/>
        <meta name="twitter:description" content={meta.description}/>
      </Head>

      <div className="bg-light-gray sm:mb-8 mb-4 border-b border-medium-gray">
        <section className="container max-w-screen-lg mx-auto px-4 sm:py-12 py-6 bg-light-gray">
          <h1 className="md:text-5xl sm:text-3xl text-2xl text-center font-bold">
            Discover and listen to the best radio stations and podcasts from around the world
          </h1>
        </section>
      </div>

      <div className="container max-w-screen-lg mx-auto px-4">

        <div className="mb-4 pb-4 border-b border-gray-200">
          <h2
            className="md:text-2xl text-xl font-semibold mb-2">
            Discover Radio
          </h2>

          <DiscoverRadioBy/>
        </div>

        {
          popularStations.length > 0 &&
          <Section title="Popular Radio Stations" url={radioURL()}>
            <StationListRow stations={popularStations}/>
          </Section>
        }
      </div>

      <div className="bg-secondary text-white my-8 py-8">
        <div className="container max-w-screen-lg mx-auto px-4">
          <Section title="Our Partners" url={partnerUrl()}>
            <ListRow items={partners}/>
          </Section>
        </div>
      </div>

      <div className="container max-w-screen-lg mx-auto px-4">

        <div className="mb-4 pb-4 border-b border-gray-200">
          <h2
            className="md:text-2xl text-xl font-semibold mb-2">
            Discover Podcasts
          </h2>

          <DiscoverPodcastsBy/>
        </div>

        {
          popularPodcasts.length > 0 &&
          <Section title="Popular Podcasts" url={podcastUrl()}>
            <ListRow items={popularPodcasts}/>
          </Section>
        }
      </div>

      <section className="bg-primary mt-8 py-8">
        <div className="container max-w-screen-lg mx-auto px-4">
          <div className="grid md:grid-cols-1 grid-cols-1 gap-6">
            <DiscoverCountryList countries={countries} fullListLink={countryLink()}/>

            <DiscoverLanguageList languages={languages} fullListLink={languageLink()}/>
          </div>
        </div>
      </section>
    </Layout>
  )
}

export default Discover

export async function getStaticProps() {
  const countries = await RadioRepository().getCountries(10)
  const languages = await RadioRepository().getLanguages(10)

  const partners = await PartnerRepository().list(6)

  const popularStations = await RadioRepository().search({limit: 12})
  const popularPodcasts = await PodcastRepository().search({limit: 12})

  const meta = {
    title: "Discover Broadcasters",
    description: "Discover and listen to the best radio stations, podcasts and prayer lines from around the world",
    canonical: discoverLink()
  }

  return {
    props: {
      countries,
      languages,
      partners,
      popularStations,
      popularPodcasts,
      meta
    },
    revalidate: 60
  }
}