import QuickSearch from "@/components/QuickSearch"
import {Disclosure} from "@headlessui/react"
import {FiMenu, FiUser} from "react-icons/fi"
import {HiX} from "react-icons/hi"
import {FiPlus} from "react-icons/fi"
import {FiSearch} from "react-icons/fi"
import {AiOutlineCompass} from "react-icons/ai"
import {discoverLink, siteUrl} from "@/helpers/helpers"
import Link from "next/link"
import {partnerUrl} from "@/helpers/partners.helper"
import {prayerLineUrl} from "@/helpers/prayer-line.helper"
import {radioURL} from "@/helpers/station.helpers"
import {FiRadio} from "react-icons/fi"
import {FaHandshake} from "react-icons/fa"
import {FaPrayingHands} from "react-icons/fa"
import {CgMediaPodcast} from "react-icons/cg"
import {podcastUrl} from "@/helpers/podcasts.helper"

const MainMenu = () => {

  return (
    <Disclosure as="nav" className="bg-secondary text-white lg:px-10 md:px-8 px-5">
      {({open}) => (
        <>
          <div className="relative flex-line justify-between md:h-20 h-14">

            <div className="flex-line">
              <Link href={siteUrl()} prefetch={false}>
                <a className="text-2xl text-white sm:mr-8 mr-4 hover:text-opacity-80">
                  <svg width="68" height="16" viewBox="0 0 68 16" xmlns="http://www.w3.org/2000/svg"
                       aria-label="Zeno.FM">
                    <g className="animate-fill" fill="#FEFEFE">
                      <path
                        d="M50.653 2.073A1.845 1.845 0 1 0 48.35 3.86v9.049L36.415.972v11.252a1.845 1.845 0 1 0 .912 0V3.175l11.936 11.936V3.86a1.845 1.845 0 0 0 1.39-1.787M13.782 11.936c-.862 0-1.583.591-1.786 1.39h-9.05L14.883 1.388H3.63a1.845 1.845 0 1 0 0 .912h9.05L.743 14.238h11.252a1.844 1.844 0 1 0 1.786-2.302M60.454 14.815a6.78 6.78 0 0 1-6.773-6.774 6.78 6.78 0 0 1 6.773-6.772 6.78 6.78 0 0 1 6.773 6.772 6.78 6.78 0 0 1-6.773 6.774m0-12.393a5.626 5.626 0 0 0-5.62 5.62 5.626 5.626 0 0 0 5.62 5.62 5.626 5.626 0 0 0 5.62-5.62 5.626 5.626 0 0 0-5.62-5.62M19.952 14.238h11.275v-.942H19.952v.942zm0-11.907h11.275v-.942H19.952v.942zm0 5.954h11.275v-.942H19.952v.942z"/>
                    </g>
                  </svg>
                </a>
              </Link>

              <a
                href="https://tools.zeno.fm/auth/realms/broadcasters/protocol/openid-connect/registrations?client_id=zeno-tools&redirect_uri=https%3A%2F%2Ftools.zeno.fm%2F&response_mode=fragment&response_type=code&scope=openid"
                className="md:flex hidden flex-row items-center font-semibold hover:text-primary mr-6 transition"
                target="_blank"
                rel="noopener noreferrer">
                <FiPlus className="mr-1 text-primary text-xl"/>
                Create
              </a>

              <Link href={discoverLink()} prefetch={false}>
                <a
                  className="md:flex hidden md:ml-0 ml-auto font-semibold hover:text-primary flex-row items-center mr-6 transition">
                  <AiOutlineCompass className="mr-1 text-primary text-xl"/>
                  Discover
                </a>
              </Link>

              <div className="md:block hidden">
                <QuickSearch/>
              </div>
            </div>

            <div className="hidden md:flex ml-auto">
              <a target="_blank" rel="noopener noreferrer"
                 href="https://tools.zeno.fm/auth/realms/broadcasters/protocol/openid-connect/auth?client_id=zeno-tools&redirect_uri=https%3A%2F%2Ftools.zeno.fm%2F&response_mode=fragment&response_type=code&scope=openid"
                 className="font-semibold hover:text-primary flex flex-row items-center mr-4">
                <FiUser className="mr-2"/>
                Log in
              </a>

              <a target="_blank" rel="noopener noreferrer"
                 href="https://tools.zeno.fm/auth/realms/broadcasters/protocol/openid-connect/registrations?client_id=zeno-tools&redirect_uri=https%3A%2F%2Ftools.zeno.fm%2F&response_mode=fragment&response_type=code&scope=openid"
                 className="font-semibold py-2 px-6 bg-white rounded-full text-black hover:bg-primary">
                Sign up
              </a>
            </div>

            <div className="flex h-full items-center md:hidden ml-auto">
              <Disclosure.Button className="justify-center text-primary focus:outline-none">
                <span className="sr-only">Open main menu</span>
                {open ? (
                  <HiX className="w-6 h-6" aria-hidden="true"/>
                ) : (
                  <FiMenu className="w-6 h-6" aria-hidden="true"/>
                )}
              </Disclosure.Button>
            </div>

          </div>

          <Disclosure.Panel className="md:hidden">
            <div className="py-2 font-medium">

              <Link href={discoverLink()} prefetch={false}>
                <a
                  className="flex-line block py-3 hover:text-primary">
                  <AiOutlineCompass className="mr-4 text-primary text-xl" aria-hidden="true"/>
                  Discover
                </a>
              </Link>

              <Link href={radioURL()} prefetch={false}>
                <a
                  className="flex-line block py-3 hover:text-primary">
                  <FiRadio className="mr-4 text-primary text-xl" aria-hidden="true"/>
                  Radio
                </a>
              </Link>

              <Link href={podcastUrl()} prefetch={false}>
                <a
                  className="flex-line block py-3 hover:text-primary">
                  <CgMediaPodcast className="mr-4 text-primary text-xl" aria-hidden="true"/>
                  Podcasts
                </a>
              </Link>

              <Link href={prayerLineUrl()} prefetch={false}>
                <a
                  className="flex-line block py-3 hover:text-primary">
                  <FaPrayingHands className="mr-4 text-primary text-xl" aria-hidden="true"/>
                  Prayer Lines
                </a>
              </Link>

              <Link href={partnerUrl()} prefetch={false}>
                <a
                  className="flex-line block py-3 hover:text-primary">
                  <FaHandshake className="mr-4 text-primary text-xl" aria-hidden="true"/>
                  Partners
                </a>
              </Link>

              <Link href={siteUrl("/search/")} prefetch={false}>
                <a className="flex-line block py-3 hover:text-primary">
                  <FiSearch className="mr-4 text-primary text-xl" aria-hidden="true"/>
                  Search
                </a>
              </Link>

              <div className="flex ml-auto space-x-4 justify-center my-4">
                <a target="_blank" rel="noopener noreferrer"
                   href="https://tools.zeno.fm/auth/realms/broadcasters/protocol/openid-connect/auth?client_id=zeno-tools&redirect_uri=https%3A%2F%2Ftools.zeno.fm%2F&response_mode=fragment&response_type=code&scope=openid"
                   className="font-semibold hover:text-primary flex flex-row items-center mr-4">
                  <FiUser className="mr-2"/>
                  Log in
                </a>

                <a target="_blank" rel="noopener noreferrer"
                   href="https://tools.zeno.fm/auth/realms/broadcasters/protocol/openid-connect/registrations?client_id=zeno-tools&redirect_uri=https%3A%2F%2Ftools.zeno.fm%2F&response_mode=fragment&response_type=code&scope=openid"
                   className="font-semibold py-2 px-6 bg-white rounded-full text-black hover:bg-primary">
                  Sign up
                </a>
              </div>

            </div>
          </Disclosure.Panel>
        </>
      )}
    </Disclosure>
  )

}

export default MainMenu