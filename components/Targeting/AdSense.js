import useCookieConsentStore from "@/store/useCookieConsentStore"
import dynamic from "next/dynamic"

const AdSenseAdUnit = dynamic(
  () => import('@/components/Targeting/AdSenseAdUnit'),
  {ssr: false}
)

const AdSense = ({
                   client = process.env.NEXT_PUBLIC_GOOGLE_ADSENSE,
                   slot,
                   adKey = '',
                   style = {display: "block"},
                   format = 'auto',
                   layout = 'display',
                   responsive = true,
                   className = '',
                   minBreakpoint,
                   maxBreakpoint
                 }) => {
  const targetingScriptsAllowed = useCookieConsentStore((state) => state.computed.targetingEnabled)

  if (!targetingScriptsAllowed) {
    return null
  }

  return (
    <AdSenseAdUnit
      client={client}
      slot={slot}
      adKey={adKey}
      style={style}
      format={format}
      layout={layout}
      responsive={responsive}
      className={className}
      minBreakpoint={minBreakpoint}
      maxBreakpoint={maxBreakpoint}
    ></AdSenseAdUnit>
  )
}

export default AdSense