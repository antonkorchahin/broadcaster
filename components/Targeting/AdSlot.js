import useCookieConsentStore from "@/store/useCookieConsentStore"
import dynamic from "next/dynamic"
import React from "react";

const Primis = dynamic(
  () => import('@/components/Targeting/Primis'),
  {ssr: false}
)

const AdSense = dynamic(
  () => import('@/components/Targeting/AdSense'),
  {ssr: false}
)

const AdSlot = ({
                  client = process.env.NEXT_PUBLIC_GOOGLE_ADSENSE,
                  slot,
                  adKey = '',
                  style = {display: "block"},
                  format = 'auto',
                  layout = 'display',
                  responsive = true,
                  className = '',
                  minBreakpoint,
                  maxBreakpoint
                }) => {
  const targetingScriptsAllowed = useCookieConsentStore((state) => state.computed.targetingEnabled)
  const country = useCookieConsentStore((state) => state.computed.country)

  if (!targetingScriptsAllowed || !country) {
    return null
  }

  return (
    <>
      {
        (country === 'US' || country === 'CA') &&
        <div className={className}>
          <Primis/>
        </div>
      }

      {
        (country !== 'US' && country !== 'CA') &&
        <AdSense slot={slot} style={style} className={className} format={format}/>
      }
    </>
  )
}

export default AdSlot