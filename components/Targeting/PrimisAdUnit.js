import Script from "next/script"
import {useEffect} from "react"
import useCookieConsentStore from "@/store/useCookieConsentStore"

const PrimisAdUnit = () => {
  const targetingScriptsAllowed = useCookieConsentStore((state) => state.computed.targetingEnabled)

  const refreshAllSlots = () => {
    if (typeof googletag !== "undefined") {
      googletag.cmd.push(() => {
        googletag.pubads().refresh();
      })
    }
  }

  useEffect(() => {
    if (targetingScriptsAllowed) {
      refreshAllSlots()
    }
  }, [targetingScriptsAllowed])

  return (
    <>
      <Script id="primis-script" strategy="lazyOnload">
        {`
          window.googletag = window.googletag || {cmd: []};
          googletag.cmd.push(function() {
            googletag.defineSlot('/21869305879/Primis_VDU', [1, 1], 'div-gpt-ad-1660664003458-0').addService(googletag.pubads());
            googletag.pubads().enableSingleRequest();
            googletag.enableServices();
          });                   
        `}
      </Script>

      <div id='div-gpt-ad-1660664003458-0'>
        <Script id="primis-ad-unit" strategy="lazyOnload">
          {`googletag.cmd.push(function() { googletag.display('div-gpt-ad-1660664003458-0'); });`}
        </Script>
      </div>
    </>
  )
}

export default PrimisAdUnit