import useCookieConsentStore from "@/store/useCookieConsentStore"
import dynamic from "next/dynamic"

const PrimisAdUnit = dynamic(
  () => import('@/components/Targeting/PrimisAdUnit'),
  {ssr: false}
)

const Primis = () => {
  const targetingScriptsAllowed = useCookieConsentStore((state) => state.computed.targetingEnabled)

  if (!targetingScriptsAllowed) {
    return null
  }

  return (
    <PrimisAdUnit/>
  )
}

export default Primis