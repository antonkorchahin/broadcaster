import {useRouter} from "next/router"
import React, {useEffect} from "react"
import useCookieConsentStore from "@/store/useCookieConsentStore"
import {useState} from "react"

const AdSenseAdUnit = ({
                         client = process.env.NEXT_PUBLIC_GOOGLE_ADSENSE,
                         slot,
                         adKey = '',
                         style = {display: "block"},
                         format = 'auto',
                         layout = 'display',
                         responsive = true,
                         className = '',
                         minBreakpoint = 0,
                         maxBreakpoint = 10000
                       }) => {
  const [state, setState] = useState({
    windowWidth: window.innerWidth,
    isDesiredWidth: false,
  });

  const targetingScriptsAllowed = useCookieConsentStore((state) => state.computed.targetingEnabled)

  const router = useRouter()

  adKey = router.asPath + (adKey ? adKey : '')

  const loadAds = () => {
    try {
      if (typeof window !== "undefined") {
        (window.adsbygoogle = window.adsbygoogle || []).push({})
      }
    } catch (error) {
      console.log("AdSense error:", error.message)
    }
  }

  const resizeHandler = () => {
    const currentWindowWidth = window.innerWidth
    const isDesiredWidth = currentWindowWidth > minBreakpoint && currentWindowWidth < maxBreakpoint
    setState({windowWidth: currentWindowWidth, isDesiredWidth})
  };

  useEffect(() => {
    window.addEventListener("resize", resizeHandler);
    return () => window.removeEventListener("resize", resizeHandler);
  }, [state.windowWidth]);

  useEffect(() => {
    if (targetingScriptsAllowed) {
      if (state.isDesiredWidth) {
        loadAds()
      } else {
        resizeHandler()
      }
    }
  }, [router.asPath, targetingScriptsAllowed, state.isDesiredWidth])

  return (
    <>
      <style>{
        `
          ins.adsbygoogle[data-ad-status="unfilled"] {
            display: none !important;
          }
        `
      }
      </style>

      {
        state.isDesiredWidth &&
        <ins key={adKey} className={`adsbygoogle ${className}`}
             style={style}
             data-ad-client={client}
             data-ad-slot={slot}
             data-ad-layout={layout}
             data-ad-format={format}
             data-full-width-responsive={responsive}></ins>
      }
    </>
  )
}

export default AdSenseAdUnit