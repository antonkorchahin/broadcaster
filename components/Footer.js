import {siteUrl} from "@/helpers/helpers"
import Image from "next/image"
import {BiCookie} from "react-icons/bi"

const Footer = () => {
  return (
    <footer className="bg-light-gray border-0 border-t">
      <div className="container mx-auto max-w-screen-lg pt-8 p-4">
        <nav className="grid sm:grid-cols-4 grid-cols-2 gap-4 mb-4 pb-6 border-0 border-b">
          <div>
            <h3>Creators</h3>
            <ul>
              <li><a href={siteUrl("/streaming/")}>Radio Streaming</a></li>
              <li><a href={siteUrl("/podcasting/")}>Podcast</a></li>
              <li><a href={siteUrl("/plans/")}>Plans</a></li>
            </ul>
          </div>
          <div>
            <h3>Partner With Us</h3>
            <ul>
              <li><a href={siteUrl("/advertise/")}>Advertise</a></li>
              <li><a href={siteUrl("/zenoapi/")}>Zeno API</a></li>
              <li><a href={siteUrl("/app-network/")}>App Network</a></li>
            </ul>
          </div>
          <div>
            <h3>About us</h3>
            <ul>
              <li><a href="https://www.zenomedia.com/about-us/">What we do</a></li>
              <li><a href="https://www.zenomedia.com/about-us/team/">Team</a></li>
              <li><a href="mailto:support@zenomedia.com">Contact</a></li>
            </ul>
          </div>
          <div>
            <h3>Follow us on social media!</h3>
            <ul>
              <li>
                <a href="https://www.facebook.com/ZenoMediaNY" target="_blank" rel="noopener noreferrer">Facebook</a>
              </li>
              <li>
                <a href="https://www.instagram.com/zeno_media/" target="_blank" rel="noopener noreferrer">Instagram</a>
              </li>
              <li><a href="https://twitter.com/Zeno_Media" target="_blank" rel="noopener noreferrer">Twitter</a></li>
              <li>
                <a href="https://www.youtube.com/channel/UCIHzLt7WuvzJCvS1z-PP3Rg" target="_blank"
                   rel="noopener noreferrer">YouTube</a>
              </li>
            </ul>
          </div>
        </nav>

        <div className="flex flex-wrap flex-row items-center text-sm">
          <div className="flex-line sm:mx-0 mx-auto sm:mb-0 mb-6">
            <Image src="/images/logo.png" alt="Zeno FM" width={48} height={48}/>
            <div className="ml-4">© {new Date().getFullYear()} Zeno FM. All rights reserved.</div>
          </div>

          <div className="sm:ml-auto sm:mr-0 mx-auto">
            <ul className="flex-line">
              <li className="mr-4">
                <a href="https://zeno.fm/privacy.pdf"
                   target="_blank" rel="noopener noreferrer">
                  Privacy
                </a>
              </li>
              <li className="mr-4">
                <a href="https://zeno.fm/terms.pdf"
                   target="_blank" rel="noopener noreferrer">
                  Terms
                </a>
              </li>
              <li className="mr-4"><a href={siteUrl("/cookies_policy.pdf")}>Cookies Policy</a></li>
              <li className="mr-4"><a href={siteUrl("/gdpr-privacy-policy")}>GDPR</a></li>
              <li>
                <button title="Cookie settings" className="rounded-full text-secondary hover:text-primary transition"
                        data-cc="c-settings">
                  <BiCookie className="w-10 h-10"/>
                </button>
              </li>
            </ul>
          </div>
        </div>
      </div>
    </footer>
  )
}

export default Footer