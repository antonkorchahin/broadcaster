import ListItem from "@/components/ListItem"

const ListRow = ({items}) => {
  return (
    <div className="overflow-x-auto">
      <div className="sm:grid flex grid-cols-6 gap-4 sm:pb-0 pb-4">
        {items.map(item => (
          <div key={item.url} className="sm:flex-shrink flex-shrink-0 sm:w-auto w-36 last:mr-0">
            <ListItem item={item}/>
          </div>
        ))}
      </div>
    </div>
  )
}

export default ListRow