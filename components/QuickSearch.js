import {siteUrl} from "@/helpers/helpers"
import {useRouter} from "next/router"

const QuickSearch = () => {
  const router = useRouter()

  const search = event => {
    event.preventDefault()

    router.push(siteUrl("/search/?query=" + encodeURIComponent(event.target.query.value)))
  }

  return (
    <div className="relative ml-auto">
      <form onSubmit={search}>
        <input type="text" name="query" placeholder="Search by name"
               className="lg:w-80 w-64 bg-secondary-light focus:ring-none focus:ring-primary focus:border-transparent border-0 form-input pl-4 pr-12 py-2 rounded-full"/>
      </form>
    </div>
  )
}

export default QuickSearch