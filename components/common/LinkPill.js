import Link from "next/link"

const LinkPill = (props) => {
  return (
    <Link href={props.href} prefetch={false}>
      <a
        className={"py-1 px-2 sm:mr-2 mr-4 sm:mb-2 mb-0 rounded text-sm border border-gray-200 hover:border-secondary bg-gray-100 hover:bg-secondary hover:text-white font-semibold flex-shrink-0 " + (props.active ? ' bg-secondary text-white ' : '') + props.className}>
        {props.children}
      </a>
    </Link>
  )
}

export default LinkPill