import React, {useEffect, useRef, useState} from "react"

const Description = ({text, isClamped = true}) => {
  const description = useRef(null);

  let [clamped, setClamped] = useState(isClamped)

  useEffect(() => {
    if (isClamped) {
      setClamped(description.current.scrollHeight > description.current.clientHeight)
    }
  }, [isClamped])

  return (
    <>
      <div ref={description} className={"description opacity-80" + (clamped ? ' line-clamp-5' : '')}
           dangerouslySetInnerHTML={{
             __html: text
           }}/>

      {
        clamped &&
        <span className="text-sm underline cursor-pointer" onClick={() => setClamped(false)}>more</span>
      }
    </>
  )
}

export default Description