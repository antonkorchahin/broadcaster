const HeadingDetails = (props) => {
  return <h2 className="text-sm opacity-80 uppercase font-semibold mb-2">{props.children}</h2>
}

export default HeadingDetails