import Image from "next/image"
import {useState} from "react"

const TheImage = ({
                    src = null,
                    alt,
                    size = null,
                    compact = false,
                    width = 240,
                    height = 240,
                    priority = false,
                    itemProp = false,
                    className = '',
                    noBorder = false,
                  }) => {
  const [imageSrc, setImageSrc] = useState(src ? src : '/images/placeholder.webp')

  if (size) {
    width = size
    height = size
  }

  let imageStyles = "flex rounded " + className

  if (!noBorder) {
    imageStyles += " border group-hover:border-primary border-gray-200"
    imageStyles += compact ? ' p-0.5' : ' p-1'
  }

  return (
    <div
      className={imageStyles}>
      <Image className="rounded"
             src={imageSrc}
             width={width}
             height={height}
             priority={priority}
             quality={100}
             onError={() => setImageSrc('/images/placeholder.webp')}
             alt={alt}/>
    </div>
  )
}

export default TheImage