const Container = (props) => {
  return (
    <div className="container max-w-screen-lg mx-auto px-4">
      {props.children}
    </div>
  )
}

export default Container