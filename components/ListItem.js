import Link from "next/link"
import TheImage from "@/components/common/TheImage"

const ListItem = ({item}) => {
  const title = typeof item.title !== 'undefined' ? item.title : item.name
  const logo = typeof item.logo !== 'undefined' && item.logo ? item.logo : '/images/placeholder.webp'

  return (
    <div className="group">
      <Link href={item.url} prefetch={false}>
        <a aria-label={title}>
          <TheImage src={logo} size={268} alt={title}/>

          <span className="group-hover:underline md:text-base text-sm mt-1 line-clamp-2 ln-wrap-line text-center">
            {title}
          </span>
        </a>
      </Link>
    </div>
  )
}

export default ListItem