import ListItem from "@/components/ListItem"

const ListGrid = ({items}) => {
  return (
    <div className="grid md:grid-cols-6 sm:grid-cols-4 grid-cols-2 gap-4">
      {items.map(item => (
        <ListItem item={item} key={item.url}/>
      ))}
    </div>
  )
}

export default ListGrid