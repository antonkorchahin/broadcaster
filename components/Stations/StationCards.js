import StationCardSupportDonations from "./StationCardSupportDonations"
import StationCardSocialMedia from "./StationCardSocialMedia"
import StationCardAnnouncements from "./StationCardAnnouncements"
import StationCardWebsite from "./StationCardWebsite"
import StationCardAppStore from "./StationCardAppStore"
import React from "react"
import {useEffect} from "react"
import {useRouter} from "next/router"
import dynamic from "next/dynamic";

const AdSlot = dynamic(
  () => import('@/components/Targeting/AdSlot'),
  {ssr: false}
)

const StationCards = ({station, stationCards}) => {
  const router = useRouter()

  useEffect(() => {
    if (typeof window !== "undefined" && typeof googletag !== "undefined") {
      googletag.cmd.push(function () {
        googletag.pubads().refresh();
      });
    }
  }, [router.asPath])

  return (
    <div className="bg-light-gray">

      <div className="container max-w-screen-lg mx-auto px-4 py-6">

        {
          !station.featured &&
          <AdSlot slot="3067681739" className="mb-6"/>
        }

        <div id="station-cards" className="box-border mx-auto md:masonry before:box-inherit after:box-inherit">
          {
            (typeof stationCards.CardSupportDonations.paypalUrl != "undefined" ||
              typeof stationCards.CardSupportDonations.cashappUrl != "undefined") &&
            <StationCardSupportDonations card={stationCards.CardSupportDonations}/>
          }

          {
            typeof stationCards.CardWebsite.name != "undefined" &&
            <StationCardWebsite station={station} card={stationCards.CardWebsite}/>
          }

          {
            (
              typeof stationCards.CardSocialMedia.facebookUrl != "undefined" ||
              typeof stationCards.CardSocialMedia.twitterUrl != "undefined" ||
              typeof stationCards.CardSocialMedia.instagramUrl != "undefined" ||
              typeof stationCards.CardSocialMedia.snapchatUrl != "undefined" ||
              typeof stationCards.CardSocialMedia.youtubeUrl != "undefined" ||
              typeof stationCards.CardSocialMedia.facebookUrl != "undefined"
            ) &&
            <StationCardSocialMedia card={stationCards.CardSocialMedia}/>
          }

          <StationCardAppStore card={stationCards.CardAppStore}/>

          {
            stationCards.CardAnnouncement.announcements.length > 0 &&
            <StationCardAnnouncements card={stationCards.CardAnnouncement}/>
          }
        </div>
      </div>
    </div>
  )
}

export default StationCards