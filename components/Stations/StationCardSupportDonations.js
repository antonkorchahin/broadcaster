import * as ga from "@/lib/ga"
import {siteUrl} from "@/helpers/helpers"
import {useRouter} from "next/router"

const StationCardSupportDonations = ({card}) => {
  const router = useRouter()

  const trackDonationClick = (type) => {
    ga.event({
      action: `donate_${type}_click`,
      params: {
        event_category: "donation",
        event_label: siteUrl(router.asPath)
      }
    })
  }

  return (
    <div>
      <h2>Support Your Broadcaster</h2>

      <p>As you know, we are an independent broadcast station. For us, It has always been hard to maintain our broadcast
        without your support.</p>
      <br/>
      <p>To support us, please click to the donate button and then choose the app you’ll donate with. Thank you very
        much!</p>

      {
        typeof card.paypalUrl != "undefined" &&
        <a onClick={() => trackDonationClick("paypal")}
           className="block my-4 py-3 text-center px-6 rounded-full font-bold bg-paypal hover:bg-paypal-hover text-white"
           href={card.paypalUrl} target="_blank" rel="noopener noreferrer">
          Donate with PayPal
        </a>
      }

      {
        typeof card.cashappUrl != "undefined" &&
        <a onClick={() => trackDonationClick("cash_app")}
           className="block my-4 py-3 text-center px-6 rounded-full font-bold bg-cashapp hover:bg-cashapp-hover text-black"
           href={card.cashappUrl} target="_blank" rel="noopener noreferrer">
          Donate with Cash App
        </a>
      }
    </div>
  )
}

export default StationCardSupportDonations