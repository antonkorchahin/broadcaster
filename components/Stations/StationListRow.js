import StationListItem from "@/components/Stations/StationListItem"

const StationListRow = ({stations}) => {
  return (
    <div className="overflow-x-auto">
      <div className="sm:grid flex grid-cols-6 gap-4 sm:pb-0 pb-4">
        {stations.map((station, index) => (
          <div key={index} className="sm:flex-shrink flex-shrink-0 sm:w-auto w-36 last:mr-0">
            <StationListItem station={station}/>
          </div>
        ))}
      </div>
    </div>
  )
}

export default StationListRow