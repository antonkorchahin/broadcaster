import StationListItem from "@/components/Stations/StationListItem"
import React, {Fragment} from "react"

const StationListGrid = ({stations}) => {
  return (
    <div className="grid md:grid-cols-6 sm:grid-cols-4 grid-cols-2 gap-4">
      {stations.map((station, index) => (
        <Fragment key={index}>
          <StationListItem station={station}/>
        </Fragment>
      ))}
    </div>
  )
}

export default StationListGrid