import {FaFacebook} from "react-icons/fa"
import {FaSnapchatGhost} from "react-icons/fa"
import {FaSpotify} from "react-icons/fa"
import {FaTwitter} from "react-icons/fa"
import {FaYoutube} from "react-icons/fa"
import {AiFillInstagram} from "react-icons/ai"

const StationCardSocialMedia = ({card}) => {

  return (
    <div id="card-social-media">
      <h2>Follow us</h2>

      <p className="mb-2">Let’s follow our station on social media!</p>

      {
        typeof card.facebookUrl != "undefined" &&
        <a href={card.facebookUrl} target="_blank" rel="noopener noreferrer"
           className="flex-line py-3 hover:text-primary">
                    <span className="follow-icon mr-2">
                      <FaFacebook/>
                    </span>
          Follow on Facebook
        </a>
      }

      {
        typeof card.twitterUrl != "undefined" &&
        <a href={card.twitterUrl} target="_blank" rel="noopener noreferrer"
           className="flex-line py-3 hover:text-primary">
                    <span className="follow-icon mr-2">
                        <FaTwitter/>
                    </span>
          Follow on Twitter
        </a>
      }

      {
        typeof card.instagramUrl != "undefined" &&
        <a href={card.instagramUrl} target="_blank" rel="noopener noreferrer"
           className="flex-line py-3 hover:text-primary">
                    <span className="follow-icon mr-2">
                        <AiFillInstagram/>
                    </span>
          Follow on Instagram
        </a>
      }

      {
        typeof card.snapchatUrl != "undefined" &&
        <a href={card.snapchatUrl} target="_blank" rel="noopener noreferrer"
           className="flex-line py-3 hover:text-primary">
                    <span className="follow-icon mr-2">
                        <FaSnapchatGhost/>
                    </span>
          Follow on Snapchat
        </a>
      }

      {
        typeof card.youtubeUrl != "undefined" &&
        <a href={card.youtubeUrl} target="_blank" rel="noopener noreferrer"
           className="flex-line py-3 hover:text-primary">
                    <span className="follow-icon mr-2">
                        <FaYoutube/>
                    </span>
          Follow on Youtube
        </a>
      }

      {
        typeof card.spotifyUrl != "undefined" &&
        <a href={card.spotifyUrl} target="_blank" rel="noopener noreferrer"
           className="flex-line py-3 hover:text-primary">
                    <span className="follow-icon mr-2">
                        <FaSpotify/>
                    </span>
          Follow on Spotify
        </a>
      }

    </div>
  )
}

export default StationCardSocialMedia