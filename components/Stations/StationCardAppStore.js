import * as ga from "@/lib/ga"
import Image from "next/image"
import {FaApple} from "react-icons/fa"
import {FaGooglePlay} from "react-icons/fa"
import {siteUrl} from "@/helpers/helpers"
import {useRouter} from "next/router"
import {getOptimizedImage} from "@/helpers/images.helper"

const StationCardAppStore = ({card}) => {
  const router = useRouter()

  const trackAppClick = (type) => {
    ga.event({
      action: `${type}_click`,
      params: {
        event_category: "get_app",
        event_label: siteUrl(router.asPath)
      }
    })
  }

  return (
    <div id="card-app-store">

      <div>
        <h2 className="text-xl font-medium text-center">Get the app for free</h2>

        <div className="p-8 text-center">
          <Image src={getOptimizedImage(card.qrCodeUrl, 200, 200)} className="rounded" height={200} width={200}
                 alt="QR"/>
        </div>
      </div>

      <div className="flex text-sm">
        <a href={card.appLinkApple} onClick={() => trackAppClick("app_store")} target="_blank" rel="noopener noreferrer"
           className="flex flex-row flex-1 mr-1 items-center py-2 rounded bg-app hover:bg-opacity-80">
          <span className="flex mx-auto">
            <span className="text-3xl mr-1">
              <FaApple/>
            </span>

            <span>
              <span className="block text-xs">Available on the</span>
              <span>App Store</span>
            </span>
          </span>
        </a>

        <a href={card.appLinkGoogle} onClick={() => trackAppClick("google_play")} target="_blank" rel="noopener noreferrer"
           className="flex flex-row flex-1 ml-1 items-center py-2 rounded bg-app hover:bg-opacity-80">
          <span className="flex mx-auto">
            <span className="text-3xl mr-1">
              <FaGooglePlay/>
            </span>

            <span>
              <span className="block text-xs">Get it on</span>
              <span>Google Play</span>
            </span>
          </span>
        </a>
      </div>
    </div>
  )

}

export default StationCardAppStore