import Link from "next/link"
import {useRouter} from "next/router"

const StationNavbar = ({station}) => {
  const router = useRouter()

  return (
    <div className="sm:py-5 py-4 sticky top-0 z-50 bg-white shadow-md flex justify-center space-x-8 font-semibold">
      <Link href={station.url} prefetch={false}>
        <a
          className={"sm:text-lg cursor-pointer " + (router.pathname === "/radio/[pretty_url]" ? "text-secondary border-b-2 border-primary" : " text-gray-500")}>
          Radio
        </a>
      </Link>

      {
        station.hasPodcasts &&
        <Link href={`${station.url}podcasts/`} prefetch={false}>
          <a
            className={"sm:text-lg cursor-pointer " + (router.pathname === "/radio/[pretty_url]/podcasts" ? "text-secondary border-b-2 border-primary" : " text-gray-500")}>
            Podcasts
          </a>
        </Link>
      }

      {
        station.hasVideo &&
        <Link href={`${station.url}video/`} prefetch={false}>
          <a
            className={"sm:text-lg cursor-pointer " + (router.pathname === "/radio/[pretty_url]/video" ? "text-secondary border-b-2 border-primary" : " text-gray-500")}>
            Video
          </a>
        </Link>
      }

    </div>
  )
}

export default StationNavbar