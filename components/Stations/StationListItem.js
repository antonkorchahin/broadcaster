import Image from "next/image"
import Link from "next/link"
import {MdStars} from "react-icons/md"

const StationListItem = ({station}) => {
  return (
    <div className="group" itemScope itemType="https://schema.org/RadioStation">
      <Link href={station.url} prefetch={false}>
        <a aria-label={station.name}
           className="relative rounded overflow-hidden flex border border-gray-200 group-hover:border-primary transition p-1 text-center">

          {
            station.sponsored &&
            <div
              className="shadow-md absolute flex-line justify-center top-2 left-2 rounded-md text-white z-20 font-bold bg-primary text-xs px-2 py-1">
              <MdStars className="h-4 w-4 mr-1"/>
              Sponsored
            </div>
          }

          <Image
            src={station.logo}
            height={268}
            width={268}
            className="rounded z-0"
            itemProp="logo"
            alt={station.name}
          />
        </a>
      </Link>

      <Link href={station.url} prefetch={false}>
        <a itemProp="url"
           className="group-hover:underline md:text-base text-sm my-1 line-clamp-2 transition text-center">
          <span>
            {
              station.featured &&
              <MdStars className="h-5 w-5 mr-1 text-primary inline-block"/>
            }
            <span itemProp="name">
              {station.name}
            </span>
          </span>
        </a>
      </Link>
    </div>
  )
}

export default StationListItem