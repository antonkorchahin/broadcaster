const StationCardAnnouncements = ({card}) => {

  const getPrettyDate = (unix_time) => {
    let date = new Date(unix_time * 1000)

    const options = {year: "numeric", month: "long", day: "numeric"}

    return date.toLocaleDateString("en-US", options)
  }

  return (
    <div>
      <h2>Announcements</h2>

      {card.announcements.map((announcement) => (

        <article className="border-b last:border-0 py-4" key={announcement.id}>
          <h3 className="mb-2 font-semibold break-words">{announcement.title}</h3>
          <p className="mb-2 break-words">{announcement.content}</p>
          <div className="text-gray-800 font-semibold text-sm">{getPrettyDate(announcement.publishDate)}</div>
        </article>

      ))}

    </div>
  )
}

export default StationCardAnnouncements