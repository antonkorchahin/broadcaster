import Section from "@/components/Section"
import {countryGenreLink, genreLink, languageGenreLink} from "@/helpers/station.helpers"
import LinkPill from "@/components/common/LinkPill"
import {categoryLink} from "@/helpers/podcasts.helper"

const StationGenreList = ({genres, country = null, language = null, genre = null, borderBottom = true}) => {
  if (country) {
    genres = genres.map(g => {
      return {
        name: g.name,
        url: countryGenreLink(country.name, g.name)
      }
    })
  } else if (language) {
    genres = genres.map(g => {
      return {
        name: g.name,
        url: languageGenreLink(language.name, g.name)
      }
    })
  } else {
    genres = genres.map(g => {
      return {
        name: g.name,
        url: genreLink(g)
      }
    })
  }

  return (
    <Section title="Top Genres" url={categoryLink()} heightAuto={true} borderBottom={borderBottom}>
      <div className="overflow-x-auto">
        <div className="flex flex-row sm:flex-wrap flex-nowrap items-center font-semibold sm:pb-0 pb-4">
          {genres.map((item, index) => (
            <LinkPill key={index} href={item.url} active={genre && item.name === genre.name}>
              {item.name}
            </LinkPill>
          ))}
        </div>
      </div>
    </Section>
  )
}

export default StationGenreList