import Section from "@/components/Section"
import StationListRow from "@/components/Stations/StationListRow"
import {countryGenreLink, languageGenreLink, radioURL} from "@/helpers/station.helpers"

const StationRecommendedStations = ({station, recommended}) => {
  let discoverLink = radioURL()

  if (station.country) {
    discoverLink = countryGenreLink(station.country, station.genre)
  } else if (station.languages.length) {
    discoverLink = languageGenreLink(station.languages[0], station.genre)
  }

  return (
    <div className="container mx-auto max-w-screen-lg mt-8 px-4">

      <Section title="Similar Stations"
               url={discoverLink}>
        <StationListRow stations={recommended}/>
      </Section>
    </div>
  )
}

export default StationRecommendedStations