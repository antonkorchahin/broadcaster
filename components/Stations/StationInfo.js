import Link from "next/link"
import Image from "next/image"
import {searchLink} from "@/helpers/helpers"
import {FiHash} from "react-icons/fi"
import {FiMapPin} from "react-icons/fi"
import {FiMic} from "react-icons/fi"
import PlayerBtnPlaybackRadio from "@/components/Player/PlayerBtnPlaybackRadio";

const StationInfo = ({station}) => {
  return (
    <div className="relative bg-transparent">
      <div className="w-full bg-black h-full absolute overflow-hidden" style={{"zIndex": "-1"}}>
        <Image className="opacity-30"
               alt={station.name}
               priority={true}
               src={station.background}
               layout="fill"
               objectFit="cover"
               quality={70}
        />
      </div>

      <div className="container z-50 bg-transparent max-w-screen-lg mx-auto pt-8 pb-6 px-4 text-white" itemScope
           itemType="https://schema.org/RadioStation">

        <div className="grid grid-cols-12 gap-4">
          <div className="md:row-span-2 row-span-1 sm:col-span-3 col-span-4">
            <div className="rounded p-1">
              <Image className="rounded" itemProp="logo"
                     src={station.logo}
                     layout="responsive"
                     height={240}
                     width={240}
                     quality={100}
                     priority={true}
                     alt={station.name}/>
            </div>
          </div>

          <div className="sm:col-span-9 col-span-8">
            <h1 className="md:text-3xl font-semibold text-xl break-words mb-4" itemProp="name">
              <Link href={station.url}>
                <a itemProp="url">
                  {station.name}
                </a>
              </Link>
            </h1>

            <div>
              <PlayerBtnPlaybackRadio radio={station}/>
            </div>
          </div>

          <div className="md:col-span-9 col-span-12">
            {
              station.description &&
              <div itemProp="description" className="description" dangerouslySetInnerHTML={{
                __html: station.description
              }}/>
            }

            <div className="sm:text-sm text-xs mt-4 flex flex-wrap">
              {
                station.genre &&
                <Link href={searchLink("radio", {genre: station.genre})} prefetch={false}>
                  <a className="tag">
                    <span>
                      <FiHash className="w-4"/>
                      {station.genre}
                    </span>
                  </a>
                </Link>
              }

              {
                (station.country || station.city) &&
                <Link href={searchLink("radio", {country: station.country})} prefetch={false}>
                  <a className="tag">
                    <span>
                      <FiMapPin className="w-4"/>
                      <span itemProp="location">{station.city ? station.city + ", " : ""} {station.country}</span>
                    </span>
                  </a>
                </Link>
              }

              {
                station.languages.length > 0 &&
                station.languages.map(language => (
                  <Link href={searchLink("radio", {language: language})} key={language} prefetch={false}>
                    <a className="tag">
                      <span>
                        <FiMic className="w-4"/>
                        <span itemProp="knowsLanguage">{language}</span>
                      </span>
                    </a>
                  </Link>
                ))
              }
            </div>
          </div>
        </div>
      </div>
    </div>
  )
}

export default StationInfo