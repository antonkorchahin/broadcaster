import Link from "next/link"
import TheImage from "@/components/common/TheImage"

const ParentLink = ({url, logo, title}) => {
  return (
    <Link href={url} prefetch={false}>
      <a className="group flex-line mb-6">
        <TheImage src={logo} size={100} alt={title} className="mr-2 w-14" compact={true}/>

        <span className="group-hover:underline line-clamp-2 ln-wrap-line">
            {title}
        </span>
      </a>
    </Link>
  )
}

export default ParentLink