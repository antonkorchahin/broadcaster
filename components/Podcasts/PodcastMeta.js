import Head from "next/head"


const PodcastMeta = ({meta}) => {
  const {title, description, logo, canonical, amp} = meta

  return (
    <Head>
      <meta itemProp="name" content={title}/>
      <meta itemProp="description" content={description}/>
      <meta itemProp="image" content={logo}/>

      <link rel="canonical" href={canonical}/>

      {
        amp &&
        <link rel="amphtml" href={amp}/>
      }

      <meta property="og:site_name" content="Zeno.FM"/>
      <meta property="og:title" content={title}/>
      <meta property="og:type" content="website"/>
      <meta property="og:url" content={canonical}/>
      <meta property="og:description" content={description}/>
      <meta property="og:image" content={logo}/>

      <meta name="twitter:card" content="summary_large_image"/>
      <meta name="twitter:site" content="@Zeno_Media"/>
      <meta name="twitter:title" content={title}/>
      <meta name="twitter:description" content={description}/>
      <meta name="twitter:image" content={logo}/>
      <meta name="twitter:image:alt" content={title}/>
    </Head>
  )
}

export default PodcastMeta