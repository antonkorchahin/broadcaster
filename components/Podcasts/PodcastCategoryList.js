import Section from "@/components/Section"
import LinkPill from "@/components/common/LinkPill"
import {categoryLink, countryCategoryLink, languageCategoryLink} from "@/helpers/podcasts.helper"

const PodcastCategoryList = ({country = null, language = null, categories, category = null, borderBottom = true}) => {
  if (country) {
    categories = categories.map(g => {
      return {
        id: g.id,
        name: g.name,
        url: countryCategoryLink(country.name, g.name)
      }
    })
  } else if (language) {
    categories = categories.map(g => {
      return {
        id: g.id,
        name: g.name,
        url: languageCategoryLink(language.name, g.name)
      }
    })
  } else {
    categories = categories.map(c => {
      return {
        id: c.id,
        name: c.name,
        url: categoryLink(c)
      }
    })
  }

  return (
    <Section title="Top Categories" url={categoryLink()} heightAuto={true} borderBottom={borderBottom}>
      <div className="overflow-x-auto">
        <div className="flex flex-row sm:flex-wrap flex-nowrap items-center font-semibold sm:pb-0 pb-4">
          {categories.map((item, index) => (
            <LinkPill key={index} href={item.url} active={category && item.name === category.name}>
              {item.name}
            </LinkPill>
          ))}
        </div>
      </div>
    </Section>
  )
}

export default PodcastCategoryList