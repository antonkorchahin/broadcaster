import PodcastEpisodeListItem from "@/components/Podcasts/PodcastEpisodeListItem"
import {CgSpinner} from "react-icons/cg"
import {FiSearch} from "react-icons/fi"
import {BsX} from "react-icons/bs"
import React, {useState} from "react"
import {fetchEpisodes} from "@/api/app/episodes.api"

const PodcastEpisodeList = ({podcast, episodes, episodesCount}) => {
  const defaultOffset = 0
  const defaultLimit = 10

  const [podcastEpisodes, setPodcastEpisodes] = useState(episodes)
  const [selectedOrder, setSelectedOrder] = useState("desc")
  const [searchTerm, setSearchTerm] = useState("")
  const [offset, setOffset] = useState(defaultOffset)
  const [loading, setLoading] = useState(false)
  const [timer, setTimer] = useState(0)
  const [hasMore, setHasMore] = useState(episodesCount > defaultLimit)

  const getEpisodes = async (params) => {
    return await fetchEpisodes({
      podcastKey: podcast.key,
      podcastIdentifier: podcast.identifier,
      podcastType: podcast.type,
      ...params
    })
  }

  function debounce(query, timeout = 500) {
    clearTimeout(timer)
    setTimer(setTimeout(async () => {
      setLoading(true)

      const results = await getEpisodes({
        query: query,
        offset: 0,
        limit: defaultLimit,
        sort_dir: selectedOrder
      })

      if (results.length < defaultLimit) {
        setHasMore(false)
      }

      setPodcastEpisodes(results)
      setLoading(false)
    }, timeout))
  }

  const doSearch = (event) => {
    setSearchTerm(event.target.value)
    debounce(event.target.value)
  }

  const resetSearch = () => {
    setPodcastEpisodes(episodes)
    setSelectedOrder('desc')
    setSearchTerm('')
    setOffset(0)
    setLoading(false)
    setHasMore(episodesCount > defaultLimit)
  }

  const loadMore = async () => {
    setLoading(true)
    let newOffset = offset + defaultLimit

    const results = await getEpisodes({
      query: searchTerm,
      offset: newOffset,
      limit: defaultLimit,
      sort_dir: selectedOrder
    })

    if (results.length < defaultLimit) {
      setHasMore(false)
    }

    setOffset(newOffset)
    setPodcastEpisodes(podcastEpisodes.concat(results))
    setLoading(false)
  }

  const sort = async order => {
    if (!order) {
      order = selectedOrder
    } else {
      setSelectedOrder(order)
    }

    const results = await getEpisodes({
      offset: 0,
      limit: defaultLimit,
      sort_dir: order
    })

    setOffset(0)
    setSearchTerm("")
    setPodcastEpisodes(results)
  }

  return (
    <>
      <div className="flex flex-wrap mb-4 flex-line">
        <div className="text-sm sm:mb-0 mb-4 sm:mr-2 mr-0">
          {episodesCount} {episodesCount === 1 ? 'episode' : 'episodes'}
        </div>

        <div className="relative sm:w-auto w-full flex-grow sm:mb-0 mb-4 sm:mr-2 mr-0">
          {
            loading &&
            <CgSpinner className="animate-spin absolute top-1/4 left-2 opacity-50"/>
          }

          {
            !loading &&
            <FiSearch className="absolute top-1/4 left-2 opacity-50"/>
          }

          <input type="text" placeholder="Search episodes..." onChange={doSearch} value={searchTerm}
                 className="border-gray-300 focus:ring focus:ring-primary focus:border-transparent form-input px-8 py-2 text-sm w-full rounded-md"/>

          {
            searchTerm.length > 0 &&
            <BsX onClick={resetSearch} className="absolute top-1/4 right-4 cursor-pointer opacity-50"/>
          }
        </div>

        <div className="sm:w-auto w-full flex-line justify-end">
          <select value={selectedOrder} onChange={(event) => sort(event.target.value)}
                  className="sm:w-44 w-48 border-gray-300 focus:ring focus:ring-primary focus:border-transparent w-full px-3 py-2 text-sm rounded-md">
            <option value="">Sorted by</option>
            <option value="desc">Published (Newest)</option>
            <option value="asc">Published (Oldest)</option>
          </select>
        </div>
      </div>

      {
        (podcastEpisodes && !!podcastEpisodes.length) &&
        <section className="grid grid-cols-1 gap-4 divide-y mb-8">
          {podcastEpisodes.map(episode => <PodcastEpisodeListItem key={episode.key} episode={episode} podcast={podcast}/>)}
        </section>
      }

      {
        hasMore &&
        <button type="button" onClick={() => loadMore()} className="load-more">
          {
            loading &&
            <span className="flex-line justify-center">
              <CgSpinner className="animate-spin text-2xl mr-1"/>
              Loading
            </span>
          }

          {
            !loading &&
            <span>Load More</span>
          }
        </button>
      }
    </>
  )
}

export default PodcastEpisodeList