import Link from "next/link"
import TheImage from "@/components/common/TheImage"
import PlayerBtnPlaybackEpisode from "@/components/Player/PlayerBtnPlaybackEpisode";


const PodcastEpisodeListItem = ({episode, podcast}) => {
  return (
    <article className="pt-4">

      <div className="grid grid-cols-1 gap-4">

        <Link href={episode.url} key={episode.key} prefetch={false}>
          <a className="group">
            <div className="flex-line">
              <TheImage src={episode.logo} size={75} alt={episode.title} className="sm:mr-4 mr-2 flex-none"
                        compact={true}/>

              <div>
                <h3
                  className="group-hover:underline font-semibold line-clamp-2 ln-wrap-line mb-2">
                  {episode.title}
                </h3>

                <div className="text-sm text-muted">{episode.published_at}</div>
              </div>
            </div>

            {
              episode.summary &&
              <div className="description line-clamp-2 opacity-80 mt-4">
                {episode.summary}
              </div>
            }
          </a>
        </Link>

        <div className="flex-line">
          <PlayerBtnPlaybackEpisode episode={episode} podcast={podcast}/>

          {
            episode.duration &&
            <span className="ml-2">{episode.duration}</span>
          }

        </div>
      </div>
    </article>
  )
}

export default PodcastEpisodeListItem