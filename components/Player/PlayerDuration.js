import usePlayerStore from "@/store/usePlayerStore";

function formatTime(seconds) {
  seconds = parseInt(seconds)
  const h = Math.floor(seconds / 3600)
  const m = Math.floor((seconds % 3600) / 60)
  const s = Math.round(seconds % 60)
  return [
    h,
    m > 9 ? m : (h ? "0" + m : m || "0"),
    s > 9 ? s : "0" + s
  ].filter(Boolean).join(":")
}

const PlayerDuration = () => {
  const duration = usePlayerStore((state) => state.duration)
  const currentTime = usePlayerStore((state) => state.currentTime)

  if (!duration) {
    return
  }

  return (
    <div className="text-sm text-gray-300">
      {formatTime(currentTime)} / {formatTime(duration)}
    </div>
  )
}

export default PlayerDuration