import usePlayerStore from "@/store/usePlayerStore"


const PlayerTitle = () => {
  return usePlayerStore((state) => state.player.title)
}

export default PlayerTitle