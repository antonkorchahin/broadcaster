import {AiFillPauseCircle, AiFillPlayCircle} from "react-icons/ai";
import {CgSpinner} from "react-icons/cg";
import usePlayerStore from "@/store/usePlayerStore";

const PlayerBtnPlayback = () => {
  const isPlaying = usePlayerStore((state) => state.state.isPlaying)
  const isLoading = usePlayerStore((state) => state.state.isLoading)
  const isPaused = usePlayerStore((state) => state.state.isPaused)
  const isStoped = usePlayerStore((state) => state.state.isStoped)

  const play = usePlayerStore((state) => state.play)
  const pause = usePlayerStore((state) => state.pause)

  function handlePause() {
    pause()
  }

  function handlePlay() {
    if (isPaused || isStoped) {
      play()
    }
  }

  if (isLoading) {
    return (
      <CgSpinner className="animate-spin mx-auto text-5xl"/>
    )
  }

  if (isPlaying) {
    return (
      <button className="w-12 h-12 hover:scale-110 transition duration-100 ease-in-out transform active:scale-100"
              onClick={handlePause}>
        <AiFillPauseCircle className="mx-auto text-5xl"/>
      </button>
    )
  }

  if (isPaused || isStoped) {
    return (
      <button className="w-12 h-12 hover:scale-110 transition duration-100 ease-in-out transform active:scale-100"
              onClick={handlePlay}>
        <AiFillPlayCircle className="mx-auto text-5xl"/>
      </button>
    )
  }
}

export default PlayerBtnPlayback