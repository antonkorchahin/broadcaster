import React, {useEffect, useRef, useState} from "react";
import usePlayerStore from "@/store/usePlayerStore";
import {getPartnerIds} from "@/api/adsPartners.api";

const PlayerInputRadio = () => {
  const [src, setSrc] = useState('')

  const playerElement = useRef(null);
  const mediaFile = usePlayerStore((state) => state.player.mediaURL)

  const status = usePlayerStore((state) => state.status)
  const isPlaying = usePlayerStore((state) => state.state.isPlaying)
  const isLoading = usePlayerStore((state) => state.state.isLoading)
  const isPaused = usePlayerStore((state) => state.state.isPaused)
  const isStoped = usePlayerStore((state) => state.state.isStoped)
  const setLoading = usePlayerStore((state) => state.setLoading)

  const play = usePlayerStore((state) => state.play)
  const stop = usePlayerStore((state) => state.stop)

  const volume = usePlayerStore((state) => state.volume)

  useEffect(() => {
    if (!isLoading) {
      if (isPlaying) {
        playerElement.current.play()
      } else if (isPaused || isStoped) {
        playerElement.current.pause()
      }
    }
  }, [status])

  useEffect(() => {
    playerElement.current.volume = volume / 100
  }, [volume])

  useEffect(() => {
    function onLoading() {
      setLoading(true)
    }

    function onPlaying() {
      setLoading(false)
      play()
    }

    function onError() {
      if (src) {
        setLoading(false)
        stop()
      }
    }

    playerElement.current.addEventListener("loadstart", onLoading)
    playerElement.current.addEventListener("canplay", onPlaying)
    playerElement.current.addEventListener("playing", onPlaying)
    playerElement.current.addEventListener("waiting", onLoading)
    playerElement.current.addEventListener("error", onError)

    return () => {
      if (playerElement.current) {
        playerElement.current.pause()

        playerElement.current.removeEventListener("loadstart", onLoading)
        playerElement.current.removeEventListener("canplay", onPlaying)
        playerElement.current.removeEventListener("playing", onPlaying)
        playerElement.current.removeEventListener("waiting", onLoading)
        playerElement.current.removeEventListener("error", onError)
      }
    }
  }, [])

  useEffect(() => {
    const updateSrc = async (newSrc) => {
      const params = await getParams()
      setSrc(newSrc + params)
    }

    if (mediaFile) {
      playerElement.current.pause()

      updateSrc(mediaFile)
    }
  }, [mediaFile])

  useEffect(() => {
    if (src) {
      playerElement.current.src = src
      playerElement.current.load()
    }
  }, [src])

  return <audio id="audioPlayer" ref={playerElement} src={src}></audio>
}

export default PlayerInputRadio

async function getParams() {
  let queryString = ""
  let partnersString = ""

  try {
    const partners = await getPartnerIds()
    partnersString = new URLSearchParams(partners).toString()
  } catch (e) {
    console.warn("Failed to fetch partners ids")
  } finally {

    try {
      const adswizz = com_adswizz_synchro_getListenerId()
      queryString = `?aw_0_req_lsid=${adswizz}`
    } catch (e) {
      console.warn("Failed to fetch adswizz listener id")
    }

    if (partnersString.length) {
      queryString += queryString.length ? "&" + partnersString : "?" + partnersString
    }
  }

  return queryString
}