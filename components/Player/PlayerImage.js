import usePlayerStore from "@/store/usePlayerStore"
import TheImage from "@/components/common/TheImage"

const PlayerImage = () => {
  const image = usePlayerStore((state) => state.player.image)

  if (!image) {
    return
  }

  return (
    <div className="flex-none sm:mr-4 mr-2" style={{width: "60px", height: "60px"}}>
      <TheImage key={image} src={image} size={60} noBorder={true}/>
    </div>
  )
}

export default PlayerImage