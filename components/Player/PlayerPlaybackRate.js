import React, {useState} from "react";
import usePlayerStore from "@/store/usePlayerStore";

const rateValues = [0.5, 0.8, 1, 1.2, 1.5, 2]

const PlayerPlaybackRate = () => {
  let [showOptions, setShowOptions] = useState(false)

  const playbackRate = usePlayerStore((state) => state.playbackRate)
  const setPlaybackRate = usePlayerStore((state) => state.setPlaybackRate)

  function handleClick() {
    setShowOptions(!showOptions)
  }

  function handleRateUpdate(rate) {
    if (!rateValues.includes(rate)) {
      rate = 1
    }

    setPlaybackRate(rate)
    setShowOptions(false)
  }

  return (
    <div className="relative">
      <button onClick={handleClick}
              className="text-center flex-none w-12 h-12 hover:text-white text-gray-300 transition duration-100 ease-in-out transform active:bg-opacity-50 active:bg-secondary rounded-full">
        {playbackRate}x
      </button>

      <div
        className={`bg-secondary text-white absolute shadow-lg w-12 bottom-10 flex-col p-2 text-center space-y-2 rounded text-secondary text-sm ` + (showOptions ? 'flex' : 'hidden')}>
        {rateValues.map((rate, index) => (
          <button key={index} onClick={() => handleRateUpdate(rate)}
                  className={`hover:text-secondary text-gray-300 cursor-pointer hover:bg-primary rounded px-1 ` + (playbackRate === rate ? 'bg-primary text-secondary' : '')}>
            {rate}x
          </button>
        ))}
      </div>
    </div>
  )
}

export default PlayerPlaybackRate