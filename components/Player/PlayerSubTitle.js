import usePlayerStore from "@/store/usePlayerStore"
import Link from "next/link";

const PlayerSubTitle = () => {
  const subTitle = usePlayerStore((state) => state.player.subTitle)
  const canonical = usePlayerStore((state) => state.player.canonical)

  if (!subTitle || !canonical) {
    return
  }

  return (
    <Link href={canonical} prefetch={false}>
      <a className="block text-xs text-gray-300 truncate">
        {subTitle}
      </a>
    </Link>
  )
}

export default PlayerSubTitle