import React, {useEffect, useRef} from "react";
import {FiVolume1, FiVolume2, FiVolumeX} from "react-icons/fi";
import usePlayerStore from "@/store/usePlayerStore";
import {updateProgress} from "@/helpers/progress.helper";

const defaultVolume = 100

const PlayerVolume = () => {
  const progressElement = useRef(null);

  const volume = usePlayerStore((state) => state.volume)
  const setVolume = usePlayerStore((state) => state.setVolume)

  function uplateVolume(newVolume = null) {
    setVolume(parseInt(newVolume))
    updateProgress(progressElement.current)
  }

  function handleChange(event) {
    uplateVolume(event.target.value)
  }

  function handleClick() {
    if (volume) {
      uplateVolume(0)
    } else {
      uplateVolume(defaultVolume)
    }
  }

  useEffect(() => {
    updateProgress(progressElement.current)
  }, [volume])

  return (
    <div className="sm:flex hidden items-center space-x-2">
      {
        volume ?
          (
            <button
              className="flex-none w-12 h-12 hover:text-white text-gray-300 hover:scale-110 transition duration-100 ease-in-out transform active:scale-100 active:bg-opacity-50 active:bg-secondary rounded-full"
              onClick={handleClick}
              aria-label="Mute">
              {
                volume <= 50 &&
                <FiVolume1 className="mx-auto text-xl"/>
              }

              {
                volume > 50 &&
                <FiVolume2 className="mx-auto text-xl"/>
              }
            </button>
          ) :
          (
            <button
              className="flex-none w-12 h-12 hover:text-white text-gray-300 hover:scale-110 transition duration-100 ease-in-out transform active:scale-100 active:bg-opacity-50 active:bg-secondary rounded-full"
              onClick={handleClick}
              aria-label="Unmute">
              <FiVolumeX className="mx-auto text-xl"/>
            </button>
          )
      }

      <input ref={progressElement} className="md:flex hidden cursor-pointer bg-primary" type="range" id="volume"
             name="volume" min="0"
             max="100"
             step="1" value={volume}
             onChange={handleChange}/>
    </div>
  )
}

export default PlayerVolume