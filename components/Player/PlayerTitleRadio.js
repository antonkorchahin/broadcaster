import usePlayerStore from "@/store/usePlayerStore"
import {API_ZENOPLAY_URL} from "@/api/index";
import {useEffect, useState} from "react";

async function getNowPlayingData(streamName) {
  return await fetch(API_ZENOPLAY_URL + `api/zenofm/nowplaying/${streamName}?_=${(new Date).getTime()}`)
    .then(async response => {
      if (response.ok) {
        return response.json()
      }

      throw Error(response.statusText)
    })
    .then(data => data)
    .catch(e => {
    })
}

const PlayerTitleRadio = () => {
  const streamName = usePlayerStore((state) => state.player.streamName)

  const [author, setAuthor] = useState("")
  const [title, setTitle] = useState("")

  useEffect(() => {
    const fetchNowPlaying = () => {
      getNowPlayingData(streamName)
        .then(data => {
          if (data) {
            setAuthor(data.artist.length ? data.artist + " - " : "")

            const title = data.title.trim()

            if (title !== '-') {
              setTitle(data.title)
            }
          }
        })
    }

    fetchNowPlaying()

    let interval = setInterval(() => {
      fetchNowPlaying()
    }, 5000)

    return () => {
      clearInterval(interval)
    }
  }, [streamName])

  return author + title
}

export default PlayerTitleRadio