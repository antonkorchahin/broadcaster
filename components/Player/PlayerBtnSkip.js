import {MdForward30, MdReplay10} from "react-icons/md";
import usePlayerStore from "@/store/usePlayerStore";

const PlayerBtnSkip = ({backward = false}) => {
  const skipTime = usePlayerStore((state) => state.skipTime)

  function handleChange() {
    skipTime(backward)
  }

  return (
    <button onClick={handleChange}
            className="sm:block hidden w-12 h-12 hover:text-white text-gray-300 hover:scale-110 transition duration-100 ease-in-out transform active:scale-100 active:bg-opacity-50 active:bg-secondary rounded-full">
      {
        backward ? <MdReplay10 className="text-4xl mx-auto"/> : <MdForward30 className="text-4xl mx-auto"/>
      }
    </button>
  )
}

export default PlayerBtnSkip