import usePlayerStore from "@/store/usePlayerStore";
import {useEffect, useRef} from "react";
import {updateProgress} from "@/helpers/progress.helper";

const PlayerProgressBar = ({readonly}) => {
  const progressElement = useRef(null);

  const duration = usePlayerStore((state) => state.duration)
  const currentTime = usePlayerStore((state) => state.currentTime)
  const setCurrentTime = usePlayerStore((state) => state.setCurrentTime)

  useEffect(() => {
    updateProgress(progressElement.current)
  }, [currentTime])

  function handleChange(e) {
    if (readonly) {
      return
    }

    const newTime = parseInt(progressElement.current.value)
    setCurrentTime(newTime)
  }

  return (
    <input ref={progressElement} id="track-progress" className="w-full cursor-pointer" type="range" step="1" min={0}
           max={duration} value={currentTime} onChange={handleChange}/>
  )
}

export default PlayerProgressBar