import React, {useEffect, useRef} from "react";
import usePlayerStore from "@/store/usePlayerStore";

const PlayerInputAd = () => {
  const playerElement = useRef(null);
  const src = usePlayerStore((state) => state.ad.mediaFile)

  const status = usePlayerStore((state) => state.status)
  const isPlaying = usePlayerStore((state) => state.state.isPlaying)
  const isLoading = usePlayerStore((state) => state.state.isLoading)
  const isPaused = usePlayerStore((state) => state.state.isPaused)
  const isStoped = usePlayerStore((state) => state.state.isStoped)
  const setLoading = usePlayerStore((state) => state.setLoading)

  const play = usePlayerStore((state) => state.play)
  const stop = usePlayerStore((state) => state.stop)

  const volume = usePlayerStore((state) => state.volume)

  const setDuration = usePlayerStore((state) => state.setDuration)
  const currentTime = usePlayerStore((state) => state.currentTime)
  const setCurrentTime = usePlayerStore((state) => state.setCurrentTime)

  const resetAd = usePlayerStore((state) => state.resetAd)
  const duration = usePlayerStore((state) => state.duration)
  const ad = usePlayerStore((state) => state.ad)

  useEffect(() => {
    if (status && !isLoading) {
      if (isPlaying) {
        playerElement.current.play()
      } else if (isPaused || isStoped) {
        playerElement.current.pause()
      }
    }
  }, [status, isLoading])

  useEffect(() => {
    if (ad.mediaFile) {
      if (currentTime === 0) {
        fetch(ad.triggers.implession)
      }

      if (currentTime === 1) {
        fetch(ad.triggers.start)
      }

      if (currentTime === Math.ceil((duration + 1) / 4)) {
        fetch(ad.triggers.firstQuartile)
      }

      if (currentTime === Math.ceil((duration + 1) / 2)) {
        fetch(ad.triggers.midpoint)
      }

      if (currentTime === Math.ceil((duration + 1) * 3 / 4)) {
        fetch(ad.triggers.thirdQuartile)
      }
    }
  }, [currentTime, ad])

  useEffect(() => {
    playerElement.current.volume = volume / 100
  }, [volume])

  useEffect(() => {
    if (src) {
      playerElement.current.src = src
      playerElement.current.load()

      function onLoading() {
        setLoading(true)
      }

      function onPlaying() {
        setLoading(false)
        play()
      }

      function onError(e) {
        if (src) {
          setLoading(false)
          playerElement.current.pause()
          resetAd()
        }
      }

      function onEnded() {
        fetch(ad.triggers.complete)
        playerElement.current.pause()
        resetAd()
      }

      function onDurationChange() {
        const duration = parseInt(playerElement.current.duration)
        setDuration(duration)
      }

      function onTimeUpdate() {
        if (playerElement.current) {
          const newTime = parseInt(playerElement.current.currentTime)
          setCurrentTime(newTime)
        }
      }

      playerElement.current.addEventListener("loadstart", onLoading)
      playerElement.current.addEventListener("canplay", onPlaying)
      playerElement.current.addEventListener("playing", onPlaying)
      playerElement.current.addEventListener("waiting", onLoading)
      playerElement.current.addEventListener("error", onError)
      playerElement.current.addEventListener("ended", onEnded)
      playerElement.current.addEventListener("durationchange", onDurationChange)
      playerElement.current.addEventListener("timeupdate", onTimeUpdate)
    }

    return () => {
      if (playerElement.current) {
        playerElement.current.pause()

        playerElement.current.removeEventListener("loadstart", onLoading)
        playerElement.current.removeEventListener("canplay", onPlaying)
        playerElement.current.removeEventListener("playing", onPlaying)
        playerElement.current.removeEventListener("waiting", onLoading)
        playerElement.current.removeEventListener("error", onError)
        playerElement.current.removeEventListener("ended", onEnded)

        playerElement.current.removeEventListener("durationchange", onDurationChange)
        playerElement.current.removeEventListener("timeupdate", onTimeUpdate)
      }
    }
  }, [src])

  return (
    <audio id="audioPlayer" ref={playerElement} src={src}></audio>
  )
}

export default PlayerInputAd