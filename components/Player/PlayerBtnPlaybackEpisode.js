import {CgSpinner} from "react-icons/cg"
import {MdPlayArrow, MdPause} from "react-icons/md"
import usePlayerStore from "@/store/usePlayerStore";
import {fetchVastData} from "@/api/vast/vast.api";

const PlayerBtnPlaybackEpisode = ({episode, podcast}) => {
  const isLoading = usePlayerStore((state) => state.loading)
  const setLoading = usePlayerStore((state) => state.setLoading)
  const play = usePlayerStore((state) => state.play)
  const pause = usePlayerStore((state) => state.pause)

  const playerID = usePlayerStore((state) => state.player.id)
  const setAd = usePlayerStore((state) => state.setAd)
  const setPlayer = usePlayerStore((state) => state.setPlayer)
  const isPlaying = usePlayerStore((state) => state.state.isPlaying)
  const resetAd = usePlayerStore((state) => state.resetAd)

  const isCurrent = playerID === episode.key

  async function handlePlay() {
    if (!isCurrent) {
      pause()
      resetAd()
      setLoading(true)

      setAd({mediaFile: ''})

      const data = {
        id: episode.key,
        image: episode.logo,
        title: episode.title,
        subTitle: podcast.title,
        mediaURL: episode.file_url,
        canonical: episode.url
      }

      setPlayer(data, 'episode')

      const ad = await fetchAd(podcast)

      if (ad) {
        setAd(ad)
      } else {
        setAd({mediaFile: null})
      }
    } else {
      play()
    }
  }

  function handlePause() {
    pause()
  }

  if (!isCurrent) {
    return (
      <button aria-label="Play" onClick={handlePlay}
              className="group w-12 h-12 border rounded-full text-center hover:bg-primary hover:border-primary">
        <MdPlayArrow className="mx-auto text-3xl group-hover:text-white"/>
      </button>
    )
  }

  if (isLoading) {
    return (
      <button className="w-12 h-12 border rounded-full text-center bg-primary text-white">
        <CgSpinner className="mx-auto text-3xl animate-spin"/>
      </button>
    )
  }

  if (isPlaying) {
    return (
      <button aria-label="Pause" onClick={handlePause}
              className="w-12 h-12 border rounded-full text-center bg-primary text-white">
        <MdPause className="mx-auto text-3xl"/>
      </button>
    )
  }

  return (
    <button aria-label="Play" onClick={handlePlay}
            className="w-12 h-12 border rounded-full text-center bg-primary text-white">
      <MdPlayArrow className="mx-auto text-3xl"/>
    </button>
  )
}

export default PlayerBtnPlaybackEpisode

async function fetchAd(podcast) {
  let params = {}

  if (podcast.key) {
    params.contentId = podcast.key
  }

  if (podcast.title) {
    params.contentName = podcast.title
  }

  if (podcast.language) {
    params.contentLanguage = podcast.language
  }

  return await fetchVastData(params)
}