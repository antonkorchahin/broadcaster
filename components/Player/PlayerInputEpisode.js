import React, {useEffect, useRef} from "react";
import usePlayerStore from "@/store/usePlayerStore";
import {getPartnerIds} from "@/api/adsPartners.api";

const PlayerInputEpisode = () => {
  const playerElement = useRef(null);
  const mediaFile = usePlayerStore((state) => state.player.mediaURL)

  const status = usePlayerStore((state) => state.status)
  const isPlaying = usePlayerStore((state) => state.state.isPlaying)
  const isLoading = usePlayerStore((state) => state.state.isLoading)
  const isPaused = usePlayerStore((state) => state.state.isPaused)
  const isStoped = usePlayerStore((state) => state.state.isStoped)
  const setLoading = usePlayerStore((state) => state.setLoading)

  const play = usePlayerStore((state) => state.play)
  const stop = usePlayerStore((state) => state.stop)

  const volume = usePlayerStore((state) => state.volume)
  const playbackRate = usePlayerStore((state) => state.playbackRate)

  const setDuration = usePlayerStore((state) => state.setDuration)
  const currentTime = usePlayerStore((state) => state.currentTime)
  const setCurrentTime = usePlayerStore((state) => state.setCurrentTime)

  useEffect(() => {
    if (status && !isLoading) {
      if (isPlaying) {
        playerElement.current.play()
      } else if (isPaused || isStoped) {
        playerElement.current.pause()
      }
    }
  }, [status, isLoading])

  useEffect(() => {
    playerElement.current.volume = volume / 100
  }, [volume])

  useEffect(() => {
    const time = parseInt(playerElement.current.currentTime)

    if (currentTime !== time) {
      playerElement.current.currentTime = currentTime
    }
  }, [currentTime])

  useEffect(() => {
    playerElement.current.playbackRate = playbackRate
  }, [playbackRate])

  useEffect(() => {
    function onLoading() {
      setLoading(true)
    }

    function onPlaying() {
      setLoading(false)
      play()
    }

    function onError(e) {
      setLoading(false)
      stop()
    }

    function onDurationChange() {
      const duration = parseInt(playerElement.current.duration)
      setDuration(duration)
    }

    function onTimeUpdate() {
      if (playerElement.current) {
        const newTime = parseInt(playerElement.current.currentTime)
        setCurrentTime(newTime)
      }
    }

    playerElement.current.addEventListener("loadstart", onLoading)
    playerElement.current.addEventListener("canplay", onPlaying)
    playerElement.current.addEventListener("playing", onPlaying)
    playerElement.current.addEventListener("waiting", onLoading)
    playerElement.current.addEventListener("error", onError)

    playerElement.current.addEventListener("durationchange", onDurationChange)
    playerElement.current.addEventListener("timeupdate", onTimeUpdate)

    return () => {
      if (playerElement.current) {
        playerElement.current.pause()

        playerElement.current.removeEventListener("loadstart", onLoading)
        playerElement.current.removeEventListener("canplay", onPlaying)
        playerElement.current.removeEventListener("playing", onPlaying)
        playerElement.current.removeEventListener("waiting", onLoading)
        playerElement.current.removeEventListener("error", onError)

        playerElement.current.removeEventListener("durationchange", onDurationChange)
        playerElement.current.removeEventListener("timeupdate", onTimeUpdate)
      }
    }
  }, [play, setLoading, stop])

  useEffect(() => {
    if (mediaFile) {
      playerElement.current.src = mediaFile
      playerElement.current.load()
    }
  }, [mediaFile])

  return <audio id="audioPlayer" ref={playerElement} src={mediaFile}></audio>
}

export default PlayerInputEpisode