import PlayerProgressBar from "@/components/Player/PlayerProgressBar";
import PlayerDuration from "@/components/Player/PlayerDuration";
import PlayerPlaybackRate from "@/components/Player/PlayerPlaybackRate";
import PlayerVolume from "@/components/Player/PlayerVolume";
import PlayerBtnSkip from "@/components/Player/PlayerBtnSkip";
import PlayerBtnPlayback from "@/components/Player/PlayerBtnPlayback";
import PlayerImage from "@/components/Player/PlayerImage";
import PlayerTitle from "@/components/Player/PlayerTitle";
import PlayerSubTitle from "@/components/Player/PlayerSubTitle";
import usePlayerStore from "@/store/usePlayerStore";
import PlayerTitleRadio from "@/components/Player/PlayerTitleRadio";
import PlayerInputRadio from "@/components/Player/PlayerInputRadio";
import PlayerInputEpisode from "@/components/Player/PlayerInputEpisode";
import PlayerInputAd from "@/components/Player/PlayerInputAd";


const Player = () => {
  const isRadioPlayer = usePlayerStore((state) => state.typeState.isRadioPlayer)
  const isEpisodePlayer = usePlayerStore((state) => state.typeState.isEpisodePlayer)
  const isAdPlayer = usePlayerStore((state) => state.typeState.isAdPlayer)

  if (!isRadioPlayer && !isEpisodePlayer && !isAdPlayer) {
    return
  }

  let playerInput = null

  if (isAdPlayer) {
    playerInput = <PlayerInputAd/>
  } else if (isEpisodePlayer) {
    playerInput = <PlayerInputEpisode/>
  } else if (isRadioPlayer) {
    playerInput = <PlayerInputRadio/>
  }

  const title = isRadioPlayer ? <PlayerTitleRadio/> : (isAdPlayer ? 'The content will resume shortly' : <PlayerTitle/>)

  return (
    <div className="sticky bottom-0 z-50">
      <div
        className="h-20 backdrop-grayscale bg-secondary bg-opacity-90 backdrop-filter backdrop-blur-md text-white w-full">

        {
          !isRadioPlayer &&
          <div className="absolute -top-1 w-full flex">
            <PlayerProgressBar readonly={!!isAdPlayer}/>
          </div>
        }

        <div className="lg:px-4 md:px-4 px-2 justify-between h-full flex-line space-x-2">

          <div className="flex-1 h-full flex-line overflow-hidden">

            <PlayerImage/>

            <div className="overflow-hidden">
              <div className="block text-sm truncate">
                {title}
              </div>

              <PlayerSubTitle/>
            </div>

          </div>

          <div className="flex-line h-full md:space-x-2 space-x-1">

            {/*<button*/}
            {/*  className="w-12 h-12 hover:text-white text-gray-300 hover:scale-110 transition duration-100 ease-in-out transform active:text-primary active:bg-opacity-50 active:bg-secondary rounded-full">*/}
            {/*  <FiSkipBack className="text-lg mx-auto"/>*/}
            {/*</button>*/}

            {
              (!isAdPlayer && isEpisodePlayer) &&
              <div className="md:block hidden">
                <PlayerBtnSkip backward/>
              </div>
            }

            <PlayerBtnPlayback/>

            {
              (!isAdPlayer && isEpisodePlayer) &&
              <div className="md:block hidden">
                <PlayerBtnSkip/>
              </div>
            }

            {/*<button*/}
            {/*  className="w-12 h-12 hover:text-white text-gray-300 hover:scale-110 transition duration-100 ease-in-out transform active:text-primary active:bg-opacity-50 active:bg-secondary rounded-full">*/}
            {/*  <FiSkipForward className="text-lg mx-auto"/>*/}
            {/*</button>*/}

          </div>

          <div className="flex-1 md:block hidden">
            <div className="h-full flex-line justify-end lg:space-x-6 md:space-x-2 space-x-0">
              {
                (isAdPlayer || isEpisodePlayer) &&
                <PlayerDuration/>
              }

              {
                (!isAdPlayer && isEpisodePlayer) &&
                <PlayerPlaybackRate/>
              }

              <PlayerVolume/>
            </div>
          </div>

        </div>
      </div>

      {
        playerInput &&
        playerInput
      }
    </div>
  )
}

export default Player