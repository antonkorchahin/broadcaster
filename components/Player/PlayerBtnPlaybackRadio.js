import {CgSpinner} from "react-icons/cg";
import usePlayerStore from "@/store/usePlayerStore";
import {FiPauseCircle, FiPlayCircle} from "react-icons/fi";

const PlayerBtnPlaybackRadio = ({radio}) => {
  const playerID = usePlayerStore((state) => state.player.id)
  const play = usePlayerStore((state) => state.play)
  const pause = usePlayerStore((state) => state.pause)
  const isPlaying = usePlayerStore((state) => state.state.isPlaying)
  const isLoading = usePlayerStore((state) => state.state.isLoading)
  const setPlayer = usePlayerStore((state) => state.setPlayer)
  const setLoading = usePlayerStore((state) => state.setLoading)
  const resetAd = usePlayerStore((state) => state.resetAd)

  const isCurrent = playerID === radio.key

  async function handlePlay() {
    if (!isCurrent) {
      pause()
      resetAd()
      setLoading(true)

      const data = {
        id: radio.key,
        image: radio.logo,
        subTitle: radio.name,
        mediaURL: radio.streamURL,
        streamName: radio.streamName,
        canonical: radio.url
      }

      setPlayer(data, 'radio')
    } else {
      play()
    }
  }

  function handlePause() {
    pause()
  }

  if (isCurrent && isLoading) {
    return (
      <button className="sm:px-4 px-3 sm:py-2 py-1 rounded-full flex text-secondary bg-primary">
        <span className="flex-line mx-auto">
          <CgSpinner className="animate-spin text-3xl"/>
          <span className="font-medium ml-1">Loading</span>
        </span>
      </button>
    )
  }

  if (isCurrent && isPlaying) {
    return (
      <button aria-label="Pause" onClick={handlePause}
              className="sm:px-4 px-3 sm:py-2 py-1 rounded-full flex text-secondary bg-primary">
        <span className="flex-line mx-auto">
          <FiPauseCircle strokeWidth={1} className="text-3xl"/>
          <span className="font-medium ml-1">Pause</span>
        </span>
      </button>
    )
  }

  return (
    <button aria-label="Play" onClick={handlePlay}
            className="sm:px-4 px-3 sm:py-2 py-1 rounded-full flex text-secondary bg-primary">
      <span className="flex-line mx-auto">
        <FiPlayCircle strokeWidth={1} className="text-3xl"/>
        <span className="font-medium ml-1">Listen Now</span>
      </span>
    </button>
  )
}

export default PlayerBtnPlaybackRadio