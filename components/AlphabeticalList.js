import Link from "next/link"

const AlphabeticalList = ({list}) => {
  const alphabeticalList = {}

  list.forEach(item => {
    const letter = item.name.charAt(0)

    if (letter in alphabeticalList) {
      alphabeticalList[letter].push(item)
    } else {
      alphabeticalList[letter] = [item]
    }
  })

  return (
    <div className="grid xl:grid-cols-6 lg:grid-cols-4 md:grid-cols-3 sm:grid-cols-2 grid-cols-1 gap-8">
      {Object.keys(alphabeticalList).map((key, index) => (
        <div key={index}>
          <h2 className="text-xl font-semibold border-b pb-4 mb-2">{key}</h2>

          {alphabeticalList[key].map((item, i) => (
            <Link href={item.url} prefetch={false} key={i}>
              <a
                className="block sm:mb-2 mb-0 text-gray-600 hover:text-primary hover:underline transition sm:py-0 py-3">
                {item.name}
              </a>
            </Link>
          ))}
        </div>
      ))}
    </div>
  )
}

export default AlphabeticalList