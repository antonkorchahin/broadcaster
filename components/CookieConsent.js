import {useEffect} from "react"

import "../node_modules/vanilla-cookieconsent/dist/cookieconsent.js"
import "../node_modules/vanilla-cookieconsent/dist/cookieconsent.css"

import useCookieConsentStore from "@/store/useCookieConsentStore"

export default function CookieConsent() {
  const setLevels = useCookieConsentStore((state) => state.setLevels)
  const setConsentFinished = useCookieConsentStore((state) => state.setConsentFinished)

  useEffect(() => {
    const callCC = () => {
      let cookieconsent = window.initCookieConsent()

      cookieconsent.run({
        current_lang: 'en',
        autoclear_cookies: true,
        revision: 3,

        onFirstAction: function (user_preferences, cookie) {
          setConsentFinished(true)
          setLevels(cookie.level)
        },

        onAccept: function (cookie) {
          setConsentFinished(true)
          setLevels(cookie.level)
        },

        onChange: function (cookie) {
          setConsentFinished(true)
          setLevels(cookie.level)
        },

        gui_options: {
          consent_modal: {
            layout: 'cloud',               // box/cloud/bar
            position: 'bottom',     // bottom/middle/top + left/right/center
            transition: 'slide',           // zoom/slide
            swap_buttons: false            // enable to invert buttons
          },
          settings_modal: {
            layout: 'box',                 // box/bar
            transition: 'slide'            // zoom/slide
          }
        },

        languages: {
          'en': {
            consent_modal: {
              title: 'We use cookies!',
              description: 'This website uses cookies to ensure you get the best experience on our website. <button type="button" data-cc="c-settings" class="cc-link">Cookie settings</button>',
              revision_message: 'This website uses cookies to ensure you get the best experience on our website. <button type="button" data-cc="c-settings" class="cc-link">Cookie settings</button>',
              primary_btn: {
                text: 'Accept all',
                role: 'accept_all'
              },
            },
            settings_modal: {
              title: 'Cookie preferences',
              save_settings_btn: 'Save settings',
              accept_all_btn: 'Accept all',
              reject_all_btn: 'Reject all',
              close_btn_label: 'Close',
              cookie_table_headers: [
                {name: 'Name'},
                {expiration: 'Cookie Lifespan'},
                {domain: 'Domain'},
                {provider: 'Provider'},
                {description: 'Description'},
              ],
              blocks: [
                {
                  title: 'Cookie usage 📢',
                  description: 'I use cookies to ensure the basic functionalities of the website and to enhance your online experience. You can choose for each category to opt-in/out whenever you want. For more details relative to cookies and other sensitive data, please read the full <a href="https://zenomedia.com/s/privacy_policy.pdf" class="cc-link">privacy policy</a>.'
                },
                {
                  title: 'Strictly necessary cookies',
                  description: 'These cookies are essential for the proper functioning of our website. Without these cookies, the website would not work properly',
                  toggle: {
                    value: 'necessary',
                    enabled: true,
                    readonly: true
                  },
                  cookie_table: [
                    {
                      name: 'cc_cookie',
                      domain: '.zeno.fm',
                      expiration: '6 months',
                      provider: 'Zeno Media',
                      description: "Used to store user's consent preferences.",
                    }
                  ]
                },
                {
                  title: 'Performance and Analytics cookies',
                  description: 'We use performance/analytics cookies to analyze how the website is accessed, used, or is performing in order to\n' +
                    'provide you with a better user experience and to maintain, operate and continually improve the website.',
                  toggle: {
                    value: 'analytics',
                    enabled: true,
                    readonly: false
                  },
                  cookie_table: [
                    {
                      name: '^_ga',
                      domain: '.zeno.fm',
                      expiration: '2 years',
                      provider: 'Alphabet',
                      description: 'Used to distinguish users.',
                    },
                    {
                      name: '_ga_4GCDY3QY3R',
                      domain: '.zeno.fm',
                      expiration: '2 years',
                      provider: 'Alphabet',
                      description: 'Used to persist session state.',
                    }
                  ]
                },
                {
                  title: 'Advertisement and Targeting cookies',
                  description: 'These cookies collect information about how you use the website, which pages you visited and which links you clicked on. All of the data is anonymized and cannot be used to identify you',
                  toggle: {
                    value: 'targeting',
                    enabled: true,
                    readonly: false
                  },
                  cookie_table: [
                    {
                      name: '__gads',
                      domain: '.zeno.fm',
                      expiration: '1 year 24 days',
                      provider: 'Alphabet',
                      description: 'Used provide ad delivery or retargeting.',
                    },
                    {
                      name: '__gpi',
                      domain: '.zeno.fm',
                      expiration: '1 year 24 days',
                      provider: 'Alphabet',
                      description: 'Used to persist session state.',
                    },
                    {
                      name: 'NID',
                      domain: '.google.com',
                      expiration: '6 months',
                      provider: 'Alphabet',
                      description: 'Used to provide ad delivery or retargeting, store user preferences.',
                    },
                    {
                      name: '1P_JAR',
                      domain: '.google.com',
                      expiration: '1 month',
                      provider: 'Alphabet',
                      description: 'Used to provide ad delivery or retargeting.',
                    },
                    {
                      name: 'IDE',
                      domain: '.doubleclick.net',
                      expiration: '13 months',
                      provider: 'Alphabet',
                      description: 'Used to provide ad delivery or retargeting.',
                    },
                    {
                      name: 'DV',
                      domain: 'www.google.com',
                      expiration: '1 day',
                      provider: 'Alphabet',
                      description: 'Used to provide ad delivery or retargeting.',
                    }
                  ]
                },
                {
                  title: 'More information',
                  description: 'For any queries in relation to our policy on cookies and your choices, please <a class="cc-link" href="mailto:support@zenomedia.com">contact us</a>.',
                }
              ]
            }
          }
        }
      })
    }

    callCC()
  }, []);

  return null;
}