import Link from "next/link"
import {categoryLink, countryLink, languageLink} from "@/helpers/podcasts.helper"

const DiscoverPodcastsBy = () => {
  return (
    <div className="grid grid-cols-3 sm:gap-4 gap-2 font-semibold sm:text-xl text-sm">
      <Link href={categoryLink()} prefetch={false}>
        <a
          className="text-center bg-gray-100 px-1 py-2 rounded-md sm:mr-2 mr-0 hover:bg-gray-200">
          By Category
        </a>
      </Link>

      <Link href={countryLink()} prefetch={false}>
        <a
          className="text-center bg-gray-100 px-1 py-2 rounded-md sm:mr-2 mr-0 hover:bg-gray-200">
          By Country
        </a>
      </Link>

      <Link href={languageLink()} prefetch={false}>
        <a
          className="text-center bg-gray-100 px-1 py-2 rounded-md hover:bg-gray-200">
          By Language
        </a>
      </Link>
    </div>
  )
}

export default DiscoverPodcastsBy