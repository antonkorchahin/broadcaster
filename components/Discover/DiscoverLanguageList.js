import Link from "next/link"

const DiscoverLanguageList = ({languages, fullListLink}) => {

  return (
    <div className="sm:text-left text-center">
      <h2 className="sm:text-2xl text-xl mb-6 font-semibold text-center">
        Top Languages
      </h2>

      <div className="flex flex-wrap justify-center flex-row items-center mb-4">
        {languages.map((language, index) => (
          <div key={index} className="sm:mb-2 mb-1">
            &nbsp;<Link href={language.url} prefetch={false}>
            <a className="sm:h-auto h-12 inline-flex">
              <span
                className="sm:text-xl self-center font-semibold sm:border-b-2 border-b transition hover:border-transparent border-black">
                {language.name}
              </span>
            </a>
          </Link> {index + 1 !== languages.length ? "/" : ""}
          </div>
        ))}
      </div>

      <div className="text-center">
        <Link href={fullListLink} prefetch={false}>
          <a className="text-center mx-auto sm:border-b-2 border-b border-black hover:border-b-0">
            All languages
          </a>
        </Link>
      </div>
    </div>
  )
}

export default DiscoverLanguageList