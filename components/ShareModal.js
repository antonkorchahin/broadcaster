import {event} from "@/lib/ga"
import {siteUrl} from "@/helpers/helpers"
import {Dialog, Transition} from "@headlessui/react"
import {FiShare2} from "react-icons/fi"
import {FiX} from "react-icons/fi"
import {useRouter} from "next/router"
import {Fragment, useState, useEffect} from "react"
import {FaFacebookF} from "react-icons/fa"
import {FaTwitter} from "react-icons/fa"

import {FacebookShareButton, TwitterShareButton} from "react-share"

const ShareModal = () => {
  const router = useRouter()

  const trackShareClick = (type) => {
    event({
      action: `share_${type}_click`,
      params: {
        event_category: "share",
        event_label: siteUrl(router.asPath)
      }
    })
  }

  const [url, setUrl] = useState("")
  let [isOpen, setIsOpen] = useState(false)

  function closeModal() {
    setIsOpen(false)
  }

  function openModal() {
    setIsOpen(true)
  }

  useEffect(() => {
    setUrl("https://" + window.location.hostname + window.location.pathname)
  }, [])

  return (
    <>
      <Transition appear show={isOpen} as={Fragment}>
        <Dialog
          as="div"
          className="fixed inset-0 z-10 overflow-y-auto"
          onClose={closeModal}
        >
          <Dialog.Overlay className="fixed inset-0 bg-black opacity-30"/>

          <div className="min-h-screen px-4 text-center">
            <Transition.Child
              as={Fragment}
              enter="ease-out duration-300"
              enterFrom="opacity-0"
              enterTo="opacity-100"
              leave="ease-in duration-200"
              leaveFrom="opacity-100"
              leaveTo="opacity-0"
            >
              <Dialog.Overlay className="fixed inset-0"/>
            </Transition.Child>

            <span
              className="inline-block h-screen align-middle"
              aria-hidden="true"
            >
              &#8203;
            </span>
            <Transition.Child
              as={Fragment}
              enter="ease-out duration-300"
              enterFrom="opacity-0 scale-95"
              enterTo="opacity-100 scale-100"
              leave="ease-in duration-200"
              leaveFrom="opacity-100 scale-100"
              leaveTo="opacity-0 scale-95"
            >
              <div
                className="inline-block w-full max-w-md p-4 my-8 overflow-hidden text-left align-middle transition-all transform bg-white shadow-xl rounded-2xl">
                <Dialog.Title
                  as="h3"
                  className="text-lg font-medium leading-6 text-gray-900"
                >
                  Share
                </Dialog.Title>

                <div className="absolute right-4 top-4">
                  <button
                    type="button"
                    className="focus:outline-none"
                    onClick={closeModal}
                  >
                    <FiX/>
                  </button>
                </div>

                <div className="mt-2 flex justify-center gap-4 text-3xl">

                  <FacebookShareButton className="focus:outline-none" onClick={() => trackShareClick("facebook")}
                                       url={url}>
                    <div className="rounded-full h-14 w-14 flex items-center text-white justify-center bg-facebook">
                      <FaFacebookF/>
                    </div>
                  </FacebookShareButton>

                  <TwitterShareButton className="focus:outline-none" onClick={() => trackShareClick("twitter")}
                                      url={url}>
                    <div className="rounded-full h-14 w-14 flex items-center text-white justify-center bg-twitter">
                      <FaTwitter/>
                    </div>
                  </TwitterShareButton>

                </div>
              </div>
            </Transition.Child>
          </div>
        </Dialog>
      </Transition>

      <button className="focus:outline-none hover:text-white text-gray-300" onClick={openModal} aria-label="share">
        <FiShare2/>
      </button>
    </>
  )

}

export default ShareModal