import Link from "next/link"
import {FiChevronRight} from "react-icons/fi"

const Section = ({title, url, children, borderBottom = false, heightAuto = false}) => {
  return (
    <section className={(!heightAuto ? "min-h-56" : "") + (borderBottom ? " border-bottom" : "")}>
      <h2 className="flex-line justify-between mb-2">
        <span className="sm:text-xl text-lg font-semibold mr-2">
          {title}
        </span>

        {
          typeof url !== "undefined" &&
          <Link href={url} prefetch={false}>
            <a className="flex-none flex-line group">
              <span
                className="border-b border-transparent group-hover:border-primary group-hover:text-primary mr-1 text-sm">View all</span>

              <FiChevronRight className="text-primary"/>
            </a>
          </Link>
        }
      </h2>

      {children}
    </section>
  )
}

export default Section