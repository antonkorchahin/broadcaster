import Section from "@/components/Section"
import {religionLink} from "@/helpers/prayer-line.helper"
import LinkPill from "@/components/common/LinkPill"

const PrayerLineReligionList = ({religions, religion = null, borderBottom = true}) => {
  return (
    <Section title="Top Religions" url={religionLink()} heightAuto={true} borderBottom={borderBottom}>
      <div className="overflow-x-auto">
        <div className="flex flex-row sm:flex-wrap flex-nowrap items-center font-semibold sm:pb-0 pb-4">
          {religions.map((item, index) => (
            <LinkPill key={index} href={item.url} active={religion && item.name === religion.name}>
              {item.name}
            </LinkPill>
          ))}
        </div>
      </div>
    </Section>
  )
}

export default PrayerLineReligionList