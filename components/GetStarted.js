import {siteUrl} from "@/helpers/helpers"
import Image from "next/image"
import Link from "next/link"

const GetStarted = () => {
  return (
    <section className="container mx-auto max-w-screen-2xl sm:px-8 px-4 my-16">
      <h2 className="sm:text-5xl text-3xl text-center mb-6 font-bold">
        Let&apos;s get started
      </h2>

      <p className="text-center sm:text-xl">See all that Zeno has to offer</p>

      <div className="grid mt-10 sm:grid-cols-2 grid-cols-1 gap-8">
        <div className="w-full h-80 block relative rounded-lg">
          <Image alt="Explore the features and how you can benefit" className="rounded-lg filter brightness-75"
                 src="/images/discover1.webp"
                 layout="fill"
                 objectFit="cover"
          />

          <div className="h-full relative p-10 flex items-end">
            <div>
              <h3 className="text-white sm:text-3xl font-bold text-2xl">Are you a broadcaster?</h3>
              <p className="text-white opacity-70 mt-2 mb-8">Explore the features and how you can benefit</p>
              <Link href={siteUrl("/streaming/")} prefetch={false}>
                <a className="px-8 py-3 rounded-full bg-primary font-semibold">
                  Get started
                </a>
              </Link>
            </div>
          </div>
        </div>
        <div className="w-full h-80 block relative rounded-lg">
          <Image alt="Top Up and Send gift card anywhere in the world" className="rounded-lg filter brightness-75"
                 src="/images/discover2.webp"
                 layout="fill"
                 objectFit="cover"
          />

          <div className="h-full relative p-10 flex items-end">
            <div>
              <h3 className="text-white sm:text-3xl font-bold text-2xl">Loved ones over seas?</h3>
              <p className="text-white opacity-70 mt-2 mb-8">Top Up and Send gift card anywhere in the world</p>
              <Link href={siteUrl("/zeno-plus/")} prefetch={false}>
                <a className="px-8 py-3 rounded-full bg-primary font-semibold">
                  Get started
                </a>
              </Link>
            </div>
          </div>
        </div>
      </div>
    </section>
  )
}

export default GetStarted