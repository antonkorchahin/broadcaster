import Link from "next/link"
import { FiChevronRight } from "react-icons/fi"

const Breadcrumbs = ({breadcrumbs, heading = null}) => {
  return (
    <div className="bg-light-gray border-0 border-b sm:mb-8 mb-4">
      <div className="container max-w-screen-lg mx-auto px-4">
        {
          heading &&
          <h1 className="font-bold md:text-3xl sm:text-2xl text-xl pt-4">
            {heading}
          </h1>
        }

        <nav className="overflow-x-auto sm:text-sm text-xs opacity-80">
          <ul className="flex-line">
            {breadcrumbs.map((breadcrumb, index) => (
              typeof breadcrumb.url === "undefined" ? (
                <li key={index} className="py-3 flex-shrink-0 pr-4">{breadcrumb.title}</li>
              ) : (
                <li key={index} className="flex-line flex-shrink-0">
                  <Link href={breadcrumb.url} prefetch={false}>
                    <a className="font-semibold py-3 hover:underline">
                      {breadcrumb.title}
                    </a>
                  </Link>

                  {
                    breadcrumbs.length > (index + 1) &&
                    <FiChevronRight className="mx-1"/>
                  }
                </li>
              )
            ))}
          </ul>
        </nav>
      </div>
    </div>
  )
}

export default Breadcrumbs