import {fetchList} from "@/api/app/index"

export const fetchPrayerLines = async (params = {}) => {
  return await fetchList(params, 'prayer-lines')
}