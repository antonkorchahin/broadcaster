import {logError} from "@/helpers/helpers"
import {BASE_URL} from "@/api/index"

export const fetchList = async (params, type) => {
  const searchParams = new URLSearchParams(params)

  const endpoint = BASE_URL + `/api/${type}/?${searchParams.toString()}`

  return await fetch(endpoint)
    .then(async response => response.json())
    .then(data => data)
    .catch(e => {
      logError(e, endpoint)
      return []
    })
}