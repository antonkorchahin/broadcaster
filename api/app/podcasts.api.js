import {fetchList} from "@/api/app/index"

export const fetchPodcasts = async (params = {}) => {
  return await fetchList(params, 'podcasts')
}