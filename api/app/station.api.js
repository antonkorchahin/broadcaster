import {fetchList} from "@/api/app/index"

export const fetchStations = async (params = {}) => {
  return await fetchList(params, 'stations')
}