import {fetchList} from "@/api/app/index"

export const fetchEpisodes = async (params = {}) => {
  return await fetchList(params, 'episodes')
}