import {TOOLS_URL} from "@/api/index"

export const fetchPartners = async (limit = null) => {
  let endpoint = TOOLS_URL + `api/zenopartners/list`

  if (limit) {
    endpoint = endpoint + `?limit=${limit}`
  }

  return await fetch(endpoint)
    .then(async response => {
      if (response.ok) {
        return response.json()
      } else {
        throw new Error(`${response.status} ${response.statusText}`)
      }
    })
    .then(data => {
      if (Array.isArray(data)) {
        return data
      }

      throw new Error("Missing data " + JSON.stringify(data))
    })
    .catch(e => {
      console.log(endpoint)
      throw e
    })
}

export const fetchPartner = async (slug = null) => {
  const endpoint = TOOLS_URL + `api/zenopartners/slug/${slug}`

  return await fetch(endpoint)
    .then(async response => {
      if (response.ok) {
        return response.json()
      } else {
        if (response.status === 404) {
          return false
        }

        throw new Error(`${response.status} ${response.statusText}`)
      }
    })
    .then(data => {
      if (data === false) {
        return false
      } else if (typeof data.key !== "undefined" || typeof data.objectID !== "undefined" || typeof data.id !== "undefined") {
        return data
      }

      throw new Error("Missing data " + JSON.stringify(data))
    })
    .catch(e => {
      console.log(endpoint)
      throw e
    })
}