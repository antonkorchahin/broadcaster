export const getPartnerIds = async () => {
  const response = await fetch("https://yield-op-idsync.live.streamtheworld.com/partnerIds", {credentials: "include"})
  return await response.json()
}