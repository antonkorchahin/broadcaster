import {API_ZENOPLAY_URL} from "@/api/index"

export const getGDPR = async () => {
  return await fetch(API_ZENOPLAY_URL + `api/zenofm/gdpr`)
    .then(async response => response.json())
    .then(data => data)
}