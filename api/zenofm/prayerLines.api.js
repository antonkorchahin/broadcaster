import {API_ZENOPLAY_URL} from "@/api/index"
import {logError} from "@/helpers/helpers"

export const searchPrayerLines = async (params) => {
  const {
    religion = null,
    query = "",
    page = 1,
    limit = 10
  } = params

  let filters = {}

  if (religion) {
    filters.religion = [religion]
  }

  let data = {
    query: query,
    page: page,
    hitsPerPage: limit,
    filters: filters
  }

  const endpoint = API_ZENOPLAY_URL + "api/zenofm/search/prayerlines/"

  try {
    const response = await fetch(endpoint, {
      method: "post",
      headers: {
        "Accept": "application/json",
        "Content-Type": "application/json"
      },
      body: JSON.stringify(data)
    })

    const results = await response.json()

    if (!results.total) {
      return []
    }

    return results.hits
  } catch (e) {
    logError(e)
    return []
  }
}

export const fetchPrayerLine = async (identifier) => {
  const endpoint = API_ZENOPLAY_URL + `api/zenofm/prayerlines/${identifier}/`

  return await fetch(endpoint)
    .then(async response => {
      if (response.ok) {
        return response.json()
      } else {
        if (response.status === 404) {
          return false
        }

        throw new Error(`${response.status} ${response.statusText}`)
      }
    })
    .then(data => {
      if (data === false) {
        return false
      } else if (typeof data.key !== "undefined" || typeof data.objectID !== "undefined" || typeof data.uid !== "undefined") {
        return data
      }

      throw new Error("Missing data " + JSON.stringify(data))
    })
    .catch(e => {
      console.log(endpoint)
      throw e
    })
}