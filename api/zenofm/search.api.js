import {API_ZENOPLAY_URL} from "@/api/index"

export const getAttributes = async (object, type, limit = null) => {

  let url = API_ZENOPLAY_URL + `api/zenofm/search/${object}/${type}`

  if (limit) {
    url = url + `?limit=${limit}&popular=true`
  }

  const res = await fetch(url)

  return await res.json()
}