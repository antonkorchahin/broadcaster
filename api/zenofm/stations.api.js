import {API_EDITOR_URL, API_ZENOPLAY_URL} from "@/api/index"
import {logError} from "@/helpers/helpers"

export const fetchRadio = async (identifier) => {
  const endpoint = API_ZENOPLAY_URL + `api/zenofm/stations/${identifier}/`

  return await fetch(endpoint)
    .then(async response => {
      if (response.ok) {
        return response.json()
      } else {
        if (response.status === 404) {
          return false
        }

        throw new Error(`${response.status} ${response.statusText}`)
      }
    })
    .then(data => {
      if (data === false) {
        return false
      } else if (typeof data.key !== "undefined" || typeof data.objectID !== "undefined") {
        return data
      }

      throw new Error("Missing data " + JSON.stringify(data))
    })
    .catch(e => {
      console.log(endpoint)
      throw e
    })
}

export const searchStations = async (params) => {
  const {
    identifier = null,
    country = null,
    language = null,
    genre = null,
    featured = null,
    query = null,
    page = null,
    limit = null
  } = params

  let filters = {}

  if (identifier) {
    filters.objectID = [identifier]
  }

  if (country) {
    filters.country = [country]
  }

  if (language) {
    filters.language = [language]
  }

  if (genre) {
    filters.content_type = [genre]
  }

  if (featured) {
    filters.featured = [featured]
  }

  let data = {
    query: query || "",
    page: page || 1,
    hitsPerPage: limit || 10,
    filters: filters
  }

  const response = await fetch(API_ZENOPLAY_URL + "api/zenofm/search/stations", {
    method: "post",
    headers: {
      "Accept": "application/json",
      "Content-Type": "application/json"
    },
    body: JSON.stringify(data)
  })

  const results = await response.json()

  if (!results.total) {
    return []
  }

  return results.hits
}

export const fetchStationCards = async (stationKey) => {
  return await fetch(API_EDITOR_URL + `api/zenofm/${stationKey}/dashboardCards`)
    .then(async response => {
      if (response.ok) {
        return response.json()
      }

      throw Error(response.statusText)
    })
    .then(data => data)
    .catch(e => {
      logError(e)
      throw e
    })
}