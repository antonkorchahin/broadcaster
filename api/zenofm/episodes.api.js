import {API_ZENOPLAY_URL} from "@/api/index"
import {logError} from "@/helpers/helpers"

export const searchEpisodes = async (podcastKey = null, params) => {
  const {
    query = "",
    offset = 0,
    limit = 10,
    sort_by = "publish_date",
    sort_dir = "desc"
  } = params

  const endpoint = API_ZENOPLAY_URL + `api/zenofm/podcasts/${podcastKey}/episodes/?` +
    `query=${encodeURIComponent(query)}&limit=${limit}&offset=${offset}&sort_by=${sort_by}&sort_dir=${sort_dir}`

  try {
    const response = await fetch(endpoint)

    return await response.json()
  } catch (e) {
    logError(e, endpoint)
    return null
  }
}

export const fetchEpisode = async (podcastId, episodeId) => {
  const endpoint = API_ZENOPLAY_URL + `api/zenofm/podcasts/${podcastId}/episode/${episodeId}`

  return await fetch(endpoint)
    .then(async response => {
      if (response.ok) {
        return response.json()
      } else {
        if (response.status === 404) {
          return false
        }

        throw new Error(`${response.status} ${response.statusText}`)
      }
    })
    .then(data => {
      if (data === false) {
        return false
      } else if (typeof data.key !== "undefined" || typeof data.objectID !== "undefined") {
        return data
      }

      throw new Error("Missing data " + JSON.stringify(data))
    })
    .catch(e => {
      console.log(endpoint)
      throw e
    })
}