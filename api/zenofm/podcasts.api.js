import {API_ZENOPLAY_URL} from "@/api/index"
import {logError} from "@/helpers/helpers"

export const searchPodcasts = async (params) => {
  const {
    station_key = null,
    country = null,
    language = null,
    category = null,
    query = null,
    page = null,
    limit = null
  } = params

  let filters = {}

  if (country) {
    filters.country = [country]
  }

  if (language) {
    filters.language = [language]
  }

  if (category) {
    filters.categories = [category]
  }

  if (station_key) {
    filters.station_key = [station_key]
  }

  let data = {
    query: query || "",
    page: page || 1,
    hitsPerPage: limit || 10,
    filters: filters
  }

  const endpoint = API_ZENOPLAY_URL + `api/zenofm/search/podcasts`

  return await fetch(endpoint, {
    method: "post",
    headers: {
      "Accept": "application/json",
      "Content-Type": "application/json"
    },
    body: JSON.stringify(data)
  })
    .then(async response => {
      if (response.ok) {
        return response.json()
      }

      throw new Error(`${response.status} ${response.statusText}`)
    })
    .then(data => {
      if (typeof data.hits !== "undefined" && Array.isArray(data.hits)) {
        return data.hits
      }

      throw new Error("Missing data " + JSON.stringify(data))
    })
    .catch(e => {
      logError(e, endpoint)
      return []
    })
}

export const fetchPodcast = async (identifier) => {
  const endpoint = API_ZENOPLAY_URL + `api/zenofm/podcasts/${identifier}/`

  return await fetch(endpoint)
    .then(async response => {
      if (response.ok) {
        return response.json()
      } else {
        if (response.status === 404) {
          return false
        }

        throw new Error(`${response.status} ${response.statusText}`)
      }
    })
    .then(data => {
      if (data === false) {
        return false
      } else if (typeof data.key !== "undefined" || typeof data.objectID !== "undefined") {
        return data
      }

      throw new Error("Missing data " + JSON.stringify(data))
    })
    .catch(e => {
      console.log(endpoint)
      throw e
    })
}

export const fetchStationPodcasts = async (identifier) => {
  const endpoint = API_ZENOPLAY_URL + `api/zenofm/stations/${identifier}/podcasts`

  return await fetch(endpoint)
    .then(async response => response.json())
    .then(data => data)
    .catch(e => {
      logError(e, endpoint)
      return []
    })
}