export const BASE_URL = process.env.NEXT_PUBLIC_BASE_URL
export const API_ZENOPLAY_URL = process.env.NEXT_PUBLIC_API_ZENOPLAY_URL
export const API_EDITOR_URL = process.env.NEXT_PUBLIC_API_EDITOR_URL
export const TOOLS_URL = process.env.NEXT_PUBLIC_API_TOOLS_URL