export const fetchVastData = async (params = null) => {
  params.apiKey = process.env.NEXT_PUBLIC_VAST_API_KEY
  params = (new URLSearchParams(params)).toString()

  return await fetch(process.env.NEXT_PUBLIC_VAST_API_URL + '/vast/4.1?' + params)
    .then(response => {
      return response.text()
    }).then(str => {
      return new window.DOMParser().parseFromString(str, "text/xml")
    }).then(data => {
      if (!data.getElementsByTagName("MediaFile").length) {
        return null
      }

      return {
        mediaFile: data.getElementsByTagName("MediaFile")[0].innerHTML,
        triggers: {
          implession: data.getElementById("impression").innerHTML.replaceAll('&amp;', '&'),
          start: data.querySelector("[event='start']").innerHTML.replaceAll('&amp;', '&'),
          firstQuartile: data.querySelector("[event='firstQuartile']").innerHTML.replaceAll('&amp;', '&'),
          midpoint: data.querySelector("[event='midpoint']").innerHTML.replaceAll('&amp;', '&'),
          thirdQuartile: data.querySelector("[event='thirdQuartile']").innerHTML.replaceAll('&amp;', '&'),
          complete: data.querySelector("[event='complete']").innerHTML.replaceAll('&amp;', '&'),
        }
      }
    })
}